import axios from "axios";

export const ENDPOINTS = {
  // BASE_URL:"http://127.0.0.1:8000/rest/",// home
  BASE_URL: "http://35.154.201.233:81/rest/", // prod

  registerCustomer: "registerCustomer/",
  customerLogin: "customerLogin/",
  getInstruments: "getInstruments/",
  getInstrumentById: "getInstrumentById/",
  makeDeal: "makeDeal/"
};

export const registerCustomerAPI = function registerCustomerAPI(data) {
  return axios.post(ENDPOINTS.BASE_URL + ENDPOINTS.registerCustomer, data, {
    headers: {
      "Content-Type": "application/json"
    }
  });
};

export const makeDealAPI = function makeDealAPI(data, id) {
  return axios.post(ENDPOINTS.BASE_URL + ENDPOINTS.makeDeal + id, data, {
    headers: {
      "Content-Type": "application/json"
    }
  });
};

export const customerLoginAPI = function customerLoginAPI(data) {
  return axios.post(ENDPOINTS.BASE_URL + ENDPOINTS.customerLogin, data, {
    headers: {
      "Content-Type": "application/json"
    }
  });
};

export const getInstrumentsAPI = function getInstrumentsAPI(acc_no) {
  return axios.get(ENDPOINTS.BASE_URL + ENDPOINTS.getInstruments + acc_no, {
    headers: {
      "Content-Type": "application/json"
    }
  });
};

export const getInstrumentByIdAPI = function getInstrumentByIdAPI(id) {
  return axios.get(ENDPOINTS.BASE_URL + ENDPOINTS.getInstrumentById + id, {
    headers: {
      "Content-Type": "application/json"
    }
  });
};
