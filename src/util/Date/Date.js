import React, { useState } from "react";
import { withStyles } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import Menu from "@material-ui/core/Menu";
import MenuItem from "@material-ui/core/MenuItem";
import Divider from "@material-ui/core/Divider";
import { useStyles } from "./css";
import "./Date.css";
import DatePicker from "react-datepicker";
import moment from "moment";
import "react-datepicker/dist/react-datepicker.css";
import zIndex from "@material-ui/core/styles/zIndex";

export default function CustomizedMenus() {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState(null);

  const [startDate, setStartDate] = useState(new Date());

  const [startDate1, setStartDate1] = useState(new Date());
  const [endDate, setEndDate] = useState(new Date());

  const [openDate, setOpenDate] = useState(false);

  const handleOpenDate = () => {
    setOpenDate(true);
  };

  const handleCloseDate = () => {
    setOpenDate(false);
  };

  const handleClick = event => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const ExampleCustomInput = ({ value, onClick }) => (
    <button
      className="example-custom-input btn-block"
      onClick={onClick}
      style={{
        width: "98%",
        textAlign: "left",
        padding: " 10px 10px 10px 10px",
        fontSize: "15px",
        zIndex: "9999",
        backgroundColor: "#fff",
        color: "grey",
        borderBottom: "1px solid grey",
        borderLeft: "1px solid grey",
        borderRight: "1px solid grey",
        borderTop: "1px solid grey"
      }}
    >
      {/* {!value ? "Select Date" : value} */}
      {value}
    </button>
  );

  return (
    <div>
      <div className="customDatePickerWidth">
        <DatePicker
          selected={startDate}
          onChange={date => setStartDate(date)}
          dateFormat="dd/MM/yyyy"
          closeOnScroll={true}
          customInput={<ExampleCustomInput />}
          popperModifiers={{
            preventOverflow: {
              enabled: true
            }
          }}
        />
      </div>
    </div>
  );
}
