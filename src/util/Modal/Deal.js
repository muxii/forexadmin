import React, { useState, useEffect,useRef } from 'react';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import './app.css';
import {useStyles} from './css'
import _ from 'lodash';
import { getInstrumentByIdAPI, makeDealAPI } from '../Service/Service';
import { Accordion, AccordionDetails, AccordionSummary, Button, FormControl, Grid, InputLabel, MenuItem, Select, TextField, Typography } from '@material-ui/core';
export const DealForm = function DealForm(props) {
    const [activeIndex, setActiveIndex] = useState(0);
    const [selectedInstrumentId, setSelectedInstrumentId] = useState(props.data && props.data.length > 0 && props.data[0].id);
    const [selectedInstrumentName, setSelectedInstrumentName] = useState(props.data && props.data.length > 0 && props.data[0].name);
    const [choosenInstrument, setChoosenInstrument] = useState(props.data && props.data.length > 0 && props.data[0]);
    const [instrument, setInstrument] = useState(props.data);
    const [stopLossStatus,setStopLossStatus]=useState(true);
    const [takeProfitStatus,setTakeProfitStatus]=useState(true);
    const [maxLotSize,setMaxLotSize]=useState(1);
  
    
    const [stopLossAmountVal,setStopLossAmountVal]=useState(0);
    const [stopLossPriceVal,setStopLossPriceVal]=useState(0);

    const [takeProfitAmountVal,setTakeProfitAmountVal]=useState(0);
    const [takeProfitPriceVal,setTakeProfitPriceVal]=useState(0);

    const [lotSize,setLotSize]=useState(props.data && props.data.length > 0 && props.data[0].min_deal_size);
    const [minLotSize,setMinLotSize]=useState(props.data && props.data.length > 0 && props.data[0].min_deal_size);
    const [open, setOpen] = useState(false);
    const [stopLossAmount,setStopLossAmount]=useState(props.data && props.data.length > 0 && props.data[0].min_stop_loss_amount );
    const [stopLossPrice,setStopLossPrice]=useState(props.data && props.data.length > 0 && props.data[0].min_stop_loss_price);
    const [takeProfitAmount,setTakeProfitAmount]=useState(props.data && props.data.length > 0 && props.data[0].min_take_profit_amount);
    const [takeProfitPrice,setTakeProfitPrice]=useState(props.data && props.data.length > 0 && props.data[0].min_take_profit_price);
    const [stopLossPriceCounter,setStopLossPriceCounter]=useState(0);
    const [takeProfitPriceCounter,setTakeProfitPriceCounter]=useState(0);
    const [dealDataVal,setDealDataVal]=useState({});
    const [call,setCall]=useState("SELL");
    const lotSizeRef = useRef(null);
    const stopLossAmtRef=useRef(null);
    const stopLossPriceRef = useRef(null);
    const takeProfitAmtRef=useRef(null);
    const takeProfitPriceRef=useRef(null);

    function addStopLossAmount(e){
        setStopLossAmountVal(stopLossAmountVal+1);
        let decimalPre=decimalPlaces(stopLossPrice)
        if(call==="SELL"){
        if(decimalPre===2){
            setStopLossPriceVal(stopLossPriceVal+(100/100));
            
        }
        else if(decimalPre===3){
            setStopLossPriceVal(stopLossPriceVal+(100/1000));
        }else if(decimalPre===4){
            setStopLossPriceVal(stopLossPriceVal+(100/10000));
            
        }
        else if(decimalPre===5){
            setStopLossPriceVal(stopLossPriceVal+(100/100000));
            
        }
        else if(decimalPre===6){
            setStopLossPriceVal(stopLossPriceVal+(100/1000000));
            
        }
        else if(decimalPre===7){
            setStopLossPriceVal(stopLossPriceVal+(100/10000000));
            
        }
    }else{
        if(decimalPre===2){
            setStopLossPriceVal(stopLossPriceVal-(100/100));
            
        }
        else if(decimalPre===3){
            setStopLossPriceVal(stopLossPriceVal-(100/1000));
        }else if(decimalPre===4){
            setStopLossPriceVal(stopLossPriceVal-(100/10000));
            
        }
        else if(decimalPre===5){
            setStopLossPriceVal(stopLossPriceVal-(100/100000));
            
        }
        else if(decimalPre===6){
            setStopLossPriceVal(stopLossPriceVal-(100/1000000));
            
        }
        else if(decimalPre===7){
            setStopLossPriceVal(stopLossPriceVal-(100/10000000));
            
        }
    }
        console.log(stopLossPrice+stopLossPriceVal);

    }

    function decimalPlaces(num) {
        var match = (''+num).match(/(?:\.(\d+))?(?:[eE]([+-]?\d+))?$/);
        if (!match) { return 0; }
        return Math.max(
             0,
             // Number of digits right of decimal point.
             (match[1] ? match[1].length : 0)
             // Adjust for scientific notation.
             - (match[2] ? +match[2] : 0));
      }

    function subtractStopLossAmount(e){
        if(stopLossAmountVal<1){
            alert("Value Cannot be zero or less!!")
            return;
        }

        setStopLossAmountVal(stopLossAmountVal-1);
        let decimalPre=decimalPlaces(stopLossPrice)
       
        if(call==="SELL"){
        if(decimalPre===2){
            setStopLossPriceVal(stopLossPriceVal-(100/100));
            
        }
        else if(decimalPre===3){
            setStopLossPriceVal(stopLossPriceVal-(100/1000));
        }else if(decimalPre===4){
            setStopLossPriceVal(stopLossPriceVal-(100/10000));
            
        }
        else if(decimalPre===5){
            setStopLossPriceVal(stopLossPriceVal-(100/100000));
            
        }
        else if(decimalPre===6){
            setStopLossPriceVal(stopLossPriceVal-(100/1000000));
            
        }
        else if(decimalPre===7){
            setStopLossPriceVal(stopLossPriceVal-(100/10000000));
            
        }
    }else{
        if(decimalPre===2){
            setStopLossPriceVal(stopLossPriceVal+(100/100));
            
        }
        else if(decimalPre===3){
            setStopLossPriceVal(stopLossPriceVal+(100/1000));
        }else if(decimalPre===4){
            setStopLossPriceVal(stopLossPriceVal+(100/10000));
            
        }
        else if(decimalPre===5){
            setStopLossPriceVal(stopLossPriceVal+(100/100000));
            
        }
        else if(decimalPre===6){
            setStopLossPriceVal(stopLossPriceVal+(100/1000000));
            
        }
        else if(decimalPre===7){
            setStopLossPriceVal(stopLossPriceVal+(100/10000000));
            
        }
    }
        console.log(stopLossPrice+stopLossPriceVal);


    }
    
    function addStopLossPrice(e){
        let decimalPre=decimalPlaces(stopLossPrice)
        if(call==="SELL"){
            

        if(decimalPre===2){
            setStopLossPriceVal(stopLossPriceVal+(10/100));
            
        }
        else if(decimalPre===3){
            setStopLossPriceVal(stopLossPriceVal+(10/1000));
        }else if(decimalPre===4){
            setStopLossPriceVal(stopLossPriceVal+(10/10000));
            
        }
        else if(decimalPre===5){
            setStopLossPriceVal(stopLossPriceVal+(10/100000));
            
        }
        else if(decimalPre===6){
            setStopLossPriceVal(stopLossPriceVal+(10/1000000));
            
        }
        else if(decimalPre===7){
            setStopLossPriceVal(stopLossPriceVal+(10/10000000));
            
        }
    }else{
        
        if(decimalPre===2){
            setStopLossPriceVal(stopLossPriceVal-(10/100));
            
        }
        else if(decimalPre===3){
            setStopLossPriceVal(stopLossPriceVal-(10/1000));
        }else if(decimalPre===4){
            setStopLossPriceVal(stopLossPriceVal-(10/10000));
            
        }
        else if(decimalPre===5){
            setStopLossPriceVal(stopLossPriceVal-(10/100000));
            
        }
        else if(decimalPre===6){
            setStopLossPriceVal(stopLossPriceVal-(10/1000000));
            
        }
        else if(decimalPre===7){
            setStopLossPriceVal(stopLossPriceVal-(10/10000000));
            
        }
    }
        console.log(stopLossPrice+stopLossPriceVal);
        if(stopLossPriceCounter!==0  && stopLossPriceCounter%100===0){
            setStopLossAmountVal(stopLossAmountVal+1);
        }
        setStopLossPriceCounter(stopLossPriceCounter+10);
        

    }

    function subtractStopLossPrice(e){
        let decimalPre=decimalPlaces(stopLossPrice)
        
        if(call==="SELL"){
            if((stopLossPrice+stopLossPriceVal)<=stopLossPrice){
        
                return;
            }

        if(decimalPre===2){
            setStopLossPriceVal(stopLossPriceVal-(10/100));
            
        }
        else if(decimalPre===3){
            setStopLossPriceVal(stopLossPriceVal-(10/1000));
        }else if(decimalPre===4){
            setStopLossPriceVal(stopLossPriceVal-(10/10000));
            
        }
        else if(decimalPre===5){
            setStopLossPriceVal(stopLossPriceVal-(10/100000));
            
        }
        else if(decimalPre===6){
            setStopLossPriceVal(stopLossPriceVal-(10/1000000));
            
        }
        else if(decimalPre===7){
            setStopLossPriceVal(stopLossPriceVal-(10/10000000));
            
        }
    }else{
        if((stopLossPrice+stopLossPriceVal)>=stopLossPrice){
            return;
        }
        if(decimalPre===2){
            setStopLossPriceVal(stopLossPriceVal+(10/100));
            
        }
        else if(decimalPre===3){
            setStopLossPriceVal(stopLossPriceVal+(10/1000));
        }else if(decimalPre===4){
            setStopLossPriceVal(stopLossPriceVal+(10/10000));
            
        }
        else if(decimalPre===5){
            setStopLossPriceVal(stopLossPriceVal+(10/100000));
            
        }
        else if(decimalPre===6){
            setStopLossPriceVal(stopLossPriceVal+(10/1000000));
            
        }
        else if(decimalPre===7){
            setStopLossPriceVal(stopLossPriceVal+(10/10000000));
            
        }
    }
        console.log(stopLossPrice+stopLossPriceVal);
        if(stopLossPriceCounter!==0  && stopLossPriceCounter%100===0){
            setStopLossAmountVal(stopLossAmountVal-1);
        }
        setStopLossPriceCounter(stopLossPriceCounter-10);
        
    }
    

    function addTakeProfitAmount(e){
        setTakeProfitAmountVal(takeProfitAmountVal+1);
        let decimalPre=decimalPlaces(takeProfitPrice)
        if(call==="SELL"){
        if(decimalPre===2){
            setTakeProfitPriceVal(takeProfitPriceVal+(100/100));
            
        }
        else if(decimalPre===3){
            setTakeProfitPriceVal(takeProfitPriceVal+(100/1000));
        }else if(decimalPre===4){
            setTakeProfitPriceVal(takeProfitPriceVal+(100/10000));
            
        }
        else if(decimalPre===5){
            setTakeProfitPriceVal(takeProfitPriceVal+(100/100000));
            
        }
        else if(decimalPre===6){
            setTakeProfitPriceVal(takeProfitPriceVal+(100/1000000));
            
        }
        else if(decimalPre===7){
            setTakeProfitPriceVal(takeProfitPriceVal+(100/10000000));
            
        }
    }else{
        if(decimalPre===2){
            setTakeProfitPriceVal(takeProfitPriceVal-(100/100));
            
        }
        else if(decimalPre===3){
            setTakeProfitPriceVal(takeProfitPriceVal-(100/1000));
        }else if(decimalPre===4){
            setTakeProfitPriceVal(takeProfitPriceVal-(100/10000));
            
        }
        else if(decimalPre===5){
            setTakeProfitPriceVal(takeProfitPriceVal-(100/100000));
            
        }
        else if(decimalPre===6){
            setTakeProfitPriceVal(takeProfitPriceVal-(100/1000000));
            
        }
        else if(decimalPre===7){
            setTakeProfitPriceVal(takeProfitPriceVal-(100/10000000));
            
        }
    }
        console.log(takeProfitPrice+takeProfitPriceVal);


    }

    function subtractTakeProfitAmount(e){

        if(takeProfitAmountVal<1){
            alert("Value Cannot be zero or less!!")
            return;
        }

        setTakeProfitAmountVal(takeProfitAmountVal-1);
        let decimalPre=decimalPlaces(takeProfitPrice)
       
        if(call==="SELL"){
        if(decimalPre===2){
            setTakeProfitPriceVal(takeProfitPriceVal-(100/100));
            
        }
        else if(decimalPre===3){
            setTakeProfitPriceVal(takeProfitPriceVal-(100/1000));
        }else if(decimalPre===4){
            setTakeProfitPriceVal(takeProfitPriceVal-(100/10000));
            
        }
        else if(decimalPre===5){
            setTakeProfitPriceVal(takeProfitPriceVal-(100/100000));
            
        }
        else if(decimalPre===6){
            setTakeProfitPriceVal(takeProfitPriceVal-(100/1000000));
            
        }
        else if(decimalPre===7){
            setTakeProfitPriceVal(takeProfitPriceVal-(100/10000000));
            
        }
    }else{
        if(decimalPre===2){
            setTakeProfitPriceVal(takeProfitPriceVal+(100/100));
            
        }
        else if(decimalPre===3){
            setTakeProfitPriceVal(takeProfitPriceVal+(100/1000));
        }else if(decimalPre===4){
            setTakeProfitPriceVal(takeProfitPriceVal+(100/10000));
            
        }
        else if(decimalPre===5){
            setTakeProfitPriceVal(takeProfitPriceVal+(100/100000));
            
        }
        else if(decimalPre===6){
            setTakeProfitPriceVal(takeProfitPriceVal+(100/1000000));
            
        }
        else if(decimalPre===7){
            setTakeProfitPriceVal(takeProfitPriceVal+(100/10000000));
            
        }
    }
        console.log(stopLossPrice+takeProfitPriceVal);



    }

    function addTakeProfitPrice(e){

        let decimalPre=decimalPlaces(takeProfitPrice)
        if(call==="SELL"){
            

        if(decimalPre===2){
            setTakeProfitPriceVal(takeProfitPriceVal+(10/100));
            
        }
        else if(decimalPre===3){
            setTakeProfitPriceVal(takeProfitPriceVal+(10/1000));
        }else if(decimalPre===4){
            setTakeProfitPriceVal(takeProfitPriceVal+(10/10000));
            
        }
        else if(decimalPre===5){
            setTakeProfitPriceVal(takeProfitPriceVal+(10/100000));
            
        }
        else if(decimalPre===6){
            setTakeProfitPriceVal(takeProfitPriceVal+(10/1000000));
            
        }
        else if(decimalPre===7){
            setTakeProfitPriceVal(takeProfitPriceVal+(10/10000000));
            
        }
    }else{
        
        if(decimalPre===2){
            setTakeProfitPriceVal(takeProfitPriceVal-(10/100));
            
        }
        else if(decimalPre===3){
            setTakeProfitPriceVal(takeProfitPriceVal-(10/1000));
        }else if(decimalPre===4){
            setTakeProfitPriceVal(takeProfitPriceVal-(10/10000));
            
        }
        else if(decimalPre===5){
            setTakeProfitPriceVal(takeProfitPriceVal-(10/100000));
            
        }
        else if(decimalPre===6){
            setTakeProfitPriceVal(takeProfitPriceVal-(10/1000000));
            
        }
        else if(decimalPre===7){
            setTakeProfitPriceVal(takeProfitPriceVal-(10/10000000));
            
        }
    }
        console.log(takeProfitPrice+takeProfitPriceVal);
        if(takeProfitPriceCounter!==0  && takeProfitPriceCounter%100===0){
            setTakeProfitAmountVal(takeProfitAmountVal+1);
        }
        setTakeProfitPriceCounter(takeProfitPriceCounter+10);
        


    }


    function subtractTakeProfitPrice(e){
        let decimalPre=decimalPlaces(takeProfitPrice)
        
        if(call==="SELL"){
            if((takeProfitPrice+takeProfitPriceVal)<=takeProfitPrice){
        
                return;
            }

        if(decimalPre===2){
            setTakeProfitPriceVal(takeProfitPriceVal-(10/100));
            
        }
        else if(decimalPre===3){
            setTakeProfitPriceVal(takeProfitPriceVal-(10/1000));
        }else if(decimalPre===4){
            setTakeProfitPriceVal(takeProfitPriceVal-(10/10000));
            
        }
        else if(decimalPre===5){
            setTakeProfitPriceVal(takeProfitPriceVal-(10/100000));
            
        }
        else if(decimalPre===6){
            setTakeProfitPriceVal(takeProfitPriceVal-(10/1000000));
            
        }
        else if(decimalPre===7){
            setTakeProfitPriceVal(takeProfitPriceVal-(10/10000000));
            
        }
    }else{
        if((takeProfitPrice+takeProfitPriceVal)>=takeProfitPrice){
            return;
        }
        if(decimalPre===2){
            setTakeProfitPriceVal(takeProfitPriceVal+(10/100));
            
        }
        else if(decimalPre===3){
            setTakeProfitPriceVal(takeProfitPriceVal+(10/1000));
        }else if(decimalPre===4){
            setTakeProfitPriceVal(takeProfitPriceVal+(10/10000));
            
        }
        else if(decimalPre===5){
            setTakeProfitPriceVal(takeProfitPriceVal+(10/100000));
            
        }
        else if(decimalPre===6){
            setTakeProfitPriceVal(takeProfitPriceVal+(10/1000000));
            
        }
        else if(decimalPre===7){
            setTakeProfitPriceVal(takeProfitPriceVal+(10/10000000));
            
        }
    }
        console.log(takeProfitPrice+takeProfitPriceVal);
        if(takeProfitPriceCounter!==0  && takeProfitPriceCounter%100===0){
            setTakeProfitAmountVal(takeProfitAmountVal-1);
        }
        setTakeProfitPriceCounter(takeProfitPriceCounter-10);
      
    }



    function addLotSize(e){
        if(parseFloat(lotSize)>=parseFloat(maxLotSize)){
            alert("Max Deal Reached!!");
            return;
        }
           setLotSize((parseFloat(lotSize)+0.01).toFixed(2));
        
    }

    function subtractLotSize(e){
        if(parseFloat(lotSize)<=parseFloat(minLotSize)){
            return;
        }
        setLotSize((parseFloat(lotSize)-0.01).toFixed(2));

    }
    

    function handleStopLossStatus(e){
        setStopLossStatus(!e.target.checked);
    }

    function handleTakeProfit(e){
        setTakeProfitStatus(!e.target.checked);
    }

    function dealData(name,value){
        // setDealDataVal({...dealDataVal,[name]:value});

    }
    function handleDeal(e){
        // console.log(lotSizeRef.current.value);
        // console.log(stopLossAmtRef.current.value);
        let dealCost =choosenInstrument.required_margin*(lotSize*100)
        if(dealCost>props.customer.practice_wallet)
        {
            alert("You Don't Have Required Margin!!!")
            return;
        }


        let dealRequest = {
            lotSize:lotSizeRef.current.value,
            stopLossAmount:stopLossAmtRef.current.value,
            stopLossPrice:stopLossPriceRef.current.value,
            takeProfitAmount:takeProfitAmtRef.current.value,
            takeProfitPrice:takeProfitPriceRef.current.value,
            dealCall:call,
            stopLossStatus:!stopLossStatus,
            takeProfitStatus:!takeProfitStatus,
            instrumentId:selectedInstrumentId,
            dealCost:dealCost,
            account:'practice',
            bidValue:choosenInstrument.bid_value,
            askValue:choosenInstrument.ask_value
        }
        makeDealAPI(dealRequest,sessionStorage.getItem('acc_no')).then((res)=>{
            console.log(res);
            props.close();
        }).catch((err)=>{
            console.log(err);
            props.close();
        })
    }

    // useEffect(()=>{
    //     getInstrumentByIdAPI(selectedInstrumentId).then((res)=>{
    //         console.log(res.data);
    //     }).catch((err)=>{
    //         console.log(err);
    //     })

    // },[selectedInstrumentId])



    // console.log(props.data);
    function handleAccordion(e, titleProps) {
        const { index } = titleProps
        const newIndex = activeIndex === index ? -1 : index
        setActiveIndex(newIndex);
    }

    function closeInterval() {

    }

    function chooseInstrument(e) {
        var instru = _.find(props.data, ['name', e.target.value]);
        setSelectedInstrumentId(instru.id);
        if (props.myInterval) {
            clearInterval(props.myInterval);
            // console.log("interval Cleared!!")
        }
        let myInter = setInterval(() => {
            getInstrumentByIdAPI(instru.id).then((res) => {
                setChoosenInstrument(res.data);
                // setLotSize(res.data.min_deal_size);
                // setMinLotSize(res.data.min_deal_size);
                setStopLossAmount(res.data.min_stop_loss_amount);
                setStopLossPrice(res.data.min_stop_loss_price);
                setTakeProfitPrice(res.data.min_take_profit_price);
                setTakeProfitAmount(res.data.min_take_profit_amount);
                
            
            }).catch((err) => {
                console.log(err);
            })

        }, 100);
        props.setMyInterval(myInter);
        setTakeProfitAmountVal(0);
        setTakeProfitPriceVal(0);
        setStopLossPriceVal(0);
        setStopLossAmountVal(0);
        setStopLossPriceCounter(0);
        setTakeProfitPriceCounter(0);
    
    }

   
    function sellCall(e)
    {
        setCall("SELL");
        setTakeProfitAmountVal(0);
        setTakeProfitPriceVal(0);
        setStopLossAmountVal(0);
        setStopLossPriceVal(0);
        setStopLossPriceCounter(0);
        setTakeProfitPriceCounter(0);
    }

    function buyCall(e){
        setCall("BUY");
        
        setTakeProfitAmountVal(0);
        setTakeProfitPriceVal(0);
        setStopLossAmountVal(0);
        setStopLossPriceVal(0);
        setStopLossPriceCounter(0);
        setTakeProfitPriceCounter(0);
    }
const classes=useStyles();
    // console.log(props.data);
    const handleClose = () => {
        setOpen(false);
      };
    
      const handleOpen = () => {
        setOpen(true);
      };
    return (
        <>
            
            <Grid container >
                    <Grid item xs={12} sm={12} md={4} lg={4}>
                                    <div>Instrument</div>
                                    <FormControl className={classes.formControl}>
                                       
                                            <Select
                                            labelId="demo-controlled-open-select-label"
                                            id="demo-controlled-open-select"
                                            open={open}
                                            onChange={chooseInstrument} 
                                            defaultValue={selectedInstrumentId} 
                                            onClose={handleClose}
                                            className={classes.inputSize}

                                            onOpen={handleOpen}
                                            value={selectedInstrumentId}
                                            
                                            >
                                                <MenuItem value="">
                                                <em>None</em>
                                                </MenuItem>
                                                {props.data && props.data.map((obj, key) => {
                                                return(
                                                <MenuItem key={key} value={obj.name}>{obj.name}</MenuItem>
                                                )
                                                })}
                                        </Select>
                                    </FormControl>
                                    {/* <select name="instrument" onChange={chooseInstrument} defaultValue={selectedInstrumentId} className="ui dropdown fluid"> */}
                                        {/* {
                                            props.data && props.data.map((obj, key) => {
                                                return (<option key={key} value={obj.name}>{obj.name}</option>)
                                            })
                                        } */}
                                    {/* </select> */}
                                
                        </Grid>
                        

                        <Grid item xs={12} sm={12} md={4} lg={4}>
                           
                               
                                    <label> Lot Size</label>                                   
                                        {/* <input type="number" ref={lotSizeRef} disabled={true}  name="lotSize" placeholder="Enter Lot Size" value={lotSize}/> */}                
                                        <form className={classes.root} noValidate autoComplete="off">
                                        <TextField type="number" 
                                            ref={lotSizeRef} 
                                            // disabled={true}  
                                            name="lotSize" 
                                            placeholder="Enter Lot Size" 
                                            value={lotSize}
                                            className={classes.lotSize}
                                            id="standard-basic"  />
                                            
                                            <Button onClick={addLotSize} className={classes.btn}>+</Button>
                                            <Button onClick={subtractLotSize} className={classes.btn}>-</Button>
                                        </form>                               
                        </Grid>

                        <Grid item xs={12} sm={12} md={4} lg={4}>
                            <div 
                            // className={choosenInstrument.current_status === -1 ? "ui red huge buttons" : choosenInstrument.current_status === 1 ? "ui green huge buttons" : "ui huge buttons"}
                            >
                                <Button 
                                style={{padding:'10px'}}
                                // onClick={sellCall} 
                                // className={call && call==="SELL"?"ui active button" :"ui button"}
                                >
                                    <h2 inverted>Sell</h2>
                                    {/* <h3>{choosenInstrument.bid_value}</h3> */}
                                </Button>
                                <Button 
                                style={{padding:'10px'}} 
                                // onClick={buyCall} 
                                // className={call && call==="BUY"?"ui active button" :"ui button"}
                                >

                                    <h2 inverted>Buy</h2>
                                    {/* <h3>{choosenInstrument.ask_value}</h3> */}

                                </Button>
                            </div>
                        </Grid>


                   
                        <Grid item xs={12} sm={12} md={12} lg={12}>
                            <Accordion >
                                <AccordionSummary 
                                 expandIcon={<ExpandMoreIcon />}
                                    // active={activeIndex === 0}
                                    index={0}
                                    // onClick={handleAccordion}
                                >
                                    <AccordionDetails>
                                    <Typography>
                                    <Grid  >
                                        <Grid>
                                            <Grid>
                                                <p>Add Stop Loss Or Take Profit</p>
                                            </Grid>
                                            <Grid>
                                                {/* <Icon name='dropdown' /> */}

                                            </Grid>

                                        </Grid>
                                    </Grid>
                                    </Typography>
                                </AccordionDetails>

                                </AccordionSummary>
                                <AccordionSummary 
                                // active={activeIndex === 0}
                                >
                                    <Grid >
                                        <Grid>
                                            <Grid >
                                                <Grid >
                                                    <Grid>
                                                        <Grid >
                                                            <div className="ui checkbox">
                                                                <input 
                                                                // onChange={handleStopLossStatus}
                                                                 type="checkbox" name="stopLossStatus" />
                                                                <label style={{color:"black", fontWeight:"900"}}>ADD STOP LOSS</label>
                                                            </div>
                                                        </Grid>
                                                    </Grid>
                                                    <Grid>
                                                        <Grid item lg={3}>
                                                            <div className="ui form">
                                                                <div className="field">
                                                                    <label>Amount in $</label>
                                                                    <div className="ui input fluid">

                                                                        <input 
                                                                        // disabled={true} 
                                                                        // ref={stopLossAmtRef}  
                                                                        type="number" 
                                                                        name="stopLossAmount"  
                                                                        // value={stopLossAmount+stopLossAmountVal}
                                                                         />
                                                                        <div className="ui buttons">
                                                                            <Button 
                                                                              style={{backgroundColor:"red"}}
                                                                            // disabled={stopLossStatus}  
                                                                            // onClick={addStopLossAmount} 
                                                                            // className="ui button primary"
                                                                            >+</Button>
                                                                            <Button
                                                                            style={{backgroundColor:"red"}}
                                                                            // disabled={stopLossStatus} 
                                                                            // onClick={subtractStopLossAmount} 
                                                                            // className="ui button"
                                                                            >-</Button>
                                                                        </div>

                                                                    </div>
                                                                    <label>Above $
                                                                        {/* {choosenInstrument.min_stop_loss_amount} */}
                                                                        </label>

                                                                </div>
                                                            </div>
                                                        </Grid>

                                                        <Grid >
                                                            <h4>OR</h4>
                                                        </Grid>
                                                        <Grid >
                                                            {/* <div className="ui form">
                                                                <div className="field">
                                                                    <label>Rate</label>
                                                                    <div className="ui input fluid">

                                                                        <input disabled={true} ref={stopLossPriceRef}  name="stopLossPrice" type="number" value={stopLossPrice+stopLossPriceVal}/>
                                                                        <div className="ui buttons">
                                                                            <button disabled={stopLossStatus} onClick={addStopLossPrice} className="ui button primary">+</button>
                                                                            <button disabled={stopLossStatus} onClick={subtractStopLossPrice} className="ui button">-</button>
                                                                        </div>

                                                                    </div>
                                                                     <label>{call && call==="BUY"?"Below  "+choosenInstrument.min_stop_loss_price:"Above  "+choosenInstrument.min_stop_loss_price}</label>

                                                                </div>
                                                            </div> */}
                                                        </Grid>


                                                    </Grid>

                                                </Grid>

                                            </Grid>
                                            <Grid >
                                                <Grid >
                                                    <Grid>
                                                        <Grid >
                                                            <div className="ui checkbox">
                                                                <input type="checkbox" name="takeProfit" 
                                                                // onChange={handleTakeProfit} 
                                                                />
                                                                <label>ADD TAKE PROFIT</label>
                                                            </div>
                                                        </Grid>
                                                    </Grid>
                                                    <Grid>
                                                        <Grid>
                                                            {/* <div className="ui form">
                                                                <div className="field">
                                                                    <label>Amount in $</label>
                                                                    <div className="ui input fluid">

                                                                        <input readOnly ref={takeProfitAmtRef} disabled={true} type="number" value={takeProfitAmount+takeProfitAmountVal} name="takeProfitAmount" />
                                                                        <div className="ui buttons">
                                                                            <button disabled={takeProfitStatus} onClick={addTakeProfitAmount} className="ui button primary">+</button>
                                                                            <button disabled={takeProfitStatus} onClick={subtractTakeProfitAmount} className="ui button">-</button>
                                                                        </div>

                                                                    </div>
                                                             <label>Above ${choosenInstrument.min_take_profit_amount}</label>

                                                                </div>
                                                            </div> */}
                                                        </Grid>

                                                        <Grid >
                                                            <h1>OR</h1>
                                                        </Grid>
                                                        <Grid >
                                                            {/* <div className="ui form">
                                                                <div className="field">
                                                                    <label>Rate</label>
                                                                    <div className="ui input fluid">

                                                                        <input disabled={true} ref={takeProfitPriceRef} type="number" name="takeProfitPrice" value={takeProfitPrice+takeProfitPriceVal} />
                                                                        <div className="ui buttons">
                                                                            <button disabled={takeProfitStatus} onClick={addTakeProfitPrice} className="ui button primary">+</button>
                                                                            <button  disabled={takeProfitStatus} onClick={subtractTakeProfitPrice} className="ui button">-</button>
                                                                        </div>

                                                                    </div>
                                                                    <label>{call && call==="BUY"?"Below  "+choosenInstrument.min_take_profit_price:"Above  "+choosenInstrument.min_take_profit_price}</label>

                                                                </div>
                                                            </div> */}
                                                        </Grid>


                                                    </Grid>

                                                </Grid>

                                            </Grid>
                                        </Grid>



                                    </Grid>

                                </AccordionSummary>
                            </Accordion>

                        </Grid>
                    
                    
                        <Grid item xs={12} sm={12} md={12} lg={12}>
                            <h2>
                            {/* This Deal Requires ${choosenInstrument.required_margin*(lotSize*100)} from your available Margin!! */}
                            </h2>
                        </Grid>
                        <Grid item xs={12} sm={12} md={12} lg={12}>
                            <Button 
                            // onClick={handleDeal} 
                            primary fluid> Make A Deal</Button>
                        </Grid>
            


                </Grid>
            
        </>
    );

}