import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles(theme => ({
  modalroot: {
    minWidth: "50%"
  },
  button: {
    display: "block",
    marginTop: theme.spacing(2)
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120
  },
  lotSize: {
    width: "50px"
  },
  inputSize: {
    width: "100px"
  },
  btn: {
    width: "10px"
  }
}));
