import React from "react";
import logo from "./logo.svg";
import "./App.css";

import { HashRouter as Router, Route } from "react-router-dom";
function App() {
  return (
    <HashRouter>
      <Router>
        <Route exact path="/" component={Login} />
        <Route exact path="/dashboard" component={Dashboard} />
      </Router>
    </HashRouter>
  );
}

export default App;
