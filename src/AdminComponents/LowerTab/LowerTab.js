import React from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Button from "@material-ui/core/Button";
import Tab from "@material-ui/core/Tab";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Box from "@material-ui/core/Box";
import AppsIcon from "@material-ui/icons/Apps";
import ListIcon from "@material-ui/icons/List";
import ZoomInIcon from "@material-ui/icons/ZoomIn";
import { useStyles } from "./css";
import Table from "../Table/Table";
import Table1 from "../Table1/Table1";

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired
};

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`
  };
}

export default function SimpleTabs(props) {
  const classes = useStyles();
  const [value, setValue] = React.useState(0);

  console.log(props);
  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <Card className={classes.cardContainer}>
      <CardContent>
        <div className={classes.root}>
          <AppBar position="static" className={classes.tabContainer}>
            <Tabs
              value={value}
              onChange={handleChange}
              aria-label="simple tabs example"
            >
              <Tab
                className={classes.tabButton}
                label="Open Orders"
                {...a11yProps(0)}
              />
              <Tab
                className={classes.tabButton}
                label="Pending Orders"
                {...a11yProps(1)}
              />
              <Grid
                justify="space-between" // Add it here :)
                container
                spacing={24}
              >
                <Grid item></Grid>
                <Grid item className={classes.desktopView}>
                  <Button
                    style={{
                      marginTop: "10px",
                      backgroundColor: "#e7505a",
                      color: "white",
                      height: "30px"
                    }}
                  >
                    {" "}
                    NEW DEAL
                  </Button>
                  <Button
                    style={{
                      marginTop: "10px",
                      marginLeft: "5px",
                      backgroundColor: "#e7505a",
                      color: "white",
                      height: "30px"
                    }}
                  >
                    NEW PENDING DEAL
                  </Button>
                  {!props.showfullBottom ? (
                    <Button
                      style={{
                        marginTop: "10px",
                        marginLeft: "5px",
                        backgroundColor: "#e7505a",
                        color: "white",
                        height: "30px"
                      }}
                      onClick={() => props.fullwidthBottom(true, "lower")}
                    >
                      <i class="fa fa-expand" aria-hidden="true"></i>
                    </Button>
                  ) : props.showfullBottom === true ? (
                    <Button
                      style={{
                        marginTop: "10px",
                        marginLeft: "5px",
                        backgroundColor: "#e7505a",
                        color: "white",
                        height: "30px"
                      }}
                      onClick={() => props.fullwidthBottom(false, "lower")}
                    >
                      <i class="fa fa-compress" aria-hidden="true"></i>
                    </Button>
                  ) : (
                    ""
                  )}
                </Grid>
              </Grid>
            </Tabs>
            {/* mobile View start */}
            <Grid justify="space-between" container spacing={24}>
              <Grid item></Grid>
              <Grid item className={classes.mobileView}>
                <Button
                  style={{
                    marginTop: "10px",
                    backgroundColor: "#e7505a",
                    color: "white",
                    height: "30px"
                  }}
                >
                  NEW DEAL
                </Button>
                <Button
                  style={{
                    marginTop: "10px",
                    marginLeft: "5px",
                    backgroundColor: "#e7505a",
                    color: "white",
                    height: "30px"
                  }}
                >
                  NEW PENDING DEAL
                </Button>
              </Grid>
            </Grid>
            {/* mobileView End */}
          </AppBar>
          <TabPanel value={value} index={0}>
            <Table />
          </TabPanel>
          <TabPanel value={value} index={1}>
            <Table1 />
          </TabPanel>
        </div>
      </CardContent>
    </Card>
  );
}
