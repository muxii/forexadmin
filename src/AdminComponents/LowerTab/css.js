import { makeStyles, fade } from "@material-ui/core/styles";
export const drawerWidth = 240;

export const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper
  },
  desktopView: {
    display: "block",
    [theme.breakpoints.down("sm")]: {
      display: "none"
    }
  },
  mobileView: {
    display: "none",
    [theme.breakpoints.down("sm")]: {
      display: "block"
    }
  },
  cardContainer: {
    marginLeft: "20px",
    marginRight: "20px",
    [theme.breakpoints.down("sm")]: {
      marginLeft: "0px",
      marginRight: "0px"
    }
  },
  tabContainer: {
    backgroundColor: "white",
    boxShadow: "1px 1px white"
  },
  tabButton: {
    textTransform: "none",
    color: "grey"
  }
}));
