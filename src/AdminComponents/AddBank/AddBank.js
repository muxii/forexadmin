import React from "react";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import Appbar from "../Appbar/Appbar";
import Appbar1 from "../Appbar1/Appbar1";
import MobileAppbar from "../MobileAppbar/MobileAppbar";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import TextField from "@material-ui/core/TextField";
import Checkbox from "@material-ui/core/Checkbox";
import FormGroup from "@material-ui/core/FormGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import { useStyles } from "./css";
import ListTable from "../ListTable/ListTable";
import { Link } from "react-router-dom";

export default function FullWidthGrid() {
  const classes = useStyles();
  const [checked, setChecked] = React.useState(true);

  const handleChange = event => {
    setChecked(event.target.checked);
  };

  return (
    <div className={classes.root}>
      <Appbar />
      <Appbar1 />
      <MobileAppbar />
      <Grid container spacing={0}>
        <Grid
          item
          xs={12}
          sm={12}
          md={9}
          lg={9}
          className={classes.cardContainer}
        >
          <Card>
            <CardContent>
              <div className={classes.flexContainer}>
                <p className={classes.title}>New Bank Account</p>
                <Link
                  style={{ textDecoration: "none" }}
                  to="/forex4money/admin/bankDetails"
                >
                  <Button className={classes.backButton}>BACK</Button>
                </Link>
              </div>

              <div className={classes.divider}></div>
              <Grid container spacing={0}>
                <Grid item xs={12} sm={12} md={6} lg={6}>
                  <form noValidate autoComplete="off">
                    <label>Nick Name</label> <br />
                    <TextField
                      size="small"
                      fullWidth
                      id="outlined-basic"
                      variant="outlined"
                      placeholder="Use Nick Names to identify your accounts"
                    />
                    <br />
                    <br />
                    <label>Beneficiary Bank Name</label> <br />
                    <TextField
                      size="small"
                      fullWidth
                      id="outlined-basic"
                      variant="outlined"
                      placeholder="Enter Bank Name"
                    />
                    <br />
                    <br />
                    <label>Beneficiary Acccount Number</label> <br />
                    <TextField
                      size="small"
                      fullWidth
                      id="outlined-basic"
                      variant="outlined"
                      placeholder="Enter Account Number"
                    />
                    <br />
                    <br />
                    <label>City</label> <br />
                    <TextField
                      size="small"
                      fullWidth
                      id="outlined-basic"
                      variant="outlined"
                      placeholder="Enter City"
                    />
                    <br />
                    <br />
                    <div>
                      <label>Currency</label>
                      <select className={classes.dropdown}>
                        <option selected>SELECT CURRENCY</option>
                        <option>US Dollar</option>
                      </select>
                    </div>
                  </form>
                </Grid>
                <Grid item xs={12} sm={12} md={6} lg={6}>
                  <form
                    className={classes.secondForm}
                    noValidate
                    autoComplete="off"
                  >
                    <label>Beneficiary Acccount Name</label> <br />
                    <TextField
                      size="small"
                      fullWidth
                      id="outlined-basic"
                      variant="outlined"
                      placeholder="Enter Beneficiary Acccount Name"
                    />
                    <br />
                    <br />
                    <label>Beneficiary Code</label> <br />
                    <TextField
                      size="small"
                      fullWidth
                      id="outlined-basic"
                      variant="outlined"
                      placeholder="Enter Beneficiary Code"
                    />
                    <br />
                    <br />
                    <label>Branch Name</label> <br />
                    <TextField
                      size="small"
                      fullWidth
                      id="outlined-basic"
                      variant="outlined"
                      placeholder="Enter Branch Name"
                    />
                    <br />
                    <br />
                    <label>Country</label>
                    <div>
                      <select className={classes.dropdown}>
                        <option selected>SELECT COUNTRY</option>
                        <option>India</option>
                        <option>India</option>
                      </select>
                    </div>
                  </form>
                </Grid>
              </Grid>
              <br />
              <div className={classes.flexContainer}>
                <p></p>
                <Button align="right" className={classes.submitButton}>
                  SAVE
                </Button>
              </div>
            </CardContent>
          </Card>
        </Grid>
        <Grid item xs={12} sm={12} md={3} lg={3}>
          <ListTable />
        </Grid>
      </Grid>
    </div>
  );
}
