import { makeStyles } from '@material-ui/core/styles';


export const useStyles = makeStyles((theme) => ({

    mainDiv:{
        backgroundColor:'#3c3c3c',
        flexGrow:1,
        padding:"20px"
    },
text:{
    color:'white'
},
bottomdiv:{
    display:"flex",
    alignItems:"center",
    justifyContent:'center',
    backgroundColor:'#252525',
    padding:'10px',
    color:'#BDBDBD',
    fontSize:'12px'
}
}))