import { Grid } from '@material-ui/core';
import React from 'react'
import { useStyles } from './css'

export const LoginFooter = () => {
    const classes=useStyles();
    return (
        <>
        <div className={classes.mainDiv}>
             <Grid container >
             <Grid item xs={12} lg={4} >
                <h3 className={classes.text} >www.profit4money.com</h3> 
                <p className={classes.text}>
                With the most advanced trading technology and services, 
                profit4Money Group has made online Forex trading easy. 
                With us you can enjoy trading conditions which are of the best in the industry.
                </p>  
                <p>
                
                </p> 
            </Grid>
                <Grid item xs={12} lg={4}>
                <h3 className={classes.text}>IMPORTANT LINKS</h3>
                <p className={classes.text}>Registration</p>
                <p className={classes.text}>IB Registration</p>
                <p className={classes.text}>Referral Program</p>
                <p className={classes.text}>Introducing broker</p>
                <p className={classes.text}>Making Profit in Forex</p>
                </Grid>
                <Grid item xs={12} lg={4}>
                <h3 className={classes.text}>CONTACT US</h3>
                <p className={classes.text}>Phone: 
                <p>
                
                </p>
                </p>
                <p className={classes.text}>Email: 
        
                </p>
                
                </Grid>
                
             </Grid>
             
        </div>
        <Grid item xs={12} lg={12} >
        <div className={classes.bottomdiv}>
                © Copyright 2020 www.profit4money.com , All Rights Reserved
        </div>
        </Grid>
        </>
    )
}
