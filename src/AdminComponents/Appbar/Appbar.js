import React from "react";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import MenuItem from "@material-ui/core/MenuItem";
import Menu from "@material-ui/core/Menu";
import KeyboardArrowDownIcon from "@material-ui/icons/KeyboardArrowDown";
import { Link } from "react-router-dom";
import { useStyles } from "./css";

export default function PrimarySearchAppBar() {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [anchorE2, setAnchorE2] = React.useState(null);

  const handleClick1 = event => {
    setAnchorEl(event.currentTarget);
  };
  const handleClick2 = event => {
    setAnchorE2(event.currentTarget);
  };

  const handleClose1 = () => {
    setAnchorEl(null);
  };
  const handleClose2 = () => {
    setAnchorE2(null);
  };
  const logout = () => {
    setAnchorE2(null);
    sessionStorage.clear();
  };
  return (
    <div className={classes.desktopView}>
      <div className={classes.grow}>
        <AppBar position="static" className={classes.appbarContainer}>
          <Toolbar>
            <Typography
              edge="start"
              className={classes.title}
              variant="h6"
              noWrap
            >
              Forex4Money
            </Typography>

            <div className={classes.grow} />
            <div style={{ display: "flex" }}>
              <Link
                style={{ textDecoration: "none" }}
                to="/forex4money/admin/demo/dashboard"
              >
                <Button className={classes.switchButton}>
                  SWITCH TO DEMO ACCOUNT
                </Button>
              </Link>
              <Link
                style={{ textDecoration: "none" }}
                to="/forex4money/admin/live/dashboard"
              >
                <Button className={classes.dashboardButton}>Dashboard</Button>
              </Link>
              <div>
                <Button
                  className={classes.dropdown}
                  aria-controls="simple-menu"
                  aria-haspopup="true"
                  onClick={handleClick1}
                >
                  Tools <KeyboardArrowDownIcon />
                </Button>
                <Menu
                  style={{ marginTop: "30px" }}
                  id="simple-menu"
                  anchorEl={anchorEl}
                  keepMounted
                  open={Boolean(anchorEl)}
                  onClose={handleClose1}
                >
                  <Link
                    to="/forex4money/admin/livecharts"
                    className={classes.link}
                  >
                    <MenuItem onClick={handleClose1}>
                      <i class="fa fa-angle-right" aria-hidden="true"></i>
                      &nbsp; Live Rates & Chart
                    </MenuItem>
                  </Link>
                  <Link
                    to="/forex4money/admin/economic"
                    className={classes.link}
                  >
                    <MenuItem onClick={handleClose1}>
                      <i class="fa fa-angle-right" aria-hidden="true"></i>
                      &nbsp; Economic Calender
                    </MenuItem>
                  </Link>
                </Menu>
              </div>
              <div>
                <Button
                  className={classes.dropdown}
                  aria-controls="simple-menu"
                  aria-haspopup="true"
                  onClick={handleClick2}
                >
                  dinesh yadav <KeyboardArrowDownIcon />
                </Button>
                <Menu
                  style={{ marginTop: "30px" }}
                  id="simple-menu"
                  anchorEl={anchorE2}
                  keepMounted
                  open={Boolean(anchorE2)}
                  onClose={handleClose2}
                >
                  <Link
                    style={{ color: "black", textDecoration: "none" }}
                    to="/forex4money/admin/activityLog"
                  >
                    <MenuItem onClick={handleClose2}>
                      <i class="fa fa-angle-right" aria-hidden="true"></i>
                      &nbsp; Activity Log
                    </MenuItem>
                  </Link>
                  <Link
                    style={{ color: "black", textDecoration: "none" }}
                    to="/forex4money/admin/profile"
                  >
                    <MenuItem onClick={handleClose2}>
                      <i class="fa fa-angle-right" aria-hidden="true"></i>
                      &nbsp;Profile
                    </MenuItem>
                  </Link>
                  <Link
                    to="/forex4money/admin/documents"
                    className={classes.link}
                  >
                    <MenuItem onClick={handleClose2}>
                      <i class="fa fa-angle-right" aria-hidden="true"></i>
                      &nbsp;Documents
                    </MenuItem>
                  </Link>
                  <Link
                    style={{ color: "black", textDecoration: "none" }}
                    to="/forex4money/admin/bankDetails"
                  >
                    <MenuItem onClick={handleClose2}>
                      <i class="fa fa-angle-right" aria-hidden="true"></i>
                      &nbsp;Bank Details
                    </MenuItem>
                  </Link>
                  <Link
                    style={{ color: "black", textDecoration: "none" }}
                    to="/forex4money/admin/changePassword"
                  >
                    <MenuItem onClick={handleClose2}>
                      <i class="fa fa-angle-right" aria-hidden="true"></i>
                      &nbsp;Change Password
                    </MenuItem>
                  </Link>
                  <Link
                    style={{ color: "black", textDecoration: "none" }}
                    to="/forex4money/admin/login"
                  >
                    <MenuItem onClick={logout}>
                      <i class="fa fa-angle-right" aria-hidden="true"></i>
                      &nbsp;Log Out
                    </MenuItem>
                  </Link>
                </Menu>
              </div>
            </div>
          </Toolbar>
        </AppBar>
      </div>
    </div>
  );
}
