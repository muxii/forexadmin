import { makeStyles, fade } from "@material-ui/core/styles";
export const drawerWidth = 240;

export const useStyles = makeStyles(theme => ({
  grow: {
    flexGrow: 1
  },
  menuButton: {
    marginRight: theme.spacing(2)
  },
  title: {
    fontSize: "15px",
    fontWeight: "bold",
    marginTop: "-30px"
  },
  appbarContainer: {
    backgroundColor: "#e7505a",
    color: "white",
    height: "30px"
  },
  Buttons: {
    textTransform: "none",
    color: "white",
    marginRight: "10px",
    fontWeight: "bold",
    fontSize: "13px",
    marginTop: "-30px"
  },
  dropdown: {
    textTransform: "none",
    color: "white",
    marginRight: "10px"
  },
  desktopView: {
    display: "block",
    [theme.breakpoints.down("sm")]: {
      display: "none"
    }
  }
}));
