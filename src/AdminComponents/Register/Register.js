import {
  Button,
  FormControl,
  Grid,
  InputAdornment,
  MenuItem,
  Select,
  Snackbar,
  TextField
} from "@material-ui/core";
import MuiAlert from '@material-ui/lab/Alert';
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import Divider from "@material-ui/core/Divider";
import InboxIcon from "@material-ui/icons/Inbox";
import React, { useState } from "react";
import CheckCircleOutlineIcon from "@material-ui/icons/CheckCircleOutline";
import { useStyles } from "./css";
import { LoginFooter } from "../LoginFooter/LoginFooter";
import { Link, Redirect } from "react-router-dom";
import FormGroup from '@material-ui/core/FormGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import UpperTab from "./UpperTab/UpperTab";
import './css'
import { COUNTRYLIST, LEVERAGE, Packages } from "../../util/util";
import Login from "../Login/Login";
import { registerCustomerAPI } from "../../util/Service/Service";

function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
  }
  
const Register = () => {
  const classes = useStyles();
  
  const [value, setValue] = useState("");
  const [error,setError]=useState(false);
  const [errorMsg,setErrorMsg]=useState(false);
  const [first_name,setFirst_name]=useState('')
     
 const [last_name,setLast_name]= useState('')
 const [email_address,setEmail_address]=useState('')
 const [password,setPassword]=useState('') 
 const [confirm_password,setConfirm_Password]=useState('')
 const [street_address,setStreet_Address]=useState('')
 const [pincode,setPincode]=useState('')
 const [city,setCity]=useState('')
 const [CountryState,setCountryState]=useState('')
 const [country,setCountry]=useState('')
 const [Package,setPackage]=useState('')
 const [leverage,setLeverage]=useState('')
 const [errorSetting,setErrorSetting]=useState(false)
 const [dob,setDob]=useState('');
 const [contact_no,setContact_No]=useState('')
 const [redirectToLogin,setRedirectToLogin]=useState(false)
 const [contactError,setContactError]=useState(false)
 const [errortellus,setErrorTellUs]=useState('')
 const [modalMsg,setModalMsg]=useState('')
 const [opencontact,setOpenContact]=useState(false)
 const [errorModal,setErrorModal]=useState(false);
 const [successModal,setSuccessModal]=useState(false)
  const [state, setState] = useState({
    checkedA: false,
    checkedB: false,
  });
  const [open, setOpen] = React.useState(false);

  const handleClick = () => {
    setOpen(true);
  };

  const handleClose = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }

    setOpen(false);
  };
  const handleCloseContact = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }

    setOpenContact(false);
  };
  const handleCloseError = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }

setErrorModal(false);
  };
  const handleCloseSuccess = (event, reason) => {
    if (reason === 'clickaway') {
      return;
    }

setSuccessModal(false);
  };

  const handleChange = (event) => {
    setState({ ...state, [event.target.name]: event.target.checked });
  };

  const handleInput=(e)=>{
      console.log({[e.target.name]:e.target.value});
    
      if(e.target.name==="first_name"){
        setFirst_name(e.target.value)
    }
    else if(e.target.name==="last_name"){
        setLast_name(e.target.value)
    }
    else if(e.target.name==="email_address"){
        setEmail_address(e.target.value)
    }
    else if(e.target.name==="password"){
        setPassword(e.target.value)
    }
    else if(e.target.name==="confirm_password"){
        setConfirm_Password(e.target.value)
        
    }
    else if(e.target.name==="street_address"){
        setStreet_Address(e.target.value)
        
    }else if(e.target.name==="pincode"){
        setPincode(e.target.value)
        
    }else if(e.target.name==="city"){
        setCity(e.target.value)
        
    }else if(e.target.name==="state"){
        setCountryState(e.target.value)
        
    }else if(e.target.name==="country"){
        setCountry(e.target.value)
        
    }else if(e.target.name==="dob"){
        setDob(e.target.value)
        
    }else if(e.target.name==="contact_no"){
        setContact_No(e.target.value)
        
        
    }
    else if(e.target.name==="package"){
        setPackage(e.target.value)
        
    }else if(e.target.name==="leverage"){
        setLeverage(e.target.value)
        
    }
   
  }
  const handCheckPassword=()=>{
    if(password===confirm_password){
        setErrorMsg(true);
        console.log("error Message")
    }
    else{
        setErrorMsg(false)
    }
  }
const settingSubmit=(value)=>{
    console.log("hello",Package,leverage)
if(Packages!==''
&& leverage!==''){
    setValue("accountsettings")
    // setErrorSetting(false)
    const data={
    "first_name":first_name, 
    "last_name":last_name,
    "email_address":email_address,
    "contact_no":contact_no,
    "password":password,
    "confirm_password":confirm_password,
    "dob":dob,
    "street_address":street_address,
    "pincode":pincode,
    "city":city,
    "state":CountryState,
    "country":country,
    "package":Package,
    "leverage":leverage
    }
    registerCustomerAPI(data).then((res)=>{
        console.log(res)
        if(res.data.msg==="success"){
            setSuccessModal(true)
            setErrorModal(false);
            setRedirectToLogin(true);
            
        }
        else{
            setErrorModal(true)
        }
    }).catch(err =>{
        if(err){
            setErrorModal(true)
            console.log(err.response.data)
            setModalMsg(err.response.data)
        }
    })
}
else{
    console.log("else here")
    setValue("accountsettings")
    setErrorSetting(true);
}
}
 const lastGrid=(value)=>{
    if(
      street_address!==''
    && pincode!==''
    && city!==''
    && CountryState!==''
    && country!==''
    && dob!==''
    && contact_no!==''&& contact_no.length===10){
        setValue(value)
    }
    else if(street_address===''
    || pincode===''
    || city===''
    || CountryState===''
    || country===''
    || dob===''
    || contact_no===''|| 
    contact_no.length===''){
      setErrorTellUs(true)
      if(errortellus && contact_no.length>10||contact_no.length<10)
      
      setOpenContact(true);
      setContactError(true)
  }
    
  else if(contact_no.length>10||contact_no.length<10){
        setOpenContact(true);
        setContactError(true)
        
    }
    
 }
  const setContinue = value => {
    console.log(first_name,last_name,password,confirm_password,email_address,)
      handCheckPassword();

      if(
        first_name!=='' 
        && last_name!==''
        && email_address!==''
        && password!==''
        && confirm_password!==''
        && confirm_password===password
         
    )
    {
    setValue(value);
}
    else{
        setError(true);
        if(confirm_password===password)
        return;
        else
        setErrorMsg(true);
        setOpen(true);
        setError(false);
    }
  };
  console.log(dob);
  if(redirectToLogin){
      return(<Redirect to="login"/>)
  }

  return (
    <>
     {!errorModal
     &&
        successModal
        &&
        <Snackbar open={successModal} autoHideDuration={3000} onClose={handleCloseSuccess}>
        <Alert onClose={handleCloseSuccess} severity="success">
         SuccessFully Created Account!!
        </Alert>
      </Snackbar>
    }
    {
         errorModal
        &&
        <Snackbar open={errorModal} autoHideDuration={3000} onClose={handleCloseError}>
        <Alert onClose={handleCloseError} severity="error">
         {modalMsg}
        </Alert>
      </Snackbar>
    }
    {
        contactError && !errortellus
        &&
        <Snackbar open={opencontact} autoHideDuration={3000} onClose={handleCloseContact}>
        <Alert onClose={handleCloseContact} severity="error">
         Mobile Number should be exact 10 Digits
        </Alert>
      </Snackbar>
    }
    {
       !error && errorMsg &&
        <Snackbar open={open} autoHideDuration={3000} onClose={handleClose}>
        <Alert onClose={handleClose} severity="error">
          Incorrect Confirm Password!! 
        </Alert>
      </Snackbar>
    }
      <div className={classes.root}>
        <Grid container>
          <Grid item xs={12} className={classes.headerbackground}>
            <div className={classes.headerbackground}>
              <div>
                <h2 className={classes.logo}>www.profit4money.com</h2>
              </div>
            </div>
          </Grid>
        </Grid>
        <div>
          <UpperTab />
        </div>
        {value === ""   &&(
          <div className={classes.formdiv}>
            <Grid container>
              <Grid item lg={6} xs={12} md={6} className={classes.Login}>
                <Grid
                  container
                  direction="row"
                  justify="center"
                  alignItems="center"
                >
                  {/* <HorizontalNonLinearStepper/> */}

                  <FormControl fullWidth className={classes.LoginForm}>
                    <h2 className={classes.LoginText}>
                      <b>CREATE</b>
                      <b style={{ color: "#f24c5c" }}>FOREX4MONEY PROFILE</b>
                    </h2>
                    <div className={classes.inputDiv}>
                      <TextField
                        id="standard-basic"
                       
                        type="text"
                        name="first_name"
                        onChange={handleInput}
                        placeholder="Enter First Name"
                        className={classes.textfield}
                        value={first_name}
                        
                        InputProps={{
                          disableUnderline: true
                        }}
                      />
                      
                    </div>
                    {error && <Grid container >
                            <div className={classes.errorText}>
                            First Name Field is Required
                            </div>
                            </Grid>}

                    <div className={classes.inputDiv}>
                      <TextField
                        type="text"
                        id="standard-basic"
                        placeholder="Enter Last Name"
                        name="last_name"
                        error={error?true:false}
                        onChange={handleInput}
                        value={last_name}
                        className={classes.textfield}
                        
                        InputProps={{
                          disableUnderline: true,
                          endAdornment: true
                        }}
                      />
                    </div>
                    {error && <Grid container >
                            <div className={classes.errorText}>
                            Last Name Field is Required
                            </div>
                            </Grid>}
                    <div className={classes.inputDiv}>
                      <TextField
                        type="text"
                        id="standard-basic"
                        placeholder="Enter Email Address"
                        name="email_address"
                        onChange={handleInput}
                        value={email_address}
                        className={classes.textfield}
                        InputProps={{
                          disableUnderline: true,
                          endAdornment: true
                        }}
                      />
                    </div>
                    {error && <Grid container >
                            <div className={classes.errorText}>
                            Email Field is Required
                            </div>
                            </Grid>}
                    <div className={classes.inputDiv}>
                      <TextField
                        type="password"
                        id="standard-basic"
                        placeholder="Enter password"
                        name="password"
                        value={password}
                        onChange={handleInput}
                        className={classes.textfield}
                        InputProps={{
                          disableUnderline: true,
                          endAdornment: true
                        }}
                      />
                    </div>
                    {error 
                    && 
                    <Grid container >
                            <div className={classes.errorText}>
                            Password Name Field is Required
                            </div>
                            </Grid>
                     }
                    <div className={classes.inputDiv}>
                      <TextField
                        type="password"
                        id="standard-basic"
                        name="confirm_password"
                        placeholder="Enter Confirm Password"
                        className={classes.textfield}
                        onChange={handleInput}
                        onFocusCapture={handCheckPassword}
                        value={confirm_password}    
                        InputProps={{
                          disableUnderline: true,
                          endAdornment: true
                        }}
                      />
                    </div>
                            {error && <Grid container >
                            <div className={classes.errorText}>
                            Confirm Password Field is Required
                            </div>
                            </Grid>}
                    <div className={classes.innerBtn}>
                      <Button
                        onClick={() => setContinue("tellusmore")}
                        className={classes.btn}
                      >
                        Continue &nbsp;
                        <i
                          class="fa fa-arrow-circle-o-right"
                          aria-hidden="true"
                        ></i>
                      </Button>
                    </div>
                  </FormControl>
                </Grid>
              </Grid>

              <Grid item xs={12} lg={6} className={classes.register}>
                <h2 className={classes.LoginText}>
                  <b>WHY</b> <b style={{ color: "#f24c5c" }}>CHOOSE US?</b>
                </h2>
                <p className={classes.textPara}>
                  The spirit of innovation coupled with our superior customer
                  service is our
                </p>
                <p className={classes.textPara}>
                  most important core value and this makes us best.
                </p>
                <br />
                <br />
                <p>
                  {/* <p className={classes.text}>
                    Register today & Get Free PDF Guide Today
                  </p> */}
                  <p>
                    <span>
                      <i
                        style={{ color: "#656565" }}
                        class="fa fa-arrow-circle-o-right"
                        aria-hidden="true"
                      ></i>
                      &nbsp;
                    </span>
                    <b className={classes.text}>
                      GET FREE DEMO ACCOUNT WITH $5000
                    </b>
                  </p>
                  <p>
                    <span>
                      <i
                        style={{ color: "#656565" }}
                        class="fa fa-arrow-circle-o-right"
                        aria-hidden="true"
                      ></i>
                      &nbsp;
                    </span>
                    <b className={classes.text}>
                      MAKE PROFIT UPTO 30% ON YOUR INVESTMENTS EVERY DAY WITH
                      OUR TRAINING
                    </b>
                  </p>
                  <p>
                    <span>
                      <i
                        style={{ color: "#656565" }}
                        class="fa fa-arrow-circle-o-right"
                        aria-hidden="true"
                      ></i>
                      &nbsp;
                    </span>
                    <b className={classes.text}>
                      FREE 1-ON-1 TRAINING WITH OUR EXPERTS TO LEARN MARKET
                      ANALYSIS
                    </b>
                  </p>
                  <p>
                    <span>
                      <i
                        style={{ color: "#656565" }}
                        class="fa fa-arrow-circle-o-right"
                        aria-hidden="true"
                      ></i>
                      &nbsp;
                    </span>
                    <b className={classes.text}>
                      NO CHARGES ON DEPOSIT & WITHDRAWAL OF FUNDS
                    </b>
                  </p>
                </p>
              </Grid>
            </Grid>
          </div>
        )}

        {/* Tra */}
        {value === "tellusmore" ? (
          <div className={classes.formdiv}>
            <Grid container>
              <Grid item lg={6} xs={12} md={6} className={classes.Login}>
                <Grid
                  container
                  direction="row"
                  justify="center"
                  alignItems="center"
                >
                  {/* <HorizontalNonLinearStepper/> */}

                  <FormControl fullWidth className={classes.LoginForm}>
                    <h2 className={classes.LoginText}>
                      RESIDENTIAL <b style={{ color: "#f24c5c" }}>ADDRESS</b>
                    </h2>
                    <div className={classes.inputDiv}>
                      <TextField
                        id="standard-basic"
                        type="text"
                        placeholder="Enter Street Address"
                        name="street_address"
                        value={street_address}
                        onChange={handleInput}
                        className={classes.textfield}
                        InputProps={{
                          disableUnderline: true
                        }}
                      />
                    </div>
                    {errortellus && <Grid container >
                            <div className={classes.errorText}>
                            Street Address Field is Required
                            </div>
                            </Grid>}
                    <div className={classes.inputDiv}>
                      <TextField
                        type="number"
                        id="standard-basic"
                        name="pincode"
                        placeholder="Enter Postal/Zip Code"
                        className={classes.textfield}
                        onChange={handleInput}
                        value={pincode}
                        InputProps={{
                          disableUnderline: true,
                          endAdornment: true
                        }}
                      />
                    </div>
                    {errortellus && <Grid container >
                            <div className={classes.errorText}>
                            Pincode Field is Required
                            </div>
                            </Grid>}
                    <div className={classes.inputDiv}>
                      <TextField
                        type="text"
                        id="standard-basic"
                        name="city"
                        placeholder="Enter City/Town"
                        value={city}
                        className={classes.textfield}
                        onChange={handleInput}
                        InputProps={{
                          disableUnderline: true,
                          endAdornment: true
                        }}
                      />
                    </div>
                    {errortellus && <Grid container >
                            <div className={classes.errorText}>
                            City Field is Required
                            </div>
                            </Grid>}
                    <div className={classes.inputDiv}>
                      <TextField
                        type="text"
                        id="standard-basic"
                        name="state"
                        value={CountryState}
                        placeholder="Enter State/Province"
                        className={classes.textfield}
                        onChange={handleInput}
                        InputProps={{
                          disableUnderline: true,
                          endAdornment: true
                        }}
                      />
                    </div>
                    {errortellus && <Grid container >
                            <div className={classes.errorText}>
                            State Field is Required
                            </div>
                            </Grid>}
                    <div className={classes.inputDiv}>
                      <Select
                        type="password"
                        id="standard-basic"
                        placeholder="Enter Country"
                        name="country"
                        className={classes.textfield}
                        onChange={handleInput}
                        disableUnderline={true}
                        value={country}
                        style={{color:'black'}}
                      >
                        {/* <MenuItem value="" disabled>
                        Placeholder
                    </MenuItem> */}
                        {COUNTRYLIST && COUNTRYLIST.map((res)=>{
                                return <MenuItem key={res.code} value={res.name} style={{color:'black'}}>{res.name}</MenuItem>
                        })
                       
                    }
                      </Select>
                    
                    </div>
                    {errortellus && <Grid container >
                            <div className={classes.errorText}>
                            Country Field is Required
                            </div>
                            </Grid>}
                    <br />
                    {/* perosnal info */}
                    <FormControl
                      style={{ marginLeft: "-10px" }}
                      fullWidth
                      className={classes.LoginForm}
                    >
                      <h2 className={classes.LoginText}>
                        PERSONAL <b style={{ color: "#f24c5c" }}>INFORMATION</b>
                      </h2>
                      <div className={classes.inputDiv}>
                     
                       
                        <TextField
                          id="standard-basic"
                          type="date"
                          placeholder="Enter Date of Birth"
                          name="dob"
                          onChange={handleInput}
                            value={dob}
                        
                          className={classes.textfield}
                          InputProps={{
                            disableUnderline: true
                          }}
                        />
                      </div>
                      {errortellus && <Grid container >
                            <div className={classes.errorText}>
                            Date of Birth Field is Required
                            </div>
                            </Grid>}
                      <div className={classes.inputDiv}>
                        <TextField
                          type="text"
                          id="standard-basic"
                          placeholder="Enter Phone"
                          name="contact_no"
                          onChange={handleInput}
                          value={contact_no}
                          className={classes.textfield}
                          InputProps={{
                            disableUnderline: true,
                            endAdornment: true
                          }}
                        />
                      </div>
                      {errortellus && <Grid container >
                            <div className={classes.errorText}>
                            Contact Number Field is Required
                            </div>
                            </Grid>}
                    </FormControl>
                    {/* end info */}

                    <div className={classes.innerBtn}>
                      <Button
                        onClick={() => lastGrid("accountsettings")}
                        className={classes.btn}
                      >
                        Continue &nbsp;
                        <i
                          class="fa fa-arrow-circle-o-right"
                          aria-hidden="true"
                        ></i>
                      </Button>

                      <Button 
                      onClick={() => setValue("")}
                      className={classes.btn}>
                        <i
                          class="fa fa-arrow-circle-o-left"
                          aria-hidden="true"
                        ></i>
                        &nbsp; BACK
                      </Button>
                    </div>
                  </FormControl>
                </Grid>
              </Grid>

              <Grid item xs={12} lg={6} className={classes.register}>
                <h2 className={classes.LoginText}>
                  WHY <b style={{ color: "#f24c5c" }}>TRADERS TRUST US</b>
                </h2>
                <div className={classes.root}>
                  <List component="nav" aria-label="main mailbox folders">
                    <ListItem>
                      <ListItemIcon>
                        <img
                          className={classes.image}
                          src={require("../../assets/images/register1.png")}
                        />
                      </ListItemIcon>
                      <ListItemText
                        primary={
                          <React.Fragment>
                            <h4 className={classes.textTitle}>TRADE SAFELY</h4>
                          </React.Fragment>
                        }
                        secondary={
                          <React.Fragment>
                            <p className={classes.textPara1}>
                              Our SSL certified platforms ensure that all the
                              information is safe and secure
                            </p>
                          </React.Fragment>
                        }
                      />
                    </ListItem>
                    <ListItem>
                      <ListItemIcon>
                        <img
                          className={classes.image}
                          src={require("../../assets/images/register2.png")}
                        />
                      </ListItemIcon>
                      <ListItemText
                        primary={
                          <React.Fragment>
                            <h4 className={classes.textTitle}>
                              NO HIDDEN CHARGES
                            </h4>
                          </React.Fragment>
                        }
                        secondary={
                          <React.Fragment>
                            <p className={classes.textPara1}>
                              All charges and fees are disclosed to you
                              beforehand and there will be no hidden costs.
                            </p>
                          </React.Fragment>
                        }
                      />
                    </ListItem>
                    <ListItem>
                      <ListItemIcon>
                        <img
                          className={classes.image}
                          src={require("../../assets/images/register3.png")}
                        />
                      </ListItemIcon>
                      <ListItemText
                        primary={
                          <React.Fragment>
                            <h4 className={classes.textTitle}>ALL IN ONE</h4>
                          </React.Fragment>
                        }
                        secondary={
                          <React.Fragment>
                            <p className={classes.textPara1}>
                              You can now trade in Forex and Commodities using
                              one single platform.
                            </p>
                          </React.Fragment>
                        }
                      />
                    </ListItem>
                    <ListItem>
                      <ListItemIcon>
                        <img
                          className={classes.image}
                          src={require("../../assets/images/register1.png")}
                        />
                      </ListItemIcon>
                      <ListItemText
                        primary={
                          <React.Fragment>
                            <h4 className={classes.textTitle}>Learn & Trade</h4>
                          </React.Fragment>
                        }
                        secondary={
                          <React.Fragment>
                            <p className={classes.textPara1}>
                              We treat your success as our own and hence we have
                              dedicated support & service team to guide you
                              whenever you need.
                            </p>
                          </React.Fragment>
                        }
                      />
                    </ListItem>
                  </List>
                </div>
              </Grid>
            </Grid>
          </div>
        ) : (
          value === "accountsettings" && (
            <div className={classes.formdiv}>
              <h2 align="center" className={classes.LoginText}>
                GET READY{" "}
                <b style={{ color: "#f24c5c" }}>FOR YOUR FOREX ACCOUNT</b>
              </h2>
              <p>
                Now you are all set to start a forex account with us and its
                just few clicks away.
              </p>
              <Grid container>
                <Grid item lg={6} xs={12} md={6} className={classes.Login}>
                  <Grid
                    container
                    direction="row"
                    justify="center"
                    alignItems="center"
                  >
                    {/* <HorizontalNonLinearStepper/> */}

                    <FormControl fullWidth className={classes.LoginForm}>
                    <label>Package</label>
                    <div className={classes.inputDiv}>
                      <Select
                        
                        id="standard-basic"
                        placeholder="Select Account Type"
                        name="package"
                        value={Package}
                        onChange={handleInput}
                        className={classes.textfield}
                        disableUnderline={true}
                        style={{color:'black'}}
                      >
                        
                       {Packages&&Packages.map((res)=>{
                        return(<MenuItem value={res.value}>{res.text}</MenuItem>)
                       })} 
                       
                      </Select>
                     
                    </div>
                    {errorSetting && <Grid container >
                            <div className={classes.errorText}>
                            Package Field is Required
                            </div>
                            </Grid>}
                     <labe>Leverage</labe>       
                    <div className={classes.inputDiv}>
                      <Select
                        type="text"
                        id="standard-basic"
                        placeholder="Leverage"
                        name="leverage"
                        className={classes.textfield}
                        disableUnderline={true}
                        onChange={handleInput}
                        value={leverage}
                        style={{color:'black'}}
                      >
                       
                        {LEVERAGE&&LEVERAGE.map((res)=>{
                        return(
                        <MenuItem value={res.value}>{res.text}</MenuItem>

                        )
                        })}
                        
                      </Select>
                     
                    </div>
                    {errorSetting && <Grid container >
                            <div className={classes.errorText}>
                            Leverage Field is Required
                            </div>
                            </Grid>}
                      <div className={classes.innerBtn}>
                        <Button
                          onClick={settingSubmit}
                          className={classes.btn}
                        >
                          Continue &nbsp;
                          <i
                            class="fa fa-arrow-circle-o-right"
                            aria-hidden="true"
                          ></i>
                        </Button>
                        <Link to="login" style={{textDecoration:'none'}}>
                        <Button
                          onClick={()=>setValue("tellusmore")}
                          className={classes.btn}
                        >
                          Back &nbsp;
                         
                        </Button>
                        </Link>
                      </div>
                    </FormControl>
                  </Grid>
                </Grid>

                <Grid item xs={12} lg={6} className={classes.register}>
                <FormControlLabel
        control={<Checkbox checked={state.checkedA}  onChange={handleChange} name="checkedA" />}
        label="I want to receive news and updates."
      />
      <br/>
         <FormControlLabel
        control={<Checkbox checked={state.checkedB}  onChange={handleChange} name="checkedB" />}
        label="I want to receive Daily Technical Analysis."
      />
                </Grid>
              </Grid>
            </div>
          )
        )}

        <LoginFooter />
      </div>
    </>
  );
};

export default Register;
