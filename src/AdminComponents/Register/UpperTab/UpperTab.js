import { Grid } from '@material-ui/core';
import React from 'react'
import {useStyles} from './css'
export const UpperTab = (props) => {

    const classes=useStyles();
    return (
        <div className={classes.maindiv}>
            <Grid container spacing={3}>
                <Grid item xs>
                    <div style={{backgroundColor:'#F24C5C',padding:'20px',borderRadius:'20px'}}>
                        <span >
                            1
                        </span>
                        &nbsp;
                        Create Your Profile
                    </div>
                </Grid>
                <Grid item xs>
                <div style={{backgroundColor:'#F24C5C',padding:'20px',borderRadius:'20px'}}>
                        <span >
                            2
                        </span>
                        &nbsp;
                       Tell us more about You.
                    </div>
                </Grid>
                <Grid item xs>
                <div style={{backgroundColor:'#F24C5C',padding:'20px',borderRadius:'20px'}}>
                        <span >
                            3
                        </span>
                        &nbsp;
                        Account Settings & Confirmation
                    </div>
                </Grid>
            </Grid>
        </div>
    )
}

export default UpperTab;