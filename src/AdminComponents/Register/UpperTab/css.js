import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles(theme => ({

maindiv:{
marginLeft:'80px',
marginRight:'80px',
marginTop:'80px',
marginBottom:'10px',
paddingLeft:'80px',
paddingRight:'80px',
[theme.breakpoints.down("sm")]: {
    marginLeft:'0px',
marginRight:'0px',
marginTop:'0px',
marginBottom:'0px',
paddingLeft:'0px',
paddingRight:'0px',
}
}

}))