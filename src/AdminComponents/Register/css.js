import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1
  },
  headerbackground: {
    flexGrow: 1,
    backgroundColor: "#f3f3f3",
    padding: "5px"
  },
  logo: {
    color: ""
  },
  Login: {
    flexGrow: 1,
    padding: "40px",
    [theme.breakpoints.down("sm")]: {
      padding: "0px"
    }
  },
  LoginForm: {
    flexGrow: 1,
    padding: "10px",
    marginTop: "-40px",
    [theme.breakpoints.down("sm")]: {
      padding: "20px",
      display: "flex",
      alignItems: "center"
    }
  },
  inputDiv: {
    border: "1px solid #d6d6d6",
    width: "100%",
    padding: "5px",
    margin: "10px",
    backgroundColor: "white"
  },
  textfield: {
    height: "40px",
    color: "#ffff",
    width: "100%"
  },
  LoginText: {
    // fontWeight: "bold",
    fontSize: "25px",
    paddingLeft: "10px",
    color: "#505050",
    [theme.breakpoints.down("sm")]: {
      paddingLeft: "0px !important"
    }
  },
  image: {
    width: "50px",
    height: "50px",
    padding: "20px",
    marginTop: "-15px"
  },
  btndiv: {
    padding: "10px",
    width: "200px",
    margin: "30px"
  },
  btn: {
    backgroundColor: "#f24c5c",
    width: "200px",
    color: "white",
    marginLeft: "5px",
    border: "1px dashed #fff",
    "&:hover": {
      transition: "1s ease ",
      backgroundColor: "white",
      color: "black",
      border: "1px dashed #656565"
    },
    [theme.breakpoints.down("sm")]: {}
  },
  innerBtn: {
    // width:'200px',
    padding: "5px",
    margin: "10px",
    display: "flex",
    justifyContent: "space-between",
    // backgroundColor:'#f24c5c',
    [theme.breakpoints.down("sm")]: {
      display: "block"
    }
  },
  errorText:{
        margin:'8px',
        color:'red',
        
  },
  forgotText: {
    color: "#337AB7",
    [theme.breakpoints.down("sm")]: {
      display: "block",
      paddingTop: "15px"
    }
  },
  register: {
    flexGrow: 1,
    padding: "10px",
    [theme.breakpoints.down("sm")]: {
      padding: "20px"
    }
  },
  textPara: {
    color: "#656565"
  },
  textTitle: {
    color: "#656565",
    fontSize: "18px",
    fontWeight: "700"
  },
  textPara1: {
    marginTop: "-20px",
    fontSize: "15px",
    color: "#656565"
  },
  text: {
    paddingLeft: "10px",
    fontSize: "12px",
    color: "black",
    fontWeight: "600"
  },
  iconFont: {
    fontSize: "15px"
  },

  regbtn: {
    textDecoration: "none"
  },
  stepper: {
    padding: "20px"
  },
  formdiv: {
    backgroundColor: "#FBFBFB",
    margin: "80px",
    padding: "20px",
    [theme.breakpoints.down("sm")]: {
      padding: "20px",
      margin: "0px"
    }
  }
}));
