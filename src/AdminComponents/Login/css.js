import { makeStyles } from "@material-ui/core/styles";

export const useStyles = makeStyles(theme => ({
  headerbackground: {
    flexGrow: 1,
    backgroundColor: "#f3f3f3",
    padding: "5px"
  },
  logo: {
    color: ""
  },
  Login: {
    flexGrow: 1,
    padding: "40px",
    [theme.breakpoints.down("sm")]: {
      padding: "0px"
    }
  },
  LoginForm: {
    flexGrow: 1,
    padding: "40px",

    [theme.breakpoints.down("sm")]: {
      padding: "20px",
      display: "flex",
      alignItems: "center"
    }
  },
  inputDiv: {
    border: "1px solid #d6d6d6",
    width: "80%",
    padding: "5px",
    margin: "25px"
  },
  textfield: {
    color: "#f3f3f3",
    width:'100%'
  },
  LoginText: {
    paddingLeft: "30px",
    color: "#505050",
    [theme.breakpoints.down("sm")]: {
      paddingLeft: "0px !important"
    }
  },
  btndiv: {
    padding: "10px",
    width: "200px",
    margin: "30px"
  },
  btn: {
    backgroundColor: "#f24c5c",
    width: "200px",
    color: "white",
    border: "1px dashed #fff",
    "&:hover": {
      transition: "1s ease ",
      backgroundColor: "white",
      color: "black",
      border: "1px dashed #656565"
    },
    [theme.breakpoints.down("sm")]: {}
  },
  innerBtn: {
    // width:'200px',
    padding: "5px",
    margin: "30px",
    display: "flex",
    justifyContent: "space-between",
    // backgroundColor:'#f24c5c',
    [theme.breakpoints.down("sm")]: {
      display: "block"
    }
  },
  forgotText: {
    color: "#337AB7",
    [theme.breakpoints.down("sm")]: {
      display: "block",
      paddingTop: "15px"
    }
  },
  register: {
    flexGrow: 1,
    padding: "70px",
    [theme.breakpoints.down("sm")]: {
      padding: "20px"
    }
  },
  iconFont: {
    fontSize: "15px"
  },
  text: {
    paddingLeft: "10px",
    color: "#656565"
  },
  regbtn:{
    textDecoration:'none'
  },
  check:{
    marginLeft:'30px',
    [theme.breakpoints.down("sm")]: {
      marginLeft: "0px"
    }
  }
}));
