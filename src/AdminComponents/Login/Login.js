import {
  Button,
  Checkbox,
  FormControl,
  FormControlLabel,
  Grid,
  InputAdornment,
  TextField
} from "@material-ui/core";
import React, { useEffect, useState } from "react";
import CheckCircleOutlineIcon from "@material-ui/icons/CheckCircleOutline";
import { useStyles } from "./css";
import { LoginFooter } from "../LoginFooter/LoginFooter";
import { Link, Redirect } from "react-router-dom";
import { CheckBox } from "@material-ui/icons";
import { customerLoginAPI } from "../../util/Service/Service";
import CustomizedDialogs from "../ErrorModal/ErrorModal";

const Login = props => {
  console.log(props);
  const classes = useStyles();
  const [payload, setPayload] = useState("");
  const [data, setData] = useState(sessionStorage.getItem("payload"));
  const [email_address, setEmail_address] = useState("");
  const [redirect, setRedirect] = useState(false);
  const [password, setPassword] = useState("");
  const [checked, setChecked] = useState(false);
  const [error, setError] = useState(false);
  const [open, setOpen] = useState(false);
  const [errorMessage, setErrorMessage] = useState("");
  useEffect(() => {
    if (data) {
      setRedirect(true);
    } else {
      setRedirect(false);
    }
  });

  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  const handleCheck = e => {
    setChecked(e.target.checked);
  };
  const handleChange = e => {
    if (e.target.name === "email_address") {
      setEmail_address(e.target.value);
    } else if (e.target.name === "password") {
      setPassword(e.target.value);
    } else {
      // if(e.target.value===false)
      // setChecked(true)
      // else{
      //   setChecked(false)
      // }
    }
  };
  // if(checked===false){
  //   setError(true)
  // }
  const SubmitForm = () => {
    console.log(email_address, checked, password);
    const data = {
      email_address: email_address,
      password: password,
      termAgree: checked
    };
    customerLoginAPI(data)
      .then(res => {
        console.log(res);
        if (res.data.msg === "success") {
          sessionStorage.setItem("payload", JSON.stringify(res.data));
          // setPayload(res.data)
          setRedirect(true);
        } else {
          setError(true);
        }
      })
      .catch(err => {
        if (err) {
          setError(true);
          setErrorMessage(err.response.data);
          setOpen(true);
          console.log(err.response);
        }
      });
  };
  if (redirect) {
    return <Redirect to="live/dashboard" />;
  }

  return (
    <div>
      {error && (
        <CustomizedDialogs
          errorMessage={errorMessage}
          handleClickOpen={handleClickOpen}
          handleClose={handleClose}
          open={open}
        />
      )}
      <Grid container>
        <Grid item xs={12} className={classes.headerbackground}>
          <div className={classes.headerbackground}>
            <div>
              <h2 className={classes.logo}>www.profit4money.com</h2>
            </div>
          </div>
        </Grid>
        <Grid item lg={6} xs={12} className={classes.Login}>
          <Grid container direction="row" justify="center" alignItems="center">
            <FormControl fullWidth className={classes.LoginForm}>
              <h2 className={classes.LoginText}>LOGIN</h2>
              <div className={classes.inputDiv}>
                <TextField
                  id="standard-basic"
                  placeholder="abc@gmail.com"
                  className={classes.textfield}
                  InputProps={{
                    disableUnderline: true
                  }}
                  name="email_address"
                  value={email_address}
                  onChange={handleChange}
                />
              </div>

              <div className={classes.inputDiv}>
                <TextField
                  type="password"
                  id="standard-basic"
                  className={classes.textfield}
                  InputProps={{
                    disableUnderline: true,
                    endAdornment: true
                  }}
                  value={password}
                  onChange={handleChange}
                  name="password"
                />
              </div>

              <FormControlLabel
                className={classes.check}
                control={
                  <Checkbox
                    checked={checked}
                    onChange={handleCheck}
                    name="termAgree"
                  />
                }
                label="I understand and accept the above fields"
              />
              <div className={classes.innerBtn}>
                <Button onClick={SubmitForm} className={classes.btn}>
                  SUBMIT
                </Button>

                <span className={classes.forgotText}>Forgot password ?</span>
              </div>
            </FormControl>
          </Grid>
        </Grid>
        <Grid item xs={12} lg={6} className={classes.register}>
          <p className={classes.text}>
            {" "}
            Register today & Get Free PDF Guide Today
          </p>
          <p className={classes.text}>
            <span>
              <CheckCircleOutlineIcon className={classes.iconFont} />
            </span>{" "}
            Make Profit Upto 30% on your investments every day with our
            training.
          </p>
          <p className={classes.text}>
            <span>
              <CheckCircleOutlineIcon className={classes.iconFont} />
            </span>{" "}
            Free 1-on-1 Training With Our Experts To Learn Market Analysis
          </p>
          <p className={classes.text}>
            <span>
              <CheckCircleOutlineIcon className={classes.iconFont} />
            </span>{" "}
            No Charges On Deposit & Withdrawal of Funds.
          </p>
          <Link to="/forex4money/admin/register" className={classes.regbtn}>
            <div className={classes.innerBtn}>
              <Button className={classes.btn}>OPEN ACCOUNT</Button>
            </div>
          </Link>
        </Grid>
      </Grid>

      <LoginFooter />
    </div>
  );
};

export default Login;
