import { makeStyles, fade } from "@material-ui/core/styles";
export const drawerWidth = 240;

export const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper
  },
  container: {
    maxHeight: 440
  },
  desktopView: {
    display: "block",
    [theme.breakpoints.down("sm")]: {
      display: "none"
    }
  },
  cardContainer: {
    marginLeft: "20px",
    marginRight: "20px",
    [theme.breakpoints.down("sm")]: {
      marginLeft: "0px",
      marginRight: "0px"
    }
  },
  tabContainer: {
    backgroundColor: "white",
    boxShadow: "1px 1px white"
  },
  tabButton: {
    textTransform: "none",
    color: "grey"
  },
  gridContainer: {
    textAlign: "center",
    border: "1px solid black"
  },
  flexContainer: {
    backgroundColor: "grey",
    marginTop: "-20px",
    display: "flex",
    justifyContent: "space-between"
  },
  title: {
    padding: "5px",
    marginTop: "0px",
    backgroundColor: "#26344B",
    color: "white",
    fontSize: "15px"
  },
  bid: {
    width: "100%",
    borderRight: "1px solid black"
  },
  ask: {
    width: "100%"
  },
  bidTitle: {
    fontSize: "15px",
    color: "white"
  },
  bidValue: {
    marginTop: "-10px",
    fontSize: "15px"
  }
}));
