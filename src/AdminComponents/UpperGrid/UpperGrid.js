import React, { useState } from "react";
import PropTypes from "prop-types";
import { makeStyles } from "@material-ui/core/styles";
import AppBar from "@material-ui/core/AppBar";
import Tabs from "@material-ui/core/Tabs";
import Button from "@material-ui/core/Button";
import Tab from "@material-ui/core/Tab";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Box from "@material-ui/core/Box";
import Table from "@material-ui/core/Table";
import AppsIcon from "@material-ui/icons/Apps";
import ListIcon from "@material-ui/icons/List";
import ZoomInIcon from "@material-ui/icons/ZoomIn";
import { useStyles } from "./css";
import Paper from "@material-ui/core/Paper";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TablePagination from "@material-ui/core/TablePagination";
import TableRow from "@material-ui/core/TableRow";
import DealModal from "../../util/Modal/Modal";
function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box p={3}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.any.isRequired,
  value: PropTypes.any.isRequired
};

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`
  };
}

export default function SimpleTabs(props) {
  const classes = useStyles();
  const [value, setValue] = useState(0);
  const [showGrid, setShowGrid] = useState(false);
  const [open, setOpen] = useState(false);

  console.log(props);
  const handleChange = (event, newValue) => {
    setValue(newValue);
  };
  const handleClickOpen = () => {
    setOpen(true);
  };
  const handleClose = () => {
    setOpen(false);
  };

  return (
    <Card className={classes.cardContainer}>
      {open && (
        <DealModal
          handleClose={handleClickOpen}
          handleClose={handleClose}
          open={open}
        />
      )}
      <CardContent>
        <div className={classes.root}>
          <AppBar position="static" className={classes.tabContainer}>
            <Tabs
              value={value}
              onChange={handleChange}
              aria-label="simple tabs example"
            >
              <Tab
                className={classes.tabButton}
                label="Currencies"
                {...a11yProps(0)}
              />
              <Tab
                className={classes.tabButton}
                label="Commodities"
                {...a11yProps(1)}
              />

              <Grid
                justify="space-between" // Add it here :)
                container
                spacing={24}
              >
                <Grid item></Grid>
                <Grid item className={classes.desktopView}>
                  <Button
                    style={{
                      marginTop: "10px",
                      backgroundColor: "#e7505a",
                      color: "white",
                      height: "30px"
                    }}
                    onClick={() => setShowGrid(false)}
                  >
                    <i class="fa fa-th-large" aria-hidden="true"></i>&nbsp; GRID
                  </Button>
                  <Button
                    style={{
                      marginTop: "10px",
                      marginLeft: "5px",
                      backgroundColor: "#e7505a",
                      color: "white",
                      height: "30px"
                    }}
                    onClick={() => setShowGrid(true)}
                  >
                    <i class="fa fa-list" aria-hidden="true"></i>&nbsp; LIST
                  </Button>
                  {!props.showfull ? (
                    <Button
                      style={{
                        marginTop: "10px",
                        marginLeft: "5px",
                        backgroundColor: "#e7505a",
                        color: "white",
                        height: "30px"
                      }}
                      onClick={() => props.fullwidth(true, "upper")}
                    >
                      <i class="fa fa-expand" aria-hidden="true"></i>
                    </Button>
                  ) : props.showfull === true ? (
                    <Button
                      style={{
                        marginTop: "10px",
                        marginLeft: "5px",
                        backgroundColor: "#e7505a",
                        color: "white",
                        height: "30px"
                      }}
                      onClick={() => props.fullwidth(false, "upper")}
                    >
                      <i class="fa fa-compress" aria-hidden="true"></i>
                    </Button>
                  ) : (
                    ""
                  )}
                </Grid>
              </Grid>
            </Tabs>
          </AppBar>
          <TabPanel value={value} index={0}>
            <Grid container spacing={0}>
              {!showGrid && (
                <Grid
                  item
                  xs={6}
                  sm={6}
                  md={3}
                  lg={3}
                  onClick={handleClickOpen}
                >
                  <div className={classes.gridContainer}>
                    <p className={classes.title}>EUR/USD(0.12345)</p>
                    <div className={classes.flexContainer}>
                      <div className={classes.bid}>
                        <p className={classes.bidTitle}>Bid</p>
                        <p className={classes.bidValue}>high</p>
                        <p className={classes.bidValue}>low</p>
                      </div>
                      <div className={classes.ask}>
                        <p className={classes.bidTitle}>Ask</p>
                        <p className={classes.bidValue}>high</p>
                        <p className={classes.bidValue}>low</p>
                      </div>
                    </div>
                  </div>
                </Grid>
              )}
              {showGrid && <ListTable />}
            </Grid>
          </TabPanel>
          <TabPanel value={value} index={1}>
            commodities
          </TabPanel>
        </div>
      </CardContent>
    </Card>
  );
}

//list table
function createData(
  instrument,
  bid,
  ask,
  open,
  high,
  low,
  change,
  changeP,
  gmt
) {
  return {
    instrument,
    bid,
    ask,
    open,
    high,
    low,
    change,
    changeP,
    gmt
  };
}

const columns = [
  { id: "instrument", label: "Instrument", minWidth: 100, align: "center" },
  { id: "bid", label: "Bid", minWidth: 100, align: "center" },
  {
    id: "ask",
    label: "Ask",
    minWidth: 100,
    align: "center"
  },
  {
    id: "open",
    label: "Open",
    minWidth: 100,
    align: "center"
  },
  {
    id: "high",
    label: "High",
    minWidth: 100,
    align: "center"
  },
  {
    id: "low",
    label: "Low",
    minWidth: 100,
    align: "center"
  },
  {
    id: "change",
    label: "Change",
    minWidth: 100,
    align: "center"
  },
  {
    id: "changeP",
    label: "Change(%)",
    minWidth: 100,
    align: "center"
  },
  {
    id: "gmt",
    label: "GMT",
    minWidth: 100,
    align: "center"
  }
];

const rows = [
  createData(
    "EUR/USD 0.01",
    <>
      <Button style={{ backgroundColor: "red" }}>1.1246</Button>
    </>,
    <>
      <Button style={{ backgroundColor: "green" }}>1.1246</Button>
    </>,
    125.144,
    12.1455,
    1.25466,
    2.36455,
    -0.14,
    "6:25:32 AM"
  ),
  createData(
    "EUR/USD 0.01",
    <>
      <Button style={{ backgroundColor: "red" }}>1.1246</Button>
    </>,
    <>
      <Button style={{ backgroundColor: "green" }}>1.1246</Button>
    </>,
    125.144,
    12.1455,
    1.25466,
    2.36455,
    -0.14,
    "6:25:32 AM"
  ),
  createData(
    "EUR/USD 0.01",
    <>
      <Button style={{ backgroundColor: "red" }}>1.1246</Button>
    </>,
    <>
      <Button style={{ backgroundColor: "green" }}>1.1246</Button>
    </>,
    125.144,
    12.1455,
    1.25466,
    2.36455,
    -0.14,
    "6:25:32 AM"
  ),
  createData(
    "EUR/USD 0.01",
    <>
      <Button style={{ backgroundColor: "red" }}>1.1246</Button>
    </>,
    <>
      <Button style={{ backgroundColor: "green" }}>1.1246</Button>
    </>,
    125.144,
    12.1455,
    1.25466,
    2.36455,
    -0.14,
    "6:25:32 AM"
  )
];

// const useStyles = makeStyles({
//   root: {
//     width: "100%"
//   },
//   container: {
//     maxHeight: 440
//   }
// });

export function ListTable() {
  const classes = useStyles();
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = event => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  return (
    <Paper className={classes.root}>
      <TableContainer style={{ height: "440px" }} className={classes.container}>
        <Table stickyHeader aria-label="sticky table">
          <TableHead>
            <TableRow>
              {columns.map(column => (
                <TableCell
                  key={column.id}
                  align={column.align}
                  style={{ minWidth: column.minWidth }}
                >
                  {column.label}
                </TableCell>
              ))}
            </TableRow>
          </TableHead>
          <TableBody>
            {rows
              .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
              .map(row => {
                return (
                  <TableRow hover role="checkbox" tabIndex={-1} key={row.code}>
                    {columns.map(column => {
                      const value = row[column.id];
                      return (
                        <TableCell key={column.id} align={column.align}>
                          {column.format && typeof value === "number"
                            ? column.format(value)
                            : value}
                        </TableCell>
                      );
                    })}
                  </TableRow>
                );
              })}
          </TableBody>
        </Table>
      </TableContainer>
    </Paper>
  );
}
