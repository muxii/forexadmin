import React from "react";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import Appbar from "../Appbar/Appbar";
import Appbar1 from "../Appbar1/Appbar1";
import MobileAppbar from "../MobileAppbar/MobileAppbar";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import TextField from "@material-ui/core/TextField";
import Checkbox from "@material-ui/core/Checkbox";
import FormGroup from "@material-ui/core/FormGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import { useStyles } from "./css";
import ListTable from "../ListTable/ListTable";
import { Link } from "react-router-dom";

export default function FullWidthGrid() {
  const classes = useStyles();
  const [checked, setChecked] = React.useState(true);

  const handleChange = event => {
    setChecked(event.target.checked);
  };

  return (
    <div className={classes.root}>
      <Appbar />
      <Appbar1 />
      <MobileAppbar />
      <Grid container spacing={0}>
        <Grid
          item
          xs={12}
          sm={12}
          md={9}
          lg={9}
          className={classes.cardContainer}
        >
          <Card>
            <CardContent>
              <p className={classes.title}>Select Mode of Payment</p>

              <Grid container spacing={0}>
                <Grid item xs={12} sm={12} md={3} lg={3}>
                  <Card className={classes.modalContainer}>
                    <CardContent>
                      <p className={classes.cardHeader}>Online Payment</p>
                      <img
                        className={classes.image}
                        src="https://cdn.forex4money.com/Images/net-banking.png"
                      />
                    </CardContent>
                  </Card>
                </Grid>
                <Grid item xs={12} sm={12} md={3} lg={3}>
                  <Card className={classes.modalContainer}>
                    <CardContent>
                      <p className={classes.cardHeader}>Mega Transfer</p>
                      <img
                        className={classes.image}
                        src="https://cdn.forex4money.com/Images/mt-logo.png"
                      />
                    </CardContent>
                  </Card>
                </Grid>
                <Grid item xs={12} sm={12} md={3} lg={3}>
                  <Card className={classes.modalContainer}>
                    <CardContent>
                      <p className={classes.cardHeader}>Bank Transfer</p>
                      <img
                        className={classes.image}
                        src="https://cdn.forex4money.com/Images/bank-transfer.png"
                      />
                    </CardContent>
                  </Card>
                </Grid>
                <Grid item xs={12} sm={12} md={6} lg={6}>
                  <br />
                  <p className={classes.title1}>Please Enter Deposit Details</p>
                  <form noValidate autoComplete="off">
                    <label>Deposit Currency</label> <br />
                    <div>
                      <select className={classes.dropdown}>
                        <option selected>SELECT CURRENCY</option>
                        <option>US Dollar</option>
                      </select>
                    </div>
                    <br />
                    <br />
                    <label>Deposit Amount</label> <br />
                    <TextField
                      size="small"
                      fullWidth
                      id="outlined-basic"
                      variant="outlined"
                    />
                  </form>
                </Grid>
              </Grid>
              <br />

              <Button align="right" className={classes.submitButton}>
                DEPOSIT
              </Button>
            </CardContent>
          </Card>
        </Grid>
        <Grid item xs={12} sm={12} md={3} lg={3}>
          <ListTable />
        </Grid>
      </Grid>
    </div>
  );
}
