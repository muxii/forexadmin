import { makeStyles, fade } from "@material-ui/core/styles";
export const drawerWidth = 240;

export const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    backgroundColor: "#f2f2f2"
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary
  },
  title: {
    fontSize: "20px",
    color: "black"
  },
  title1: {
    fontSize: "25px",
    color: "grey"
  },
  divider: {
    marginTop: "-10px",
    marginBottom: "10px",
    borderBottom: "1px solid grey"
  },
  cardContainer: {
    padding: "20px",
    [theme.breakpoints.down("sm")]: {
      padding: "0px"
    }
  },
  secondForm: {
    marginLeft: "10px",
    [theme.breakpoints.down("sm")]: {
      marginLeft: "0px",
      marginTop: "20px"
    }
  },
  checkboxButton: {
    marginTop: "10px",
    [theme.breakpoints.down("sm")]: {
      marginTop: "10px"
    }
  },
  submitButton: {
    textTransform: "none",
    backgroundColor: "#e7505a",
    color: "white",
    [theme.breakpoints.down("sm")]: {
      marginTop: "0px"
    }
  },
  dropdown: {
    fontSize: "14px",
    color: "black",
    width: "100%",
    height: "35px",
    outline: "0px",
    [theme.breakpoints.down("sm")]: {
      width: "99.5%"
    }
  },
  flexContainer: {
    display: "flex",
    justifyContent: "space-between"
  },
  backButton: {
    backgroundColor: "#e7505a",
    height: "30px",
    color: "white",
    margin: "20px",
    "&:hover": {
      backgroundColor: "#e7505a",
      color: "white"
    }
  },
  modalContainer: {
    backgroundColor: "#f2f2f2",
    width: "80%",
    // "&:hover": {
    //   color: "#e7505a",
    //   border: "1px solid #e7505a"
    // },
    [theme.breakpoints.down("sm")]: {
      marginLeft: "20%",
      marginRight: "20%",
      width: "60%",
      marginTop: "30px"
    }
  },
  cardHeader: {
    textAlign: "center",
    color: "black",
    "&:hover": {
      color: "#e7505a"
    }
  },
  image: {
    display: "block",
    marginLeft: "auto",
    marginRight: "auto",
    width: "65px",
    height: "65px",
    marginTop: "30px"
  }
}));
