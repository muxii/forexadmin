import React from "react";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import Appbar from "../Appbar/Appbar";
import Appbar1 from "../Appbar1/Appbar1";
import MobileAppbar from "../MobileAppbar/MobileAppbar";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import TextField from "@material-ui/core/TextField";
import Checkbox from "@material-ui/core/Checkbox";
import FormGroup from "@material-ui/core/FormGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import { useStyles } from "./css";
import ListTable from "../ListTable/ListTable";

export default function FullWidthGrid() {
  const classes = useStyles();
  const [checked, setChecked] = React.useState(true);

  const handleChange = event => {
    setChecked(event.target.checked);
  };

  return (
    <div className={classes.root}>
      <Appbar />
      <Appbar1 />
      <MobileAppbar />
      <Grid container spacing={0}>
        <Grid
          item
          xs={12}
          sm={12}
          md={9}
          lg={9}
          className={classes.cardContainer}
        >
          <Card>
            <CardContent>
              <p className={classes.title}>My Profile</p>
              <div className={classes.divider}></div>
              <Grid container spacing={0}>
                <Grid item xs={12} sm={12} md={6} lg={6}>
                  <form noValidate autoComplete="off">
                    <label>Account Number</label> <br />
                    <TextField
                      size="small"
                      fullWidth
                      id="outlined-basic"
                      variant="outlined"
                      defaultValue="F4000063"
                    />
                    <br />
                    <br />
                    <label>Package</label> <br />
                    <TextField
                      size="small"
                      fullWidth
                      id="outlined-basic"
                      variant="outlined"
                      defaultValue="MINI"
                    />
                    <br />
                    <br />
                    <label>First Name</label> <br />
                    <TextField
                      size="small"
                      fullWidth
                      id="outlined-basic"
                      variant="outlined"
                      defaultValue="dinesh"
                    />
                    <br />
                    <br />
                    <label>Mobile Name</label> <br />
                    <TextField
                      size="small"
                      fullWidth
                      id="outlined-basic"
                      variant="outlined"
                      defaultValue="1234561236"
                    />
                    <br />
                    <br />
                    <label>Street Address</label> <br />
                    <TextField
                      size="small"
                      fullWidth
                      id="outlined-basic"
                      variant="outlined"
                      defaultValue="Gali no.2"
                    />
                    <br />
                    <br />
                    <label>City</label> <br />
                    <TextField
                      size="small"
                      fullWidth
                      id="outlined-basic"
                      variant="outlined"
                      defaultValue="mumbai"
                    />
                    <br />
                    <br />
                    <label>Country</label> <br />
                    <TextField
                      size="small"
                      fullWidth
                      id="outlined-basic"
                      variant="outlined"
                      defaultValue="INDIA"
                    />
                  </form>
                </Grid>
                <Grid item xs={12} sm={12} md={6} lg={6}>
                  <form
                    className={classes.secondForm}
                    noValidate
                    autoComplete="off"
                  >
                    <label>Email Address</label> <br />
                    <TextField
                      size="small"
                      fullWidth
                      id="outlined-basic"
                      variant="outlined"
                      defaultValue="dineshkyadav@gmail.com"
                    />
                    <br />
                    <br />
                    <label>Leverage Ratio</label> <br />
                    <TextField
                      size="small"
                      fullWidth
                      id="outlined-basic"
                      variant="outlined"
                      defaultValue="1:500"
                    />
                    <br />
                    <br />
                    <label>Last Name</label> <br />
                    <TextField
                      size="small"
                      fullWidth
                      id="outlined-basic"
                      variant="outlined"
                      defaultValue="yadav"
                    />
                    <br />
                    <br />
                    <label>Date Of Birth</label> <br />
                    <TextField
                      size="small"
                      fullWidth
                      id="outlined-basic"
                      variant="outlined"
                      defaultValue="10/02/2010"
                    />
                    <br />
                    <br />
                    <label>State</label> <br />
                    <TextField
                      size="small"
                      fullWidth
                      id="outlined-basic"
                      variant="outlined"
                      defaultValue="na"
                    />
                    <br />
                    <br />
                    <label>Postal Code</label> <br />
                    <TextField
                      size="small"
                      fullWidth
                      id="outlined-basic"
                      variant="outlined"
                      defaultValue="400103"
                    />
                  </form>
                </Grid>
              </Grid>
              <FormControlLabel
                className={classes.checkboxButton}
                control={<Checkbox name="checkedC" />}
                label="Daily Analysis"
              />
              <FormControlLabel
                className={classes.checkboxButton}
                control={<Checkbox name="checkedC" />}
                label="Receive News"
              />
              <br />
              <Button align="right" className={classes.submitButton}>
                UPDTAE PROFILE
              </Button>
            </CardContent>
          </Card>
        </Grid>
        <Grid item xs={12} sm={12} md={3} lg={3}>
          <ListTable />
        </Grid>
      </Grid>
    </div>
  );
}
