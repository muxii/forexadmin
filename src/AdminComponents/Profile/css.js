import { makeStyles, fade } from "@material-ui/core/styles";
export const drawerWidth = 240;

export const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    backgroundColor: "#f2f2f2"
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary
  },
  title: {
    fontSize: "20px",
    color: "grey"
  },
  divider: {
    marginTop: "-10px",
    marginBottom: "10px",
    borderBottom: "1px solid grey"
  },
  cardContainer: {
    padding: "20px",
    [theme.breakpoints.down("sm")]: {
      padding: "0px"
    }
  },
  secondForm: {
    marginLeft: "10px",
    [theme.breakpoints.down("sm")]: {
      marginLeft: "0px",
      marginTop: "20px"
    }
  },
  checkboxButton: {
    marginTop: "10px",
    [theme.breakpoints.down("sm")]: {
      marginTop: "10px"
    }
  },
  submitButton: {
    textTransform: "none",
    backgroundColor: "#e7505a",
    color: "white",
    [theme.breakpoints.down("sm")]: {
      marginTop: "0px"
    }
  }
}));
