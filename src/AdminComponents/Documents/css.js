import { makeStyles, fade } from "@material-ui/core/styles";
export const drawerWidth = 240;

export const useStyles = makeStyles(theme => ({
    root: {
        flexGrow: 1,
        backgroundColor: "#f2f2f2",
        boxShadow:"0px 2px 3px 2px rgba(0, 0, 0, 0.1)"
      },
      chartDiv:{
        padding:'10px',
        margin:'20px',
        backgroundColor:'white',
        boxShadow:"0px 2px 3px 2px rgba(0, 0, 0, 0.1)"
    },
    charHeader:{
        color:'20303D',
        fontSize:'20px'
    },
    text:{
        fontSize:'14px',
        color:"#20303D"
    },
    inputfield:{
        width:'100%'
    },
    border:{
        border: "1px solid #c2cad8",
        padding:'5px',
        margin:"5px"
    },
    drop:{
        marginTop:'30px',
        backgroundColor:'#3FC9D5',
        color:'white',
        "&:hover":{
            backgroundColor:'#3FC9D5',
            color:'white'
        }
    }

    }))