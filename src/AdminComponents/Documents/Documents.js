import React, { useCallback, useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import Appbar from "../Appbar/Appbar";
import Appbar1 from "../Appbar1/Appbar1";
import MobileAppbar from "../MobileAppbar/MobileAppbar";
import UpperTab from "../Register/UpperTab/UpperTab";
import LowerTab from "../LowerTab/LowerTab";
import ListTable from "../ListTable/ListTable";
import { useStyles } from "./css";
import { Button, Divider, FormControl, InputLabel, NativeSelect } from "@material-ui/core";
import { useDropzone } from 'react-dropzone'

export const Documents = () => {
    const [documentType, setDocumentType] = useState('')
    const [document,setDocument]=useState('');
    const classes = useStyles();
    const onDrop = useCallback((acceptedFiles) => {
       console.log(acceptedFiles)
    }, [])
    const { getRootProps, getInputProps, isDragActive } = useDropzone({ onDrop })
    

    const handleChange = (e) => {
        if (e.target.name === 'documenttype') {
            setDocumentType(e.target.value);
        }
        else if(e.target.name === 'document'){
            setDocument(e.target.value);
        }
        else{

        }
    }
    return (
        <div className={classes.root}>
            <Appbar />
            <Appbar1 />
            <MobileAppbar />
            <Grid container spacing={0}>
                <Grid item xs={12} sm={12} md={9} lg={9}>
                    <div className={classes.chartDiv}>
                        <p className={classes.charHeader}>My Documents</p>
                        <Divider />
                        <p className={classes.text}>We are committed to protecting the best interests of our clients. To provide the highest level of security to your account, we require Account Identification.</p>
                        <p className={classes.text}>For this, you just need to send following documents</p>
                        <h3 className={classes.charHeader}>
                            Attachment - Max Size 10 MB
                   </h3>
                        <div>
                            <Grid container spacing={0}>
                                <Grid item xs={12} lg={4}>
                                    <div> Document Type</div>
                                    <div className={classes.border}>
                                        <FormControl className={classes.inputfield}>
                                            <NativeSelect
                                                id="demo-customized-select-native"
                                                value={documentType}
                                                name="documenttype"
                                                onChange={handleChange}
                                                fullWidth={true}
                                                style={{ minWidth: '80%' }}
                                                disableUnderline={true}
                                            >
                                                <option aria-label="None" value="Select Document Type" />
                                                <option value="">Select Document Type</option>
                                                <option value={10}>Ten</option>
                                                <option value={20}>Twenty</option>
                                                <option value={30}>Thirty</option>
                                            </NativeSelect>
                                        </FormControl>
                                    </div>
                                </Grid>
                                <Grid item xs={12} lg={4}>
                                    <div> Document</div>
                                    <div className={classes.border}>
                                        <FormControl className={classes.inputfield}>

                                            <NativeSelect
                                                id="demo-customized-select-native"
                                                value={document}
                                                name="document"
                                                onChange={handleChange}
                                                disableUnderline={true}
                                            >
                                                <option aria-label="None" value="Select Documents" />
                                                <option value="">Select Documents</option>
                                                <option value={10}>Ten</option>
                                                <option value={20}>Twenty</option>
                                                <option value={30}>Thirty</option>
                                            </NativeSelect>
                                        </FormControl>
                                    </div>
                                </Grid>
                                <Grid item xs={12} lg={4} >
                                    <div {...getRootProps()}>
                                        <input {...getInputProps()} className={classes.drop}/>
                                        {
                                            isDragActive ?
                                                <Button  className={classes.drop}>Select Files</Button> :
                                                <Button  className={classes.drop}>Select +</Button>
                                        }
                                    </div>
                                </Grid>
                            </Grid>
                        </div>
                    </div>
                </Grid>
                <Grid item xs={12} sm={12} md={3} lg={3}>
                    <ListTable />
                </Grid>
            </Grid>
        </div>
    )
}

export default Documents;
