import { makeStyles, fade } from "@material-ui/core/styles";
export const drawerWidth = 240;

export const useStyles = makeStyles(theme => ({
  root: {
    width: "100%",
    maxWidth: 360,
    backgroundColor: theme.palette.background.paper
  },
  listItemText: {
    marginTop: "-10px",
    fontSize: "15px",
    fontWeight: "bold"
  },
  listContainer: {
    height: "100vh",
    backgroundColor: "#1e1e1e",
    color: "white"
  },
  summaryText: {
    textAlign: "center",
    marginTop: "-2px",
    fontSize: "23px"
  },
  showButton: {
    textTransform: "none",
    color: "white",
    marginLeft: "30px"
  },
  underline: {
    display: "block",
    marginLeft: "auto",
    marginRight: "auto",
    marginTop: "-10px",
    marginBottom: "20px",
    borderBottom: "1px solid white",
    width: "25%"
  },
  desktopView: {
    display: "block",
    [theme.breakpoints.down("sm")]: {
      display: "none"
    }
  }
}));
