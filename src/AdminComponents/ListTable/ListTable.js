import React, { useState } from "react";
import { makeStyles } from "@material-ui/core/styles";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import Button from "@material-ui/core/Button";
import { useStyles } from "./css";

export default function SimpleList() {
  const classes = useStyles();
  const [show, setShow] = useState("");

  const handleOpen = () => {
    setShow(true);
  };

  const handleClose = () => {
    setShow(false);
  };

  return (
    <div className={classes.desktopView}>
      <div className={classes.root}>
        <List
          className={classes.listContainer}
          component="nav"
          aria-label="main mailbox folders"
        >
          <h2 className={classes.summaryText}>Account Summary</h2>
          <div className={classes.underline}></div>
          <ListItem>
            <ListItemText
              classes={{ primary: classes.listItemText }}
              primary="Package"
            />
            <ListItemText
              classes={{ primary: classes.listItemText }}
              align="right"
              primary="Mini"
            />
          </ListItem>
          <ListItem>
            <ListItemText
              classes={{ primary: classes.listItemText }}
              primary="Balance"
            />
            <ListItemText
              classes={{ primary: classes.listItemText }}
              align="right"
              primary="$-4,496.64"
            />
          </ListItem>
          <ListItem>
            <ListItemText
              classes={{ primary: classes.listItemText }}
              primary="Open P/L"
            />
            <ListItemText
              classes={{ primary: classes.listItemText }}
              align="right"
              primary="$0.00"
            />
          </ListItem>
          <ListItem>
            <ListItemText
              classes={{ primary: classes.listItemText }}
              primary="Net Equity"
            />
            <ListItemText
              classes={{ primary: classes.listItemText }}
              align="right"
              primary="$-4,496.64"
            />
          </ListItem>
          <ListItem>
            <ListItemText
              classes={{ primary: classes.listItemText }}
              primary="Margin"
            />
            <ListItemText
              classes={{ primary: classes.listItemText }}
              align="right"
              primary="$0.00"
            />
          </ListItem>
          <ListItem>
            <ListItemText
              classes={{ primary: classes.listItemText }}
              primary="Free Margin"
            />
            <ListItemText
              classes={{ primary: classes.listItemText }}
              align="right"
              primary="$-4,496.64"
            />
          </ListItem>
          <ListItem>
            <ListItemText
              classes={{ primary: classes.listItemText }}
              primary="Margin Level"
            />
            <ListItemText
              classes={{ primary: classes.listItemText }}
              align="right"
              primary="0.00 %"
            />
          </ListItem>
          <ListItem>
            <ListItemText
              classes={{ primary: classes.listItemText }}
              primary="Net Exposer"
            />
            <ListItemText
              classes={{ primary: classes.listItemText }}
              align="right"
              primary="$0.00"
            />
          </ListItem>

          {!show && (
            <>
              <Button onClick={handleOpen} className={classes.showButton}>
                Show More Details
              </Button>
            </>
          )}

          {show && (
            <>
              <ListItem>
                <ListItemText
                  classes={{ primary: classes.listItemText }}
                  primary="Trading Bonus"
                />
                <ListItemText
                  classes={{ primary: classes.listItemText }}
                  align="right"
                  primary="$0.00"
                />
              </ListItem>
              <ListItem>
                <ListItemText
                  classes={{ primary: classes.listItemText }}
                  primary="Cash back"
                />
                <ListItemText
                  classes={{ primary: classes.listItemText }}
                  align="right"
                  primary="$0.00"
                />
              </ListItem>
              <ListItem>
                <ListItemText
                  classes={{ primary: classes.listItemText }}
                  primary="Max Exposer"
                />
                <ListItemText
                  classes={{ primary: classes.listItemText }}
                  align="right"
                  primary="$0.00"
                />
              </ListItem>
              <ListItem>
                <ListItemText
                  classes={{ primary: classes.listItemText }}
                  primary="Pending Withdrawal"
                />
                <ListItemText
                  classes={{ primary: classes.listItemText }}
                  align="right"
                  primary="$0.00"
                />
              </ListItem>
              <Button onClick={handleClose} className={classes.showButton}>
                Show Less Details
              </Button>
            </>
          )}
        </List>
      </div>
    </div>
  );
}
