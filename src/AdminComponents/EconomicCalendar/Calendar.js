import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import Appbar from "../Appbar/Appbar";
import Appbar1 from "../Appbar1/Appbar1";
import MobileAppbar from "../MobileAppbar/MobileAppbar";
import UpperTab from "../Register/UpperTab/UpperTab";
import LowerTab from "../LowerTab/LowerTab";
import ListTable from "../ListTable/ListTable";
import { useStyles } from "./css";
import { Divider } from "@material-ui/core";

export const Calendar = () => {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <Appbar />
      <Appbar1 />
      <MobileAppbar />
      <Grid container spacing={0}>
        <Grid item xs={12} sm={12} md={9} lg={9}>
          <div className={classes.calendardiv}>
            <p className={classes.charHeader}>Economic Calendar</p>
            <Divider />
            <Grid
              container
              direction="row"
              justify="center"
              alignItems="center"
            >
              <iframe
                src="https://sslecal2.forexprostools.com?columns=exc_flags,exc_currency,exc_importance,exc_actual,exc_forecast,exc_previous&features=datepicker,timezone&countries=25,32,6,37,72,22,17,39,14,10,35,43,56,36,110,11,26,12,4,5&calType=week&timeZone=8&lang=1"
                width="650"
                height="467"
                frameborder="0"
                allowtransparency="true"
                marginwidth="0"
                marginheight="0"
              ></iframe>
            </Grid>
            <div className={classes.powered}>
              <span className={classes.insidediv}>
                <a
                  href="https://www.investing.com/"
                  rel="nofollow"
                  target="_blank"
                  className={classes.economicanchor}
                ></a>
              </span>
            </div>
          </div>
        </Grid>
        <Grid item xs={12} sm={12} md={3} lg={3}>
          <ListTable />
        </Grid>
      </Grid>
    </div>
  );
};

export default Calendar;
