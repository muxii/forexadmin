import { makeStyles, fade } from "@material-ui/core/styles";
export const drawerWidth = 240;

export const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    backgroundColor: "#f2f2f2"
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary
  },
calendardiv:{
    backgroundColor:'white',
    padding:'10px',
    margin:"20px",
    
    boxShadow:"0px 2px 3px 2px rgba(0, 0, 0, 0.1)"

},
charHeader:{
    color:'20303D',
    fontSize:'20px'
},
insidediv:{
    fontSize: "11px",
    color: "#333333",
    textDecoration: 'none'
},
economicanchor:{
    fontSize: "11px",
    color: "#06529D",
    fontWeight: "bold"
},
powered:{

fontFamily: "Arial Helvetica sans-serif"
}}))