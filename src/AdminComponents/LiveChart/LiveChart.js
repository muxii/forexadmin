import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import Appbar from "../Appbar/Appbar";
import Appbar1 from "../Appbar1/Appbar1";
import MobileAppbar from "../MobileAppbar/MobileAppbar";
import UpperTab from "../Register/UpperTab/UpperTab";
import LowerTab from "../LowerTab/LowerTab";
import ListTable from "../ListTable/ListTable";
import { useStyles } from "./css";
import { Divider } from "@material-ui/core";

export const LiveChart = () => {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <Appbar />
      <Appbar1 />
      <MobileAppbar />
      <Grid container spacing={0}>
        <Grid item xs={12} sm={12} md={9} lg={9}>
          <div className={classes.chartDiv}>
            <p className={classes.charHeader}>Live Rate & Charts</p>
            <Divider />
            <Grid
              container
              direction="row"
              justify="center"
              alignItems="center"
            >
              <iframe
                height="480"
                width="650"
                src="https://ssltvc.forexprostools.com/?pair_ID=1&height=480&width=650&interval=300&plotStyle=area&domain_ID=1&lang_ID=1&timezone_ID=7"
              ></iframe>
            </Grid>
          </div>
        </Grid>
        <Grid item xs={12} sm={12} md={3} lg={3}>
          <ListTable />
        </Grid>
      </Grid>
    </div>
  );
};

export default LiveChart;
