import React from "react";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import Appbar from "../Appbar/Appbar";
import Appbar1 from "../Appbar1/Appbar1";
import MobileAppbar from "../MobileAppbar/MobileAppbar";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import TextField from "@material-ui/core/TextField";
import Checkbox from "@material-ui/core/Checkbox";
import FormGroup from "@material-ui/core/FormGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import { useStyles } from "./css";
import ListTable from "../ListTable/ListTable";
import { Link } from "react-router-dom";

export default function FullWidthGrid() {
  const classes = useStyles();
  const [checked, setChecked] = React.useState(true);

  const handleChange = event => {
    setChecked(event.target.checked);
  };

  return (
    <div className={classes.root}>
      <Appbar />
      <Appbar1 />
      <MobileAppbar />
      <Grid container spacing={0}>
        <Grid
          item
          xs={12}
          sm={12}
          md={9}
          lg={9}
          className={classes.cardContainer}
        >
          <Card>
            <CardContent>
              <p className={classes.title}>New Withdrawal Request</p>

              <div className={classes.divider}></div>
              <Grid container spacing={0}>
                <Grid item xs={12} sm={12} md={6} lg={6}>
                  <div>
                    <label>Bank Account</label>
                    <select className={classes.dropdown}>
                      <option selected>SELECT BANK ACCOUNT</option>
                    </select>
                  </div>
                </Grid>
                <Grid item xs={12} sm={12} md={6} lg={6}>
                  <form
                    className={classes.secondForm}
                    noValidate
                    autoComplete="off"
                  >
                    <label>Withdrawal Amount</label> <br />
                    <TextField
                      size="small"
                      fullWidth
                      id="outlined-basic"
                      variant="outlined"
                    />
                    <br />
                    <br />
                  </form>
                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <form noValidate autoComplete="off">
                    <label>Remarks</label> <br />
                    <TextField
                      size="small"
                      multiline
                      rows={3}
                      fullWidth
                      id="outlined-basic"
                      variant="outlined"
                    />
                    <br />
                    <br />
                  </form>
                </Grid>
              </Grid>
              <br />
              <div className={classes.flexContainer}>
                <p></p>
                <Button className={classes.submitButton}>REQUEST</Button>
              </div>
            </CardContent>
          </Card>
        </Grid>
        <Grid item xs={12} sm={12} md={3} lg={3}>
          <ListTable />
        </Grid>
      </Grid>
    </div>
  );
}
