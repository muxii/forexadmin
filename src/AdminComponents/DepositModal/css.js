import { makeStyles, fade } from "@material-ui/core/styles";
export const drawerWidth = 240;

export const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    backgroundColor: "#f2f2f2"
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary
  },
  cardHeader: {
    color: "black"
  },
  modalContainer: {
    backgroundColor: "#f2f2f2",
    width: "200px",
    marginRight: "20px",
    [theme.breakpoints.down("sm")]: {
      marginLeft: "20%",
      marginRight: "20%",
      width: "60%",
      marginTop: "30px"
    }
  },
  image: {
    display: "block",
    marginLeft: "auto",
    marginRight: "auto",
    width: "65px",
    height: "65px"
    // marginTop: "30px"
  }
}));
