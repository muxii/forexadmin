import React from "react";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import DialogActions from "@material-ui/core/DialogActions";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogTitle from "@material-ui/core/DialogTitle";
import Slide from "@material-ui/core/Slide";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import { Link } from "react-router-dom";
import { useStyles } from "./css";

const Transition = React.forwardRef(function Transition(props, ref) {
  return <Slide direction="up" ref={ref} {...props} />;
});

export default function AlertDialogSlide(props) {
  const classes = useStyles();
  const [maxWidth, setMaxWidth] = React.useState("xl");

  return (
    <div>
      <Dialog
        maxWidth={maxWidth}
        open={props.isOpen}
        TransitionComponent={Transition}
        keepMounted
        onClose={props.CloseModal}
        aria-labelledby="alert-dialog-slide-title"
        aria-describedby="alert-dialog-slide-description"
      >
        <DialogTitle id="alert-dialog-slide-title">
          {"Select Mode of Payment"}
        </DialogTitle>
        <DialogContent>
          <DialogContentText id="alert-dialog-slide-description">
            <Grid container spacing={0}>
              <Grid item xs={12} sm={12} md={4} lg={4}>
                <Link
                  style={{ textDecoration: "none" }}
                  to="/forex4money/admin/deposit/addDeposit"
                >
                  <Card className={classes.modalContainer}>
                    <CardContent>
                      <p className={classes.cardHeader}>Online Payment</p>
                      <img
                        className={classes.image}
                        src="https://cdn.forex4money.com/Images/net-banking.png"
                      />
                    </CardContent>
                  </Card>
                </Link>
              </Grid>
              <Grid item xs={12} sm={12} md={4} lg={4}>
                <Link
                  style={{ textDecoration: "none" }}
                  to="/forex4money/admin/deposit/addDeposit"
                >
                  <Card className={classes.modalContainer}>
                    <CardContent>
                      <p className={classes.cardHeader}>Mega Transfer</p>
                      <img
                        className={classes.image}
                        src="https://cdn.forex4money.com/Images/mt-logo.png"
                      />
                    </CardContent>
                  </Card>
                </Link>
              </Grid>
              <Grid item xs={12} sm={12} md={4} lg={4}>
                <Link
                  style={{ textDecoration: "none" }}
                  to="/forex4money/admin/deposit/addDeposit"
                >
                  <Card className={classes.modalContainer}>
                    <CardContent>
                      <p className={classes.cardHeader}>Bank Transfer</p>
                      <img
                        className={classes.image}
                        src="https://cdn.forex4money.com/Images/bank-transfer.png"
                      />
                    </CardContent>
                  </Card>
                </Link>
              </Grid>
            </Grid>
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={props.closeModal} color="primary">
            Close
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
