import React from "react";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import DemoAppbar from "../DemoAppbar/DemoAppbar";
import DemoAppbar1 from "../DemoAppbar1/DemoAppbar1";
import DemoMobileBar from "../DemoMobileBar/DemoMobileBar";
import Card from "@material-ui/core/Card";
import Button from "@material-ui/core/Button";
import CardContent from "@material-ui/core/CardContent";
import Accordion from "@material-ui/core/Accordion";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import Typography from "@material-ui/core/Typography";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import { useStyles } from "./css";
import SearchIcon from "@material-ui/icons/Search";
import InputBase from "@material-ui/core/InputBase";
import ListTable from "../../ListTable/ListTable";
import PropTypes from "prop-types";
import { lighten, makeStyles } from "@material-ui/core/styles";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TablePagination from "@material-ui/core/TablePagination";
import TableRow from "@material-ui/core/TableRow";
import TableSortLabel from "@material-ui/core/TableSortLabel";
import Date from "../../../util/Date/Date";

export default function FullWidthGrid() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <DemoAppbar />
      <DemoAppbar1 />
      <DemoMobileBar />
      <Grid container spacing={0}>
        <Grid
          item
          xs={12}
          sm={12}
          md={9}
          lg={9}
          className={classes.cardContainer}
        >
          <CardTable />
        </Grid>
        <Grid item xs={12} sm={12} md={3} lg={3}>
          <ListTable />
        </Grid>
      </Grid>
    </div>
  );
}

// table

function createData(
  order,
  lotSize,
  profit,
  openTime,
  openRate,
  closeRate,
  orderProfit,
  netProfit,
  closeTime,
  action
) {
  return {
    order,
    lotSize,
    profit,
    openTime,
    openRate,
    closeRate,
    orderProfit,
    netProfit,
    closeTime,
    action
  };
}

const headCells = [
  {
    id: "order",
    numeric: false,
    disablePadding: true,
    label: "Order # Instrument"
  },
  {
    id: "lotSize",
    numeric: true,
    disablePadding: false,
    label: "Type Lot Size"
  },
  {
    id: "Take Profit Stop Loss",
    numeric: true,
    disablePadding: false,
    label: "Bank Name"
  },
  {
    id: "openTime",
    numeric: true,
    disablePadding: false,
    label: "Open Time"
  },
  {
    id: "openRate",
    numeric: true,
    disablePadding: false,
    label: "Open Rate"
  },
  {
    id: "closeRate",
    numeric: true,
    disablePadding: false,
    label: "Close Rate"
  },
  {
    id: "orderProfit",
    numeric: true,
    disablePadding: false,
    label: "Order Profit Swap"
  },
  {
    id: "netProfit",
    numeric: true,
    disablePadding: false,
    label: "Net Profit"
  },
  {
    id: "closeTime",
    numeric: true,
    disablePadding: false,
    label: "Close Time"
  },
  {
    id: "action",
    numeric: true,
    disablePadding: false,
    label: ""
  }
];

const rows = [
  createData(
    "145036 XAU/USD",
    "Buy 0.01000",
    "",
    "23-May-2017 10:06:52 AM",
    "1261.80000",
    "1294.63000",
    "$328.30 $0.00",
    "$328.30",
    "25-Sep-2017 11:26:09 AM",
    "Manual Close"
  ),
  createData(
    "145036 XAU/USD",
    "Buy 0.01000	",
    "32131",
    "23-May-2017 10:06:52 AM",
    "1261.80000",
    "1294.63000",
    "$328.30 $0.00",
    "$328.30",
    "25-Sep-2017 11:26:09 AM",
    "Manual Close"
  ),
  createData(
    "145036 XAU/USD",
    "Buy 0.01000	",
    "312312",
    "23-May-2017 10:06:52 AM",
    "1261.80000",
    "1294.63000",
    "$328.30 $0.00",
    "$328.30",
    "25-Sep-2017 11:26:09 AM",
    "Manual Close"
  ),
  createData(
    "145036 XAU/USD",
    "Buy 0.01000	",
    "",
    "23-May-2017 10:06:52 AM",
    "1261.80000",
    "1294.63000",
    "$328.30 $0.00",
    "$328.30",
    "25-Sep-2017 11:26:09 AM",
    "Manual Close"
  ),
  createData(
    "145036 XAU/USD",
    "Buy 0.01000	",
    "312312",
    "23-May-2017 10:06:52 AM",
    "1261.80000",
    "1294.63000",
    "$328.30 $0.00",
    "$328.30",
    "25-Sep-2017 11:26:09 AM",
    "Manual Close"
  )
];

function descendingComparator(a, b, orderBy) {
  if (b[orderBy] < a[orderBy]) {
    return -1;
  }
  if (b[orderBy] > a[orderBy]) {
    return 1;
  }
  return 0;
}

function getComparator(order, orderBy) {
  return order === "desc"
    ? (a, b) => descendingComparator(a, b, orderBy)
    : (a, b) => -descendingComparator(a, b, orderBy);
}

function stableSort(array, comparator) {
  const stabilizedThis = array.map((el, index) => [el, index]);
  stabilizedThis.sort((a, b) => {
    const order = comparator(a[0], b[0]);
    if (order !== 0) return order;
    return a[1] - b[1];
  });
  return stabilizedThis.map(el => el[0]);
}

function EnhancedTableHead(props) {
  const {
    classes,
    onSelectAllClick,
    order,
    orderBy,
    numSelected,
    rowCount,
    onRequestSort
  } = props;
  const createSortHandler = property => event => {
    onRequestSort(event, property);
  };

  return (
    <TableHead>
      <TableRow>
        <TableCell
          padding="checkbox"
          style={{
            backgroundColor: "lightgrey"
          }}
        >
          {/* <Checkbox
            indeterminate={numSelected > 0 && numSelected < rowCount}
            checked={rowCount > 0 && numSelected === rowCount}
            onChange={onSelectAllClick}
            inputProps={{ 'aria-label': 'select all desserts' }}
          /> */}
        </TableCell>
        {headCells.map(headCell => (
          <TableCell
            key={headCell.id}
            align={headCell.numeric ? "right" : "left"}
            padding={headCell.disablePadding ? "none" : "default"}
            sortDirection={orderBy === headCell.id ? order : false}
            style={{
              backgroundColor: "lightgrey",
              color: "black",
              fontSize: "14px",
              textAlign: "center"
            }}
          >
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : "asc"}
              onClick={createSortHandler(headCell.id)}
            >
              {headCell.label}
              {orderBy === headCell.id ? (
                <span className={classes.visuallyHidden}>
                  {order === "desc" ? "sorted descending" : "sorted ascending"}
                </span>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
}

EnhancedTableHead.propTypes = {
  classes: PropTypes.object.isRequired,
  numSelected: PropTypes.number.isRequired,
  onRequestSort: PropTypes.func.isRequired,
  onSelectAllClick: PropTypes.func.isRequired,
  order: PropTypes.oneOf(["asc", "desc"]).isRequired,
  orderBy: PropTypes.string.isRequired,
  rowCount: PropTypes.number.isRequired
};

const useToolbarStyles = makeStyles(theme => ({
  root: {
    paddingLeft: theme.spacing(2),
    paddingRight: theme.spacing(1)
  },
  highlight:
    theme.palette.type === "light"
      ? {
          color: theme.palette.secondary.main,
          backgroundColor: lighten(theme.palette.secondary.light, 0.85)
        }
      : {
          color: theme.palette.text.primary,
          backgroundColor: theme.palette.secondary.dark
        },
  title: {
    flex: "1 1 100%"
  }
}));

export function CardTable() {
  const classes = useStyles();
  const [order, setOrder] = React.useState("asc");
  const [orderBy, setOrderBy] = React.useState("calories");
  const [selected, setSelected] = React.useState([]);
  const [page, setPage] = React.useState(0);
  const [dense, setDense] = React.useState(false);
  const [rowsPerPage, setRowsPerPage] = React.useState(5);

  const handleRequestSort = (event, property) => {
    const isAsc = orderBy === property && order === "asc";
    setOrder(isAsc ? "desc" : "asc");
    setOrderBy(property);
  };

  const handleSelectAllClick = event => {
    if (event.target.checked) {
      const newSelecteds = rows.map(n => n.name);
      setSelected(newSelecteds);
      return;
    }
    setSelected([]);
  };

  const handleClick = (event, name) => {
    const selectedIndex = selected.indexOf(name);
    let newSelected = [];

    if (selectedIndex === -1) {
      newSelected = newSelected.concat(selected, name);
    } else if (selectedIndex === 0) {
      newSelected = newSelected.concat(selected.slice(1));
    } else if (selectedIndex === selected.length - 1) {
      newSelected = newSelected.concat(selected.slice(0, -1));
    } else if (selectedIndex > 0) {
      newSelected = newSelected.concat(
        selected.slice(0, selectedIndex),
        selected.slice(selectedIndex + 1)
      );
    }

    setSelected(newSelected);
  };

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = event => {
    setRowsPerPage(parseInt(event.target.value, 10));
    setPage(0);
  };

  const handleChangeDense = event => {
    setDense(event.target.checked);
  };

  const isSelected = name => selected.indexOf(name) !== -1;

  const emptyRows =
    rowsPerPage - Math.min(rowsPerPage, rows.length - page * rowsPerPage);

  return (
    <div className={classes.root}>
      <Paper className={classes.paper}>
        <div style={{ display: "flex", justifyContent: "space-between" }}>
          <p className={classes.tableTitle}>Closed Orders</p>
          <Button className={classes.addButton}>Zoom Icon</Button>
        </div>

        <div
          style={{ borderBottom: "1px solid grey", marginBottom: "10px" }}
        ></div>

        <TableContainer style={{ border: "1px solid grey" }}>
          <Table
            className={classes.table}
            aria-labelledby="tableTitle"
            size={dense ? "small" : "medium"}
            aria-label="enhanced table"
          >
            <EnhancedTableHead
              classes={classes}
              numSelected={selected.length}
              order={order}
              orderBy={orderBy}
              onSelectAllClick={handleSelectAllClick}
              onRequestSort={handleRequestSort}
              rowCount={rows.length}
            />
            <TableBody>
              {stableSort(rows, getComparator(order, orderBy))
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((row, index) => {
                  const isItemSelected = isSelected(row.name);
                  const labelId = `enhanced-table-checkbox-${index}`;

                  return (
                    <TableRow
                      hover
                      //   onClick={(event) => handleClick(event, row.name)}
                      role="checkbox"
                      aria-checked={isItemSelected}
                      tabIndex={-1}
                      key={row.name}
                      selected={isItemSelected}
                    >
                      <TableCell padding="checkbox">
                        {/* <Checkbox
                          checked={isItemSelected}
                          inputProps={{ 'aria-labelledby': labelId }}
                        /> */}
                      </TableCell>
                      <TableCell
                        component="th"
                        id={labelId}
                        scope="row"
                        padding="none"
                        align="center"
                      >
                        {row.order}
                      </TableCell>
                      <TableCell align="center">{row.lotSize}</TableCell>
                      <TableCell align="center">{row.profit}</TableCell>
                      <TableCell align="center">{row.openTime}</TableCell>
                      <TableCell align="center">{row.openRate}</TableCell>
                      <TableCell align="center">{row.closeRate}</TableCell>
                      <TableCell align="center">{row.orderProfit}</TableCell>
                      <TableCell align="center">{row.netProfit}</TableCell>
                      <TableCell align="center">{row.closeTime}</TableCell>
                      <TableCell align="center">{row.action}</TableCell>
                    </TableRow>
                  );
                })}
              {emptyRows > 0 && (
                <TableRow style={{ height: (dense ? 33 : 53) * emptyRows }}>
                  <TableCell colSpan={6} />
                </TableRow>
              )}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          rowsPerPageOptions={[5, 10, 25]}
          component="div"
          count={rows.length}
          rowsPerPage={rowsPerPage}
          page={page}
          onChangePage={handleChangePage}
          onChangeRowsPerPage={handleChangeRowsPerPage}
        />
      </Paper>
    </div>
  );
}
