import { makeStyles, fade } from "@material-ui/core/styles";
export const drawerWidth = 240;

export const useStyles = makeStyles(theme => ({
  grow: {
    flexGrow: 1
  },
  menuButton: {
    marginRight: theme.spacing(2)
  },
  appbarContainer: {
    backgroundColor: "#1e1e1e",
    color: "white",
    height: "50px"
  },
  title: {
    fontSize: "25px",
    marginTop: "-15px"
  },
  switchButton: {
    marginTop: "-10px",
    height: "35px",
    fontSize: "12px",
    backgroundColor: "#e7505a",
    color: "white",
    marginRight: "10px",
    "&:hover": {
      backgroundColor: "#e7505a"
    }
  },
  dashboardButton: {
    marginTop: "-10px",
    textTransform: "none",
    color: "white",
    marginRight: "10px"
  },
  dropdown: {
    marginTop: "-8px",
    textTransform: "none",
    color: "white",
    marginRight: "10px"
  },
  desktopView: {
    display: "block",
    [theme.breakpoints.down("sm")]: {
      display: "none"
    }
  },
  link: {
    textDecoration: "none",
    color: "black"
  }
}));
