import React from "react";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import { Link } from "react-router-dom";
import { useStyles } from "./css";

export default function PrimarySearchAppBar() {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [anchorE2, setAnchorE2] = React.useState(null);

  const handleClick1 = event => {
    setAnchorEl(event.currentTarget);
  };
  const handleClick2 = event => {
    setAnchorE2(event.currentTarget);
  };

  const handleClose1 = () => {
    setAnchorEl(null);
  };
  const handleClose2 = () => {
    setAnchorE2(null);
  };

  return (
    <div className={classes.desktopView}>
      <div className={classes.grow}>
        <AppBar position="static" className={classes.appbarContainer}>
          <Toolbar>
            <Typography
              edge="start"
              className={classes.title}
              variant="h9"
              noWrap
            >
              A/C No: F4M000063
            </Typography>

            <div className={classes.grow} />
            <div style={{ display: "flex" }}>
              <Link
                style={{ textDecoration: "none" }}
                to="/forex4money/admin/demo/closedDeals"
              >
                <Button className={classes.Buttons}>CLOSE DEALS</Button>
              </Link>
              <Link
                style={{ textDecoration: "none" }}
                to="/forex4money/admin/demo/accountStatement"
              >
                <Button className={classes.Buttons}>ACCOUNT STATEMENT</Button>
              </Link>
            </div>
          </Toolbar>
        </AppBar>
      </div>
    </div>
  );
}
