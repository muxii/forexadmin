import React from "react";
import clsx from "clsx";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Grid from "@material-ui/core/Grid";
import CssBaseline from "@material-ui/core/CssBaseline";
import List from "@material-ui/core/List";
import ButtonGroup from "@material-ui/core/ButtonGroup";
import Button from "@material-ui/core/Button";
import MenuItem from "@material-ui/core/MenuItem";
import Menu from "@material-ui/core/Menu";
import KeyboardArrowDownIcon from "@material-ui/icons/KeyboardArrowDown";
import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import InboxIcon from "@material-ui/icons/MoveToInbox";
import ExpandLessIcon from "@material-ui/icons/ExpandLess";
import MailIcon from "@material-ui/icons/Mail";
import { Link } from "react-router-dom";
import { useStyles } from "./css";

export default function PersistentDrawerRight() {
  const classes = useStyles();
  const theme = useTheme();
  const [open, setOpen] = React.useState(false);
  const [anchorEl, setAnchorEl] = React.useState(null);
  const [anchorE2, setAnchorE2] = React.useState(null);

  const handleClick1 = event => {
    setAnchorEl(event.currentTarget);
  };
  const handleClick2 = event => {
    setAnchorE2(event.currentTarget);
  };

  const handleClose1 = () => {
    setAnchorEl(null);
  };
  const handleClose2 = () => {
    setAnchorE2(null);
  };

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  const logout = () => {
    setAnchorE2(null);
    sessionStorage.clear();
  };

  return (
    <div className={classes.mobileView}>
      <div className={classes.root}>
        <CssBaseline />
        <AppBar
          position="fixed"
          className={clsx(classes.appBar, {
            [classes.appBarShift]: open
          })}
        >
          <Toolbar>
            <Typography variant="h4" noWrap className={classes.title}>
              Forex4Money
            </Typography>
            <IconButton
              color="inherit"
              aria-label="open drawer"
              edge="end"
              onClick={handleDrawerOpen}
              className={clsx(open && classes.hide)}
            >
              <MenuIcon />
            </IconButton>
          </Toolbar>
          <br />
          <AppBar position="static" className={classes.mobileAppbar}>
            <Toolbar>
              <Link to="/forex4money/admin/demo/dashboard">
                <Button className={classes.dashboardBtn}>Dashboard</Button>
              </Link>
              <div>
                <Button
                  className={classes.dropdown}
                  aria-controls="simple-menu"
                  aria-haspopup="true"
                  onClick={handleClick2}
                >
                  dinesh yadav <KeyboardArrowDownIcon />
                </Button>
                <Menu
                  style={{ marginTop: "30px" }}
                  id="simple-menu"
                  anchorEl={anchorE2}
                  keepMounted
                  open={Boolean(anchorE2)}
                  onClose={handleClose2}
                >
                  <Link
                    style={{ color: "black", textDecoration: "none" }}
                    to="/forex4money/admin/login"
                  >
                    <MenuItem onClick={logout}>
                      <i class="fa fa-angle-right" aria-hidden="true"></i>
                      &nbsp;Log Out
                    </MenuItem>
                  </Link>
                </Menu>
              </div>
            </Toolbar>
          </AppBar>
          <br />
          <Link
            style={{ textDecoration: "none" }}
            to="/forex4money/admin/live/dashboard"
          >
            <Button className={classes.switchButton}>
              SWITCH TO Live ACCOUNT
            </Button>
          </Link>
        </AppBar>
        <main
          className={clsx(classes.content, {
            [classes.contentShift]: open
          })}
        >
          <div className={classes.drawerHeader} />
          <br />
          <br />
          <br />
          <br />
          <div style={{ display: "flex", justifyContent: "space-between" }}>
            <div>
              <p>Balance</p>
              <p>Open P/L</p>
              <p>Net Equity</p>
              <p>Margin</p>
            </div>
            <div>
              <p>$-4,12345.12</p>
              <p>$0.00</p>
              <p>$-4,12345.12</p>
              <p>$0.00</p>
            </div>
          </div>
        </main>
        <Drawer
          className={classes.drawer}
          variant="persistent"
          anchor="top"
          open={open}
          classes={{
            paper: classes.drawerPaper
          }}
        >
          <div style={{ backgroundColor: "#e7505a", color: "white" }}>
            <div className={classes.drawerHeader}>
              <Grid
                justify="space-between" // Add it here :)
                container
                spacing={24}
              >
                <Grid item></Grid>
                <Grid item>
                  <IconButton onClick={handleDrawerClose}>
                    <ExpandLessIcon style={{ color: "#fff" }} />
                  </IconButton>
                </Grid>
              </Grid>
            </div>
            <Divider />

            <List>
              <Link
                style={{ color: "white", textDecoration: "none" }}
                to="/forex4money/admin/demo/closedDeals"
              >
                <ListItem button>
                  <ListItemText primary="CLOSED DEALS" />
                </ListItem>
              </Link>
              <Link
                style={{ color: "white", textDecoration: "none" }}
                to="/forex4money/admin/demo/accountStatement"
              >
                <ListItem button>
                  <ListItemText primary="ACCOUNT STATEMENT" />
                </ListItem>
              </Link>
            </List>
          </div>
        </Drawer>
      </div>
    </div>
  );
}
