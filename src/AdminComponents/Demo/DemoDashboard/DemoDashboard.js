import React, { useState, useEffect } from "react";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import DemoAppbar from "../DemoAppbar/DemoAppbar";
import DemoAppbar1 from "../DemoAppbar1/DemoAppbar1";
import DemoMobileBar from "../DemoMobileBar/DemoMobileBar";
import LowerTab from "../../LowerTab/LowerTab";
import ListTable from "../../ListTable/ListTable";
import { useStyles } from "./css";
import { Redirect } from "react-router-dom";
import SimpleTabs from "../../UpperGrid/UpperGrid";

export default function FullWidthGrid(props) {
  console.log(props);

  const [showfull, setShowFull] = useState(false);
  const [showfullBottom, setShowFullBottom] = useState(false);
  const [data, setData] = useState(sessionStorage.getItem("payload"));
  const [redirect, setRedirect] = useState(false);
  const [tab, setTab] = useState("");
  const classes = useStyles();
  const payload = JSON.parse(data);
  console.log(payload);

  useEffect(() => {
    if (data) {
      setRedirect(false);
    } else {
      setRedirect(true);
    }
  }, [props.location.state]);

  const fullwidth = (value, tab) => {
    setShowFull(value);
    setTab(tab);
  };
  const fullwidthBottom = (value, tab) => {
    setShowFullBottom(value);
    setTab(tab);
  };

  if (redirect) {
    return <Redirect to="login" />;
  }
  return (
    <div className={classes.root}>
      {!showfull && !showfullBottom && (
        <>
          <DemoAppbar />
          <DemoAppbar1 />
        </>
      )}
      <DemoMobileBar />
      <Grid container spacing={0}>
        {!showfull && !showfullBottom ? (
          <>
            <Grid item xs={12} sm={12} md={9} lg={9}>
              <br />
              <SimpleTabs showfull={showfull} fullwidth={fullwidth} />
              <br />
              <LowerTab
                showfullBottom={showfullBottom}
                fullwidthBottom={fullwidthBottom}
              />
            </Grid>
            <Grid item xs={12} sm={12} md={3} lg={3}>
              <ListTable />
            </Grid>
          </>
        ) : showfull === true && tab === "upper" && showfullBottom === false ? (
          <>
            <Grid container>
              <Grid item xs={12} sm={12} md={12} lg={12}>
                <SimpleTabs showfull={showfull} fullwidth={fullwidth} />
              </Grid>
            </Grid>
          </>
        ) : showfullBottom === true && tab === "lower" && !showfull ? (
          <>
            <Grid container>
              <Grid item xs={12} sm={12} md={12} lg={12}>
                <LowerTab
                  showfullBottom={showfullBottom}
                  fullwidthBottom={fullwidthBottom}
                />
              </Grid>
            </Grid>
          </>
        ) : (
          ""
        )}
      </Grid>
    </div>
  );
}
