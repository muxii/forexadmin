import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import MuiDialogActions from '@material-ui/core/DialogActions';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Typography from '@material-ui/core/Typography';
import {useStyles} from './css'

const styles = (theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(5),
   
  },
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
    color: theme.palette.grey[500],
  },
});

const DialogTitle = withStyles(styles)((props) => {
  const { children, classes, onClose, ...other } = props;
  return (
    <MuiDialogTitle  disableTypography className={classes.root} {...other}>
      <Typography variant="h6">{children}</Typography>
      {onClose ? (
        <IconButton aria-label="close" className={classes.closeButton} onClick={onClose}>
          <CloseIcon />
        </IconButton>
      ) : null}
    </MuiDialogTitle>
  );
});

const DialogContent = withStyles((theme) => ({
  root: {
    padding: theme.spacing(4),
  },
}))(MuiDialogContent);

const DialogActions = withStyles((theme) => ({
  root: {
    margin: 0,
    padding: theme.spacing(4),
  },
}))(MuiDialogActions);

export default function CustomizedDialogs(props) {
  console.log("Modal",props)
const classes=useStyles();
  return (
    <div>
      {/* <Button variant="outlined" color="primary" onClick={handleClickOpen}>
        Open dialog
      </Button> */}
      <Dialog onClose={props.handleClose} fullWidth classes={classes.modalroot}  aria-labelledby="customized-dialog-title" open={props.open}>
        <DialogTitle style={{minWidth:'50%'}} id="customized-dialog-title" onClose={props.handleClose}>
          Modal title
        </DialogTitle>
        <DialogContent dividers style={{minWidth:'50%'}}>
          <Typography gutterBottom style={{minWidth:'100%'}}>
          
         {props.errorMessage}
          </Typography>
         
        </DialogContent>
        <DialogActions>
          <Button autoFocus onClick={props.handleClose} style={{backgroundColor:'#F24C5C' ,color:'#ffff'}}>
            OK
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
}
