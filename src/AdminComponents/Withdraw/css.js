import { makeStyles, fade } from "@material-ui/core/styles";
export const drawerWidth = 240;

export const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    backgroundColor: "#f2f2f2"
  },
  paper: {
    padding: theme.spacing(2),
    textAlign: "center",
    color: theme.palette.text.secondary
  },
  cardContainer: {
    padding: "20px",
    [theme.breakpoints.down("sm")]: {
      padding: "0px"
    }
  },
  table: {
    minWidth: 750
  },
  visuallyHidden: {
    border: 0,
    clip: "rect(0 0 0 0)",
    height: 1,
    margin: -1,
    overflow: "hidden",
    padding: 0,
    position: "absolute",
    top: 20,
    width: 1
  },
  search: {
    position: "relative",
    borderRadius: theme.shape.borderRadius,
    backgroundColor: fade(theme.palette.common.white, 0.15),
    "&:hover": {
      backgroundColor: fade(theme.palette.common.white, 0.25)
    },
    border: "1px solid grey",
    marginRight: theme.spacing(2),
    width: "100%",
    height: "35px",
    backgroundColor: "#fff",
    [theme.breakpoints.up("sm")]: {
      marginLeft: theme.spacing(3),
      width: "auto"
    },
    [theme.breakpoints.down("sm")]: {
      marginLeft: "0px"
    }
  },
  searchIcon: {
    padding: theme.spacing(0, 2),
    height: "100%",
    position: "absolute",
    pointerEvents: "none",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    backgroundColor: "#fff"
  },
  inputRoot: {
    color: "black"
  },
  inputInput: {
    padding: theme.spacing(1, 1, 1, 0),
    paddingLeft: `calc(1em + ${theme.spacing(4)}px)`,
    transition: theme.transitions.create("width"),
    width: "100px",
    [theme.breakpoints.down("sm")]: {
      width: "100px"
    }
  },
  heading: {
    fontSize: "20px"
  },
  dropdownUpper: {
    fontSize: "15px",
    color: "black",
    width: "100%",
    height: "35px",
    outline: "0px"
  },
  dropdown: {
    fontSize: "14px",
    color: "black",
    width: "90px",
    height: "35px",
    outline: "0px"
  },
  containerDate: {
    border: "1px solid grey",
    width: "100%"
  },
  divider: {
    width: "100%",
    borderBottom: "1px solid grey"
  },
  searchButton: {
    backgroundColor: "#e7505a",
    color: "white",
    margin: "20px"
  },
  addButton: {
    backgroundColor: "#e7505a",
    color: "white",
    height: "30px",
    margin: "20px",
    "&:hover": {
      backgroundColor: "#e7505a",
      color: "white"
    }
  },
  tableTitle: {
    textAlign: "left",
    color: "black",
    fontSize: "20px"
  }
}));
