import React from "react";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Breadcrumbs from "@material-ui/core/Breadcrumbs";
import MobNavbar from "../WebDashboard/MobNavbar/MobNavbar";
import TopNavbar from "../WebDashboard/TopNavbar/TopNavbar";
import Navbar from "../WebDashboard/Navbar/Navbar";
import Contact from "../WebDashboard/Contact/Contact";
import Footer from "../WebDashboard/Footer/Footer";
import ContactForm from "../ContactForm/ContactForm";
import { useStyles } from "./css";
import { Link } from "react-router-dom";

export default function FullWidthGrid() {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <MobNavbar />
      <TopNavbar />
      <Navbar />
      <Grid container spacing={0}>
        <Grid container className={classes.paddingDiv}>
          <Grid item xs={12} sm={12} md={7} lg={7}>
            <div>
              <Breadcrumbs aria-label="breadcrumb" className={classes.titleTop}>
                <Link to="/" className={classes.link}>
                  <b className={classes.breadHome}>Home</b>
                </Link>
                <Typography color="textPrimary">
                  <b className={classes.breadText}>Tools</b>
                </Typography>
                <Typography color="textPrimary">
                  <b className={classes.breadText}>Promotions</b>
                </Typography>
              </Breadcrumbs>
              <h1 className={classes.title}>Promotions</h1>
              <div className={classes.bottomBorder}></div>
              <p className={classes.para}>
                When you register with us for profit trading account, it is
                guaranteed that the experience will be overwhelming. We are
                committed offering the best services and the maximum profits.
                Though there is a great scope for making money in the niche, we
                strive hard to make it further interesting and profitable. Our
                approach is to offer the maximum benefits to you so that we get
                the recognition of the most-trusted partner in the niche.
              </p>
              <p className={classes.para}>
                What if you get a part of your deposit money back as Welcome
                Bonus? Isn’t it a mind-boggling idea indeed? Yes, at
                profit4money, we add a part of your money to your trading
                balance. You have some more money for trading. Since you get a
                rise in the initial amount, higher earnings await for you.
              </p>
              <p className={classes.para}>
                Cash back promotions are popular worldwide because they make the
                game further interesting. With profit4money, you can be assured
                about great opportunities for earnings.
              </p>
              <p className={classes.para}>
                Rebate offers or rebate bonus pay you for trading activities
                regardless of the loss or profits made in the individual trade.
                Since each trade brings a chance to earn money, it is an added
                benefit to the whole game.
              </p>
              <p className={classes.para}>
                Other than rebate offers and cash back offers, there are several
                promotions such as activity bonus which offers you additional
                benefits based on the frequency and volume of your profit
                trading. The only objective of providing bonus is to encourage
                you for doing further good. It makes the trading benefit further
                exciting. The unique business proposition suits the individual
                trader and enhances the experience.
              </p>

              <p className={classes.para}>
                Read the terms and conditions carefully before accepting the
                promotion offers by profit4money. We want to make it a
                transparent business. You get benefited by the fantastic offers
                provided by us. Our motto is to make the profit Trading business
                as much interesting as possible.
              </p>
            </div>
          </Grid>
          <Grid item item xs={12} sm={12} md={5} lg={5}>
            <ContactForm />
          </Grid>
        </Grid>
      </Grid>
      <Contact />
      <Footer />
    </div>
  );
}
