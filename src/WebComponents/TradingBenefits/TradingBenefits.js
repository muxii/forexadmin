import React from "react";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Breadcrumbs from "@material-ui/core/Breadcrumbs";
import MobNavbar from "../WebDashboard/MobNavbar/MobNavbar";
import TopNavbar from "../WebDashboard/TopNavbar/TopNavbar";
import Navbar from "../WebDashboard/Navbar/Navbar";
import Contact from "../WebDashboard/Contact/Contact";
import Footer from "../WebDashboard/Footer/Footer";
import ContactForm from "../ContactForm/ContactForm";
import { useStyles } from "./css";
import { Link } from "react-router-dom";

export default function FullWidthGrid() {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <MobNavbar />
      <TopNavbar />
      <Navbar />
      <Grid container spacing={0}>
        <Grid container className={classes.paddingDiv}>
          <Grid item xs={12} sm={12} md={7} lg={7}>
            <div>
              <Breadcrumbs aria-label="breadcrumb" className={classes.titleTop}>
                <Link to="/" className={classes.link}>
                  <b className={classes.breadHome}>Home</b>
                </Link>
                <Typography color="textPrimary">
                  <b className={classes.breadText}>Tools</b>
                </Typography>
                <Typography color="textPrimary">
                  <b className={classes.breadText}>Trading Benefits</b>
                </Typography>
              </Breadcrumbs>
              <h1 className={classes.title}>Trading Benefits</h1>
              <div className={classes.bottomBorder}></div>
              <h2 className={classes.subTitle}>
                Why traders choose profit4money?
              </h2>
              <div className={classes.bottomBorder}></div>
              <p className={classes.para}>
                At profit4moeny, we bring the best state-of-art trading base to
                the desirable traders and let them explore the best of global
                trading markets. Providing with appropriate suggestions about
                <b>where to invest</b> and the correct moves to be made, we tend
                to ease trading for beginners and even the great rollers.
              </p>
              <p className={classes.para}>
                We come up with a variety of financial products including CFDs,
                foreign exchange, commodity spot trading etc. We come up with a
                wide range of trading functions comprehensive solutions for
                end-users that are combined with robust platform and
                flexibility. Being market leaders who are obligated to provide a
                considerable level of services, we come forward with the
                greatest professionalism and business integrity.
              </p>
              <p className={classes.para}>
                Working along profit4money will let you attain a combination of
                several great qualities like technical innovation and industry
                expertise. We tend to deliver the best liquidity levels and real
                time quotes to our clients with a lot of other{" "}
                <b>trading benefits</b>. All these aspects ensure a fruitful
                trading environment for them.
              </p>
              <h2 className={classes.subTitle}>
                Liquidity and Global Connections
              </h2>
              <div className={classes.bottomBorder}></div>
              <ul>
                <li className={classes.para1}>
                  Directly access global markets that include shares, profit,
                  indices and commodities.
                </li>
                <li className={classes.para1}>
                  Access worldwide resources that may be required to attain
                  global liquidity at just a click of the mouse.
                </li>
                <li className={classes.para1}>
                  Get excellent customer support in all possible ways.
                </li>
                <li className={classes.para1}>
                  Comprehensive wide range of interbank market access tools that
                  could be helpful to professional traders possessing larger
                  accounts.
                </li>
              </ul>
              <h2 className={classes.subTitle}>Trading and Costs</h2>
              <div className={classes.bottomBorder}></div>
              <ul>
                <li className={classes.para1}>
                  Interbank spreads for several major pairs and competitive
                  spreads for various other instruments.
                </li>
                <li className={classes.para1}>
                  No extra charges or hidden fees.
                </li>
                <li className={classes.para1}>
                  Advanced technology for efficient protection.
                </li>
                <li className={classes.para1}>
                  The ability to add remove or change limit order for any
                  tradable instruments even when the market is closed.
                </li>
                <li className={classes.para1}>Hedging capabilities</li>
                <li className={classes.para1}>1-click deal execution</li>
                <li className={classes.para1}>
                  Intermediate payments for future profits
                </li>
                <li className={classes.para1}>
                  No margin calls. Full margin use for ensuring optimal
                  utilization of the complete trading capital involved.
                </li>
                <li className={classes.para1}>
                  User selectable leverage options
                </li>
              </ul>
              <h2 className={classes.subTitle}>Risk Management</h2>
              <div className={classes.bottomBorder}></div>
              <ul>
                <li className={classes.para1}>
                  Ensuring execution of limit orders, real-time margin
                  protection, and various tools for automatic risk management
                  along with protection of negative balance.
                </li>
                <li className={classes.para1}>
                  Advanced technology for ensuring uninterrupted maintenance of
                  orders even under market conditions that are volatile.
                </li>
              </ul>
              <p className={classes.para}>
                At profit4money, you are offered with the best customer support
                and commitment. With the various <b>trading benefits</b> we tend
                to create the most suitable trading platform. Our major interest
                is to support traders who want to invest along the rules of{" "}
                <b>profit factory forum</b>. With our efforts and algorithms we
                look into the fact that the highest good of the traders is
                considered.
              </p>
            </div>
          </Grid>
          <Grid item item xs={12} sm={12} md={5} lg={5}>
            <ContactForm />
          </Grid>
        </Grid>
      </Grid>
      <Contact />
      <Footer />
    </div>
  );
}
