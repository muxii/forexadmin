import React from "react";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Breadcrumbs from "@material-ui/core/Breadcrumbs";
import MobNavbar from "../WebDashboard/MobNavbar/MobNavbar";
import TopNavbar from "../WebDashboard/TopNavbar/TopNavbar";
import Navbar from "../WebDashboard/Navbar/Navbar";
import Contact from "../WebDashboard/Contact/Contact";
import Footer from "../WebDashboard/Footer/Footer";
import ContactForm from "../ContactForm/ContactForm";
import { useStyles } from "./css";
import { Link } from "react-router-dom";

export default function FullWidthGrid() {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <MobNavbar />
      <TopNavbar />
      <Navbar />
      <Grid container spacing={0}>
        <Grid container className={classes.paddingDiv}>
          <Grid item xs={12} sm={12} md={7} lg={7}>
            <div>
              <Breadcrumbs aria-label="breadcrumb" className={classes.titleTop}>
                <Link to="/" className={classes.link}>
                  <b className={classes.breadHome}>Home</b>
                </Link>
                <Typography color="textPrimary">
                  <b className={classes.breadText}>Tools</b>
                </Typography>
                <Typography color="textPrimary">
                  <b className={classes.breadText}>Trading Terms</b>
                </Typography>
              </Breadcrumbs>
              <h1 className={classes.title}>Trading Terms</h1>
              <div className={classes.bottomBorder}></div>
              <p className={classes.para}>
                The data on the website is based on the information received
                from research, report and from reliable sources. They cannot be
                displayed, copied, performed, altered, or use anywhere in any
                manner without profit4money's consent.
              </p>
              <p className={classes.para}>
                We are not liable for any damage to be made as to use of the
                site and or the site data contained in it as of
                unavailability/malfunctions of the site including its diverse
                services.
              </p>
              <p className={classes.para}>
                You will be liable for maintaining and offering the means by
                which to use the site, which might include, however, is not
                limited to, mode, a personal computer or any other access line.
              </p>
              <p className={classes.para}>
                You further consider all risks connected with the storage and
                use of data on your PC or on any other device by which you will
                get access to the site and the services.
              </p>
              <p className={classes.para}>
                You represent and warrant that you have implemented and plan to
                operate and maintain appropriate protection in relation to the
                security and control of access to your computer, computer
                viruses or other similarly harmful or inappropriate materials,
                devices, information or data.
              </p>
              <h4>Change Policy</h4>
              <p className={classes.para}>
                profit4money has all the right to update and modify the site
                content, consisting of discontinuing and adding services or
                instructions. So the site users shall have no doubt or complain
                or right to compensation of any type of damage that the client
                claims happened to him/her as of the change.
              </p>
              <p className={classes.para}>
                As per our trading terms, you agree that we are not responsible
                in any way in the event of any damage and failure to your PCs or
                any other system.
              </p>
              <h4>Anti Money Laundering Rules</h4>
              <p className={classes.para}>
                We are also obliged to comply with AML rules and does its
                efforts to confirm the client's identity. being our client you
                are cordially asked to offer copies of some documents. If you
                fail to offer such copies under one week- your account may get
                blocked until you offer copies of your documents.
              </p>
              <h4>Security of your personal data</h4>
              <p className={classes.para}>
                We use security systems as well as procedures to provide you a
                secure and safe trading environment and to secure your trading
                and financial information.
              </p>
              <p className={classes.para}>
                when you sign up with us, you will get a unique User ID, account
                number as well as password. We recommend our clients to not
                disclose any personal data with any outsider.
              </p>
              <h4>Indemnity</h4>
              <p className={classes.para}>
                You consent to pay us against all claims, liabilities and
                expenses that might result from any violation of this Agreement
                by you or by a system on which you use to access the website.
              </p>
            </div>
          </Grid>
          <Grid item item xs={12} sm={12} md={5} lg={5}>
            <ContactForm />
          </Grid>
        </Grid>
      </Grid>
      <Contact />
      <Footer />
    </div>
  );
}
