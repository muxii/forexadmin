import React from "react";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Breadcrumbs from "@material-ui/core/Breadcrumbs";
import MobNavbar from "../WebDashboard/MobNavbar/MobNavbar";
import TopNavbar from "../WebDashboard/TopNavbar/TopNavbar";
import Navbar from "../WebDashboard/Navbar/Navbar";
import Contact from "../WebDashboard/Contact/Contact";
import Footer from "../WebDashboard/Footer/Footer";
import ContactForm from "../ContactForm/ContactForm";
import { useStyles } from "./css";
import { Link } from "react-router-dom";

export default function FullWidthGrid() {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <MobNavbar />
      <TopNavbar />
      <Navbar />
      <Grid container spacing={0}>
        <Grid container className={classes.paddingDiv}>
          <Grid item xs={12} sm={12} md={7} lg={7}>
            <div>
              <Breadcrumbs aria-label="breadcrumb" className={classes.titleTop}>
                <Link to="/" className={classes.link}>
                  <b className={classes.breadHome}>Home</b>
                </Link>
                <Typography color="textPrimary">
                  <b className={classes.breadText}>profit</b>
                </Typography>
                <Typography color="textPrimary">
                  <b className={classes.breadText}>Indices</b>
                </Typography>
              </Breadcrumbs>
              <h1 className={classes.title}>Indices</h1>
              <div className={classes.bottomBorder}></div>
              <p className={classes.para}>
                You may have heard people discussing the performance of “market
                today” every now and then. Well, here is a bit into trading
                indices. Trading some of the major market indices has been a
                considerable way of investing. However, it does not have to be
                available to individual investors always. With profit4money you
                can access the market indices you desire in the form of CFDs and
                make the most benefit out of it.
              </p>
              <h2 className={classes.subTitle}>
                Why is CFD trading preferable?
              </h2>
              <div className={classes.bottomBorder}></div>
              <p className={classes.para}>
                CFD trading is known to be a form of trading derivatives. This
                means that you can deal on prices that have been derived on
                underlying markets and not underlying markets itself. It has
                been a popular form of trading since it enables traders to:
              </p>
              <ul>
                <li className={classes.para1}>
                  Leverage and let capital go further
                </li>
                <li className={classes.para1}>Go long or short</li>
                <li className={classes.para1}>Trade a wide range of markets</li>
                <li className={classes.para1}>
                  Mirror trading the underlying markets
                </li>
                <li className={classes.para1}>Hedge a portfolio</li>
                <li className={classes.para1}>Use DMA</li>
              </ul>
            </div>
          </Grid>
          <Grid item item xs={12} sm={12} md={5} lg={5}>
            <ContactForm />
          </Grid>
          <Grid item item xs={12} sm={12} md={12} lg={12}>
            <div>
              <h2 className={classes.subTitle}>
                Benefits of indices and CFD trading
              </h2>
              <h5>Leverage</h5>
              <p className={classes.para2}>
                CFDs allow your investment capitals go further since you would
                be required to deposit only a considerable portion of your the
                entire value of your trade to create a position. This deposit is
                called margin. Further deposits would depend on the size of your
                position and the margin factor for the market you choose.
                However, profit or loss would be dependent on the full size of
                your position.
              </p>
              <h5>Going Short</h5>
              <p className={classes.para2}>
                Since CFD trading also considers an agreement to exchange the
                considerable difference in the opening price and closing price
                of your position, it could be considered more flexible than any
                other forms of trading.
              </p>
              <h5>Wide Range of markets</h5>
              <p className={classes.para2}>
                With these contracts you could trade over 16,000 markets that
                include indices, shares, commodities, cryptocurrencies, profit
                and several others. You can trade over different markets from
                the same platform.
              </p>
              <h5>Similarity to underlying market</h5>
              <p className={classes.para2}>
                CFDs have been designed to very closely mimic the trading
                environments of various underlying markets. Buying CFDs is
                similar to buying shares for any brand. However, you will not be
                entitled to any shareholder privileges and your position would
                be adjusted to the offset the effects caused by any dividend
                payments.
              </p>
              <h5>Hedging your share portfolio </h5>
              <p className={classes.para2}>
                For example, you have a several number of shares with a company
                and decide to hold on to them for a long time. You may open a
                short position considering that the banking sector would offset
                any losses with CFDs. If things fall perfect, you will earn a
                profit. If not, you can close your CFD position and offset the
                losses against future profits for CGT purposes.
              </p>
              <p>
                <b>CFD trading</b> this way could prove to be beneficial.
              </p>
            </div>
          </Grid>
        </Grid>
      </Grid>
      <Contact />
      <Footer />
    </div>
  );
}
