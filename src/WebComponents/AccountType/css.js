import { makeStyles, fade } from "@material-ui/core/styles";

export const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1
  },
  titleTop: {
    marginTop: "150px",
    [theme.breakpoints.down("sm")]: {
      marginTop: "50px"
    }
  },
  breadHome: {
    color: "#25707D"
  },
  breadText: {
    color: "#333333",
    fontWeight: "200"
  },
  paddingDiv: {
    marginLeft: "10%",
    marginRight: "10%",
    marginBottom: "150px",
    [theme.breakpoints.down("sm")]: {
      marginLeft: "5%",
      marginRight: "5%",
      marginBottom: "50px"
    }
  },
  title: {
    marginTop: "20px",
    color: "#25707D",
    fontSize: "40px",
    [theme.breakpoints.down("sm")]: {
      fontSize: "30px"
    }
  },
  subTitle: {
    color: "#25707D",
    fontSize: "25px",
    [theme.breakpoints.down("sm")]: {
      fontSize: "20px"
    }
  },
  bottomBorder: {
    marginTop: "-10px",
    width: "60px",
    borderBottom: "3px solid #C34245"
  },
  para: {
    marginTop: "40px",
    color: "#333333",
    lineHeight: "30px",
    marginBottom: "27px",
    textAlign: "justify"
  },
  para1: {
    color: "#333333",
    lineHeight: "30px",
    textAlign: "justify"
  },
  moreButton: {
    textAlign: "left",
    marginBottom: "50px",
    textTransform: "none",
    backgroundColor: "#fff",
    color: "#25707D",
    fontSize: "15px",
    fontWeight: "900",
    cursor: "pointer",
    "&:hover": {
      backgroundColor: "#fff",
      color: "#25707D",
      width: "100px",
      borderBottom: "1px solid #25707D"
    }
  },
  cardContainer: {
    width: "350px",
    height: "800px",
    borderRadius: "10px",
    backgroundColor: "#F9F9F9",
    boxShadow: " 0px 0px 15px 0px rgba(0, 0, 0, 0.1)",
    [theme.breakpoints.down("sm")]: {
      width: "100%",
      height: "800px",
      marginBottom: "50px"
    }
  },
  cardContainer1: {
    width: "350px",
    height: "770px",
    border: "2px solid #25707d",
    marginTop: "-30px",
    paddingTop: "55px",
    borderRadius: "10px",
    backgroundColor: "#F9F9F9",
    boxShadow: " 0px 0px 15px 0px rgba(0, 0, 0, 0.1)",
    [theme.breakpoints.down("sm")]: {
      width: "100%",
      height: "800px",
      marginBottom: "50px"
    }
  },
  image: {
    display: "block",
    marginLeft: "auto",
    marginRight: "auto"
  },
  imageCircle: {
    display: "block",
    marginLeft: "auto",
    marginRight: "auto",
    // border: "1px solid black",
    width: "185px",
    height: "185px",
    borderRadius: "50%"
  },
  packName: {
    textAlign: "center",
    color: "#888888",
    fontSize: "30px"
  },
  packPara: {
    height: "100px",
    textAlign: "center",
    padding: "20px",
    color: "#236E7B",
    lineHeight: "1.5"
  },
  borderDiv: {
    borderBottom: "1px solid #d8edf1"
  },
  packDescription: {
    textAlign: "center",
    color: "#236E7B",
    fontWeight: "700"
  },
  packPrize: {
    textAlign: "center",
    color: "#25707D",
    fontSize: "22px"
  },
  signUpButton: {
    marginLeft: "20%",
    width: "60%",
    marginTop: "40px",
    marginBottom: "20px",
    backgroundColor: "#F9F9F9",
    color: "#25707D",
    height: "55px",
    lineHeight: "55px",
    fontSize: "16px",
    fontWeight: "bold",
    borderRadius: " 2px",
    border: "1px solid #25707D",
    cursor: "pointer",
    "&:hover": {
      backgroundColor: "#25707D",
      color: "#fff",
      width: "60%",
      borderBottom: "1px solid #25707D"
    }
  },
  signUpButton1: {
    marginLeft: "20%",
    width: "60%",
    marginTop: "40px",
    marginBottom: "20px",
    backgroundColor: "#25707D",
    color: "#F9F9F9",
    height: "55px",
    lineHeight: "55px",
    fontSize: "16px",
    fontWeight: "bold",
    borderRadius: " 2px",
    border: "1px solid #25707D",
    cursor: "pointer",
    "&:hover": {
      backgroundColor: "#fff",
      color: "#25707D",
      width: "60%",
      borderBottom: "1px solid #25707D"
    }
  },
  link: {
    textDecoration: "none"
  },
  bottomLine: {
    marginTop: "100px",
    borderBottom: "1px solid #25707D"
  }
}));
