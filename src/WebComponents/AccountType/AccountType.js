import React from "react";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Breadcrumbs from "@material-ui/core/Breadcrumbs";
import MobNavbar from "../WebDashboard/MobNavbar/MobNavbar";
import TopNavbar from "../WebDashboard/TopNavbar/TopNavbar";
import Navbar from "../WebDashboard/Navbar/Navbar";
import Contact from "../WebDashboard/Contact/Contact";
import Footer from "../WebDashboard/Footer/Footer";
import Button from "@material-ui/core/Button";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import { useStyles } from "./css";
import { Link } from "react-router-dom";

export default function FullWidthGrid() {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <MobNavbar />
      <TopNavbar />
      <Navbar />
      <Grid container spacing={0}>
        <Grid container className={classes.paddingDiv}>
          <Grid item xs={12} sm={12} md={12} lg={12}>
            <div>
              <Breadcrumbs aria-label="breadcrumb" className={classes.titleTop}>
                <Link to="/" className={classes.link}>
                  <b className={classes.breadHome}>Home</b>
                </Link>
                <Typography color="textPrimary">
                  <b className={classes.breadText}>Tools</b>
                </Typography>
                <Typography color="textPrimary">
                  <b className={classes.breadText}>Account Types</b>
                </Typography>
              </Breadcrumbs>
              <p className={classes.para}>
                While you may be interested in entering into the profit market
                and deriving the best profits with smart investments, at
                profit4money, we offer you with several different types of
                accounts based on your interest. It is important to understand
                that profit trading is something you need to pursue quite very
                seriously.
              </p>
              <p className={classes.para}>
                You should be prepared to make up the investments correctly as
                may be demanded. Success will not come to you so easily. You
                therefore need to proceed with the correct decisions,
                determining whether or not is a trading regimen good for you.
                Before you get to know the trading environment, it would be
                suggestible to open up a demo account rather than a live account
                straightforward. You can open a demo account at $50.00.
              </p>
              <p className={classes.para}>
                Opening a profit account would mean that you will have to be all
                prepared for the various aspects of investments and losses that
                are going to come your way through prevailing market trends. It
                would be rather better to be very selective about how you
                proceed with any further investments.
              </p>
              <h1 className={classes.title}>
                Available types of profit accounts
              </h1>
              <div className={classes.bottomBorder}></div>
              <p className={classes.para}>
                Apart from the informative demo account option, you have three
                main <b>profitlive</b> accounts. Based on the size of positions
                you want to create, you may choose from these and start with
                your investments in the financial markets. No matter what
                account type you select, you would need to practice considerably
                in order to make sure that you successfully drive in profits.
                You may select suitable ones from the following account types:
              </p>
              <h4>Mini Account</h4>
              <p className={classes.para}>
                Mini accounts are known to be small-cap accounts that let the
                traders to enter into the market with the least monetary
                investment. You can start at the minimum of $100. Such accounts
                tend to limit the position size of the accounts to smaller lots.
                These accounts are perfect when the traders are seeking to
                minimize the risk involved.
              </p>
              <h4>Standard Account</h4>
              <p className={classes.para}>
                As far as account nomenclature is considered, standard accounts
                tend to provide with the greatest variations. The minimum
                deposit for this profit account is known to be $2,000. Although
                traders are restricted for dealing with mini lots, there are
                some platforms that allow 1 position lot size on these accounts.
              </p>
              <h4>Premium Account</h4>
              <p className={classes.para}>
                The premium account type is the best for high rollers. The
                minimum amount for this account type is $5,000. The traders with
                this account type generally attract a lot of bonuses and chances
                of more profit earnings along with greater risk bearing.
              </p>
              <p className={classes.para}>
                Based on the different <b>types of accounts</b>, you can select
                the ones that you think will serve your purpose well.
              </p>
            </div>
            <Grid container spacing={2} style={{ marginTop: "100px" }}>
              <Grid item lg={4}>
                <Card className={classes.cardContainer}>
                  <CardContent>
                    <div className={classes.imageCircle}>
                      <img
                        className={classes.image}
                        src={require("../../assets/images/accountTypeMini.png")}
                      />
                    </div>
                    <h1 className={classes.packName}>MINI</h1>
                    <p className={classes.packPara}>
                      These accounts are perfect when the traders are seeking to
                      minimize the risk involved.
                    </p>
                    <div className={classes.borderDiv}></div>
                    <p className={classes.packDescription}>
                      4 PIP* TIGHT SPREAD
                    </p>
                    <div className={classes.borderDiv}></div>
                    <p className={classes.packDescription}>
                      TRAINING - PROVIDED
                    </p>
                    <div className={classes.borderDiv}></div>
                    <h1 className={classes.packPrize}>$100 - $1,999 MAX</h1>
                    <Button className={classes.signUpButton}>
                      SIGN UP &nbsp;
                      <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                    </Button>
                  </CardContent>
                </Card>
              </Grid>
              <Grid item lg={4}>
                <Card className={classes.cardContainer1}>
                  <CardContent>
                    <div className={classes.imageCircle}>
                      <img
                        className={classes.image}
                        src={require("../../assets/images/accountTypeStandard.png")}
                      />
                    </div>
                    <h1 className={classes.packName}>STANDARD</h1>
                    <p className={classes.packPara}>
                      These accounts tend to provide with greatest variations
                    </p>
                    <div className={classes.borderDiv}></div>
                    <p className={classes.packDescription}>
                      3 PIP* TIGHT SPREAD
                    </p>
                    <div className={classes.borderDiv}></div>
                    <p className={classes.packDescription}>
                      TRAINING - PROVIDED
                    </p>
                    <div className={classes.borderDiv}></div>
                    <h1 className={classes.packPrize}>$2,000 – $4,999 MAX</h1>
                    <Button className={classes.signUpButton1}>
                      SIGN UP &nbsp;
                      <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                    </Button>
                  </CardContent>
                </Card>
              </Grid>
              <Grid item lg={4}>
                <Card className={classes.cardContainer}>
                  <CardContent>
                    <div className={classes.imageCircle}>
                      <img
                        className={classes.image}
                        src={require("../../assets/images/accountTypePremium.png")}
                      />
                    </div>
                    <h1 className={classes.packName}>PREMIUM</h1>
                    <p className={classes.packPara}>
                      The premium account type is the best for high rollers and
                      attracts a lot of bonuses and chances of more profit
                      earnings along with greater risk-bearing.
                    </p>
                    <div className={classes.borderDiv}></div>
                    <p className={classes.packDescription}>
                      2 PIP* TIGHT SPREAD
                    </p>
                    <div className={classes.borderDiv}></div>
                    <p className={classes.packDescription}>
                      TRAINING - PROVIDED
                    </p>
                    <div className={classes.borderDiv}></div>
                    <h1 className={classes.packPrize}>$5,000 AND ABOVE</h1>
                    <Button className={classes.signUpButton}>
                      SIGN UP &nbsp;
                      <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                    </Button>
                  </CardContent>
                </Card>
              </Grid>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
      <Contact />
      <Footer />
    </div>
  );
}
