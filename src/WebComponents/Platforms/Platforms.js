import React from "react";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Breadcrumbs from "@material-ui/core/Breadcrumbs";
import MobNavbar from "../WebDashboard/MobNavbar/MobNavbar";
import TopNavbar from "../WebDashboard/TopNavbar/TopNavbar";
import Navbar from "../WebDashboard/Navbar/Navbar";
import Contact from "../WebDashboard/Contact/Contact";
import Footer from "../WebDashboard/Footer/Footer";
import ContactForm from "../ContactForm/ContactForm";
import { useStyles } from "./css";
import { Link } from "react-router-dom";

export default function FullWidthGrid() {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <MobNavbar />
      <TopNavbar />
      <Navbar />
      <Grid container spacing={0}>
        <Grid container className={classes.paddingDiv}>
          <Grid item xs={12} sm={12} md={7} lg={7}>
            <div>
              <Breadcrumbs aria-label="breadcrumb" className={classes.titleTop}>
                <Link to="/" className={classes.link}>
                  <b className={classes.breadHome}>Home</b>
                </Link>
                <Typography color="textPrimary">
                  <b className={classes.breadText}>Tools</b>
                </Typography>
                <Typography color="textPrimary">
                  <b className={classes.breadText}>Platforms</b>
                </Typography>
              </Breadcrumbs>
              <h1 className={classes.title}>
                Trade with best trading platform of India-profit4money
              </h1>
              <div className={classes.bottomBorder}></div>
              <p className={classes.para}>
                No matter whether you are a newbie or a professional trader, you
                will find the best platform to meet your trading needs.
                profit4money set f trading platforms are made to offer
                exceptional speed, as well as performance and assure you have
                great control of your profit trading experience.
              </p>
              <p className={classes.para}>
                profit4money offers you the choice of tree unique tradable
                platform crafted to meet your trading activity, personal style
                as well as net connection speed.
              </p>
              <p className={classes.para}>
                First account platform is a type of platform you can download.
                it fits best for skilled traders and the second platform is a
                web based platform. Each of our platforms fits your unique
                requirements.
              </p>
              <h2 className={classes.subTitle}>Mobile trading:</h2>
              <div className={classes.bottomBorder}></div>
              <p className={classes.para}>
                Take advantage of our mobile trading platform. No matter from
                which corner of the world you're, this will not impact your
                trading experience. you can rely on our advanced technology to
                offer you an exceptional trading experience.
              </p>
              <p className={classes.para}>
                It comes with advanced risk management ad full-featured trading
                options. Not just this, it offers you advanced analysis tools
                and trading View charting.
              </p>
              <p className={classes.para}>
                with this trading platform, you can quickly check account
                balances, view your trade history, and get price alerts. it is
                crafted for professional traders that demand advanced trading
                analytic tools and features in a single package.
              </p>
              <p className={classes.para}>
                Built for serious traders that demand sophisticated trading
                features and analytic tools in one powerful package.
              </p>
              <h2 className={classes.subTitle}>Web based platform:</h2>
              <div className={classes.bottomBorder}></div>
              <p className={classes.para}>
                Trade profit with web trading platform, our award-winning
                platform combines a set of technical tools, an intuitive
                interface, an advanced charting package and so on. profit4money
                web based platform is the best choice for mobile trader.
              </p>
              <p className={classes.para}>
                This trading platform offers traders obtain several features
                straight from a PC like open and exit positions, withdrawing and
                depositing funds and checking trading history without
                downloading. It is best for those who explore more often and
                require instant accessibility.
              </p>
              <h2 className={classes.subTitle}>FXnet Platform:</h2>
              <div className={classes.bottomBorder}></div>
              <p className={classes.para}>
                This trading platform needs downloading and is highly customized
                to fill the unique needs of both newbie or expert traders.
                FFXnet offers all of the essential data needed which can be
                shown on the device screen for ease of use.
              </p>
              <p className={classes.para}>
                This trading platform is especially crafted to offer the trader
                the opportunity to execute limit orders, single click dels as
                well as enables personalization as per trader's preference.
              </p>
              <p className={classes.para}>
                No doubt profit grading is among the most popular type of trade
                right now. Its various trading platforms offers you a chance to
                select the one that best fits your trading objectives.
              </p>
            </div>
          </Grid>
          <Grid item item xs={12} sm={12} md={5} lg={5}>
            <ContactForm />
          </Grid>
        </Grid>
      </Grid>
      <Contact />
      <Footer />
    </div>
  );
}
