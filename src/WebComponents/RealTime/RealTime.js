import React from "react";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Breadcrumbs from "@material-ui/core/Breadcrumbs";
import MobNavbar from "../WebDashboard/MobNavbar/MobNavbar";
import TopNavbar from "../WebDashboard/TopNavbar/TopNavbar";
import Navbar from "../WebDashboard/Navbar/Navbar";
import Contact from "../WebDashboard/Contact/Contact";
import Footer from "../WebDashboard/Footer/Footer";
import ContactForm from "../ContactForm/ContactForm";
import { useStyles } from "./css";
import { Link } from "react-router-dom";

export default function FullWidthGrid() {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <MobNavbar />
      <TopNavbar />
      <Navbar />
      <Grid container spacing={0}>
        <Grid container className={classes.paddingDiv}>
          <Grid item xs={12} sm={12} md={7} lg={7}>
            <div>
              <Breadcrumbs aria-label="breadcrumb" className={classes.titleTop}>
                <Link to="/" className={classes.link}>
                  <b className={classes.breadHome}>Home</b>
                </Link>
                <Typography color="textPrimary">
                  <b className={classes.breadText}>Tools</b>
                </Typography>
                <Typography color="textPrimary">
                  <b className={classes.breadText}>Real Time Charts</b>
                </Typography>
              </Breadcrumbs>
              <h1 className={classes.title}>Real Time Charts</h1>
              <div className={classes.bottomBorder}></div>
              <p className={classes.para}>
                A profit trading chart enables a trader to watch historical rates
                of currency exchange. profit4money brokers will offer free
                trading chart software for our potential customers who have open
                trading accounts. Traders use this charting software to know the
                probability flow on a particular currency pair.
              </p>
              <p className={classes.para}>
                profit4money offers direct and free access to our advanced and
                most useful market indicators and trading charts. our tools are
                highly customizable, easy to use and offer traders with amazing
                trading experience by letting them use incredible features
                offered by these tools to make an informed trading decision.
              </p>
              <h2 className={classes.subTitle}>
                5 strong reasons to use our trading chart software:
              </h2>
              <div className={classes.bottomBorder}></div>
              <ul>
                <li>
                  <p className={classes.para}>
                    Our charts can assist traders to track current price flow
                    and examine market trends.
                  </p>
                </li>
                <li>
                  <p className={classes.para}>
                    Multiple time frames enable traders to watch patterns and
                    trends easily
                  </p>
                </li>
                <li>
                  <p className={classes.para}>
                    Highly customizable tools to fit with trader's requirement
                  </p>
                </li>
                <li>
                  <p className={classes.para}>
                    Power to overlay various indicators for every commodity or
                    currency pair
                  </p>
                </li>
                <li>
                  <p className={classes.para}>
                    Can help traders in taking real-time decisions
                  </p>
                </li>
              </ul>
              <p className={classes.para}>
                profit4money offer trading chart software to our clients. our
                tool generally shows information as a bar chart, line chart, as
                well as candlestick chart depends on the trader desires.
              </p>
              <p className={classes.para}>
                Usually, traders use candlestick chart as it shows the most
                extensive range of data.
              </p>
              <p className={classes.para}>
                The trading chart usually shows opening prices, closing prices,
                low price, and high price points. Candlestick and bar charts
                show data on the closing and opening currency pair prices, low
                and high prices as well for the currency pair.
              </p>
              <h2 className={classes.subTitle}>
                Why is profit charting software important?
              </h2>
              <p className={classes.para}>
                Trading Chart is a very important tool in trading, and
                especially for technical traders. Technical search depends on
                the examination of chart patterns, price charts, price action
                trading, technical indicators, which makes reliable and stable
                trading chart very essential for such traders
              </p>
              <p className={classes.para}>
                Trading charts are the keys to unlock all the trading secrets.
                Trading charts are the keys to unlock all the trading secrets.
                We offer updated profit trading charts on the popular currency
                pairs and more detail on technical analyses with the use of
                Trading charts.
              </p>
              <p className={classes.para}>
                As you become more comfortable reading and understanding the
                real-time charts you will know how to add other important tools
                like technical analysis to calculate the changes in value and
                rate of market volatility.
              </p>
            </div>
          </Grid>
          <Grid item item xs={12} sm={12} md={5} lg={5}>
            <ContactForm />
          </Grid>
        </Grid>
      </Grid>
      <Contact />
      <Footer />
    </div>
  );
}
