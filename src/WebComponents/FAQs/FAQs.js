import React from "react";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Breadcrumbs from "@material-ui/core/Breadcrumbs";
import Accordion from "@material-ui/core/Accordion";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import MobNavbar from "../WebDashboard/MobNavbar/MobNavbar";
import TopNavbar from "../WebDashboard/TopNavbar/TopNavbar";
import Navbar from "../WebDashboard/Navbar/Navbar";
import Contact from "../WebDashboard/Contact/Contact";
import Footer from "../WebDashboard/Footer/Footer";
import ContactForm from "../ContactForm/ContactForm";
import { useStyles } from "./css";
import { Link } from "react-router-dom";

export default function FullWidthGrid() {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <MobNavbar />
      <TopNavbar />
      <Navbar />
      <Grid container spacing={0}>
        <Grid container className={classes.paddingDiv}>
          <Grid item xs={12} sm={12} md={7} lg={7}>
            <div>
              <Breadcrumbs aria-label="breadcrumb" className={classes.titleTop}>
                <Link to="/" className={classes.link}>
                  <b className={classes.breadHome}>Home</b>
                </Link>
                <Typography color="textPrimary">
                  <b className={classes.breadText}>profit</b>
                </Typography>
                <Typography color="textPrimary">
                  <b className={classes.breadText}>Trading FAQs</b>
                </Typography>
              </Breadcrumbs>
              <h1 className={classes.title}>Trading FAQs</h1>
              <div className={classes.bottomBorder}></div>
              <p className={classes.para}>
                There are various considerable elements that need to be known
                while you may be dealing with profit. Before you dive into the
                regular updates of your investments, it may be important to
                recognize several basics of profit. These include some commonly
                used terms and concepts that need to be understood.
              </p>
              <div className={classes.root}>
                <Accordion>
                  <AccordionSummary
                    style={{ backgroundColor: "#25707D", color: "#fff" }}
                    expandIcon={<ExpandMoreIcon style={{ color: "#fff" }} />}
                    aria-controls="panel1a-content"
                    id="panel1a-header"
                  >
                    <Typography className={classes.heading}>
                      What is a CFD?
                    </Typography>
                  </AccordionSummary>
                  <AccordionDetails>
                    <Grid container>
                      <Grid item xs={12}>
                        <p className={classes.para}>
                          A Contract for Difference or CFD is known to be an
                          agreement among the considerable two parties who may
                          have agreed to exchange the difference between the
                          closing price and opening price of a contract. CFDs
                          are known to be derivative products that let you
                          conduct trade on live movements among market prices.
                          All this is done without really having the ownership
                          of any underlying instruments with regards to the
                          contract.
                        </p>
                        <p className={classes.para}>
                          CFDs can be used for speculating future changes in the
                          market prices irrespective of the rise or fall in the
                          underlying markets or the profits due to increasing
                          price. However, with the wide variety of markets we
                          offer you may be able to access markets like never
                          before. With us you will have CFDs for indices, shares
                          and commodities.
                        </p>
                      </Grid>
                    </Grid>
                  </AccordionDetails>
                </Accordion>
                <br />
                <Accordion>
                  <AccordionSummary
                    style={{ backgroundColor: "#25707D", color: "#fff" }}
                    expandIcon={<ExpandMoreIcon style={{ color: "#fff" }} />}
                    aria-controls="panel2a-content"
                    id="panel2a-header"
                  >
                    <Typography className={classes.heading}>
                      What is profit?
                    </Typography>
                  </AccordionSummary>
                  <AccordionDetails>
                    <Grid container>
                      <Grid item xs={12}>
                        <p className={classes.para}>
                          profit is known to be the abbreviated term used for
                          Foreign Exchange. It is basically the process of
                          selling and buying currencies. The foreign exchange
                          market is known to be the most liquid and the largest
                          market in the world. The profit market operates 24
                          hours continually through Friday from Sunday night. It
                          comprises of currency speculators, central banks,
                          government, organizations, international and retail
                          investors. Over all these years, the size of profit
                          market has been growing constantly.
                        </p>
                      </Grid>
                    </Grid>
                  </AccordionDetails>
                </Accordion>
                <br />
                <Accordion>
                  <AccordionSummary
                    style={{ backgroundColor: "#25707D", color: "#fff" }}
                    expandIcon={<ExpandMoreIcon style={{ color: "#fff" }} />}
                    aria-controls="panel2a-content"
                    id="panel2a-header"
                  >
                    <Typography className={classes.heading}>
                      How does CFD and profit Trading Work?
                    </Typography>
                  </AccordionSummary>
                  <AccordionDetails>
                    <p className={classes.para}>
                      profit is known to be traded in currency pairs while CFDs
                      are financial instruments that are known to be valued in
                      specific considered currencies. EUR/USD (Euro/US Dollar),
                      USD/JPY (US Dollar/Japanese Yen), EUR/JPY (Euro/Japanese
                      Yen), GBP/USD (Great British Pound/US Dollar), and AUD/USD
                      (Australian Dollar/US Dollar) comprise some of the common
                      currency pairs. Such financial instrument or
                    </p>
                  </AccordionDetails>
                </Accordion>
                <br />
                <Accordion>
                  <AccordionSummary
                    style={{ backgroundColor: "#25707D", color: "#fff" }}
                    expandIcon={<ExpandMoreIcon style={{ color: "#fff" }} />}
                    aria-controls="panel2a-content"
                    id="panel2a-header"
                  >
                    <Typography className={classes.heading}>
                      What are the market trading hours?
                    </Typography>
                  </AccordionSummary>
                  <AccordionDetails>
                    <Grid container>
                      <Grid item xs={12}>
                        <p className={classes.para1}>
                          During North American and European winter time, this
                          weekly activity is supposed to begin at 22:00 GMT on
                          Sunday and continues until 21:00 GMT on Friday.
                          However, during Day Light Saving here, the market time
                          changes to begin on 21:00 GMT on Sunday and continues
                          till 20:00 on Friday. The activity hours may vary as
                          per unusual liquidity conditions and public holidays
                          or events that are likely to arise from various global
                          events. With the purpose of risk management or due to
                          liquidity, profit4money may alter the closing and
                          opening times. While most of the instruments are
                          traded 24 hours, there may be a few which are traded
                          as per special trading hours.
                        </p>
                      </Grid>
                    </Grid>
                  </AccordionDetails>
                </Accordion>
                <br />
                <Accordion>
                  <AccordionSummary
                    style={{ backgroundColor: "#25707D", color: "#fff" }}
                    expandIcon={<ExpandMoreIcon style={{ color: "#fff" }} />}
                    aria-controls="panel2a-content"
                    id="panel2a-header"
                  >
                    <Typography className={classes.heading}>
                      What are the tools needed to trade profit and CFD?
                    </Typography>
                  </AccordionSummary>
                  <AccordionDetails>
                    <Grid container>
                      <Grid item xs={12}>
                        <p className={classes.para1}>
                          To be able to conduct trading all you would need is a
                          funded trading account and internet connection. In
                          addition to this, it is suggestible to acquire
                          suitable knowledge and information about profit and CFD
                          trading along with various other financial instruments
                          that may be included. Appropriate knowledge and
                          trading tools will let you minimize the risks
                          involved.
                        </p>
                      </Grid>
                    </Grid>
                  </AccordionDetails>
                </Accordion>
                <br />
                <Accordion>
                  <AccordionSummary
                    style={{ backgroundColor: "#25707D", color: "#fff" }}
                    expandIcon={<ExpandMoreIcon style={{ color: "#fff" }} />}
                    aria-controls="panel2a-content"
                    id="panel2a-header"
                  >
                    <Typography className={classes.heading}>
                      What is the minimum age to be able to trade?
                    </Typography>
                  </AccordionSummary>
                  <AccordionDetails>
                    <Grid container>
                      <Grid item xs={12}>
                        <p className={classes.para1}>
                          What is the minimum age to be able to trade?
                        </p>
                      </Grid>
                    </Grid>
                  </AccordionDetails>
                </Accordion>
                <br />
                <Accordion>
                  <AccordionSummary
                    style={{ backgroundColor: "#25707D", color: "#fff" }}
                    expandIcon={<ExpandMoreIcon style={{ color: "#fff" }} />}
                    aria-controls="panel2a-content"
                    id="panel2a-header"
                  >
                    <Typography className={classes.heading}>
                      What is pip in profit?
                    </Typography>
                  </AccordionSummary>
                  <AccordionDetails>
                    <Grid container>
                      <Grid item xs={12}>
                        <p className={classes.para1}>
                          A pip is known to be an incremental price movement
                          which carries a specific value which is dependent on
                          the considerable market. In simple words, it is the
                          standard unit of measurement for recognizing the
                          change in value in the exchange rate.
                        </p>
                        <p className={classes.para1}>
                          Originally, pip was considered as the smallest that
                          would be done to a FX price. However, with the arrival
                          of other precise pricing methods, this original
                          definition does not hold importance. Traditionally, FX
                          prices had been quoted for a set of decimal places
                          that most commonly include four decimal places. A pip
                          was originally considered as a one point movement in
                          the last decimal place that has been quoted.
                        </p>
                        <p className={classes.para1}>
                          Many brokers are now known to quote profit price for an
                          extra decimal value. However, it may now be clear that
                          pip is no longer the final decimal place in a value.
                          It continues to be a standardized value across various
                          platforms and among different brokers which makes it
                          an important unit of measure. Without such a unit of
                          measure, there are chances that apples would be
                          compared to oranges. When considering generic points
                          pip certainly holds relevance. This measuring unit is
                          essential for margin calculation at various points.
                        </p>
                      </Grid>
                    </Grid>
                  </AccordionDetails>
                </Accordion>
                <br />
                <Accordion>
                  <AccordionSummary
                    style={{ backgroundColor: "#25707D", color: "#fff" }}
                    expandIcon={<ExpandMoreIcon style={{ color: "#fff" }} />}
                    aria-controls="panel2a-content"
                    id="panel2a-header"
                  >
                    <Typography className={classes.heading}>
                      What is a Spread?
                    </Typography>
                  </AccordionSummary>
                  <AccordionDetails>
                    <Grid container>
                      <Grid item xs={12}>
                        <p className={classes.para1}>
                          A spread is known to be the difference between the
                          Selling price and Buying price of two considerable
                          instruments. For instance, if EUR/USD is being traded
                          at 1.3098 (selling price) and 1.3100 (buying price)
                          then the spread would be 2 pips.
                        </p>
                      </Grid>
                    </Grid>
                  </AccordionDetails>
                </Accordion>
                <br />
                <Accordion>
                  <AccordionSummary
                    style={{ backgroundColor: "#25707D", color: "#fff" }}
                    expandIcon={<ExpandMoreIcon style={{ color: "#fff" }} />}
                    aria-controls="panel2a-content"
                    id="panel2a-header"
                  >
                    <Typography className={classes.heading}>
                      What does going “long” and “short” mean?
                    </Typography>
                  </AccordionSummary>
                  <AccordionDetails>
                    <Grid container>
                      <Grid item xs={12}>
                        <p className={classes.para1}>
                          Going “long’ refers to the situation when a trader
                          buys any asset expecting a rise in its future value.
                          It is often addressed as opening a long position. On
                          the other hand, opening a short position or going
                          “short” is when the trader sells away their asset
                          expecting a decline in its price so that it could be
                          bought later in future at a price that is
                          comparatively lower.
                        </p>
                      </Grid>
                    </Grid>
                  </AccordionDetails>
                </Accordion>
                <br />
                <Accordion>
                  <AccordionSummary
                    style={{ backgroundColor: "#25707D", color: "#fff" }}
                    expandIcon={<ExpandMoreIcon style={{ color: "#fff" }} />}
                    aria-controls="panel2a-content"
                    id="panel2a-header"
                  >
                    <Typography className={classes.heading}>
                      DDoes profit market have any central location?
                    </Typography>
                  </AccordionSummary>
                  <AccordionDetails>
                    <Grid container>
                      <Grid item xs={12}>
                        <p className={classes.para1}>
                          Unlike equities market the profit market operates
                          without a central location. The considerable
                          transactions are known to take place over the internet
                          mostly via mobile devices. This market is known to be
                          available 24 hours a day.
                        </p>
                      </Grid>
                    </Grid>
                  </AccordionDetails>
                </Accordion>
                <br />
                <Accordion>
                  <AccordionSummary
                    style={{ backgroundColor: "#25707D", color: "#fff" }}
                    expandIcon={<ExpandMoreIcon style={{ color: "#fff" }} />}
                    aria-controls="panel2a-content"
                    id="panel2a-header"
                  >
                    <Typography className={classes.heading}>
                      How are prices determined?
                    </Typography>
                  </AccordionSummary>
                  <AccordionDetails>
                    <Grid container>
                      <Grid item xs={12}>
                        <p className={classes.para1}>
                          There are different ways in which prices can change.
                          The value of an asset could be affected due to various
                          political and economic conditions. Also, they could
                          change considering inflation, interest rates, demand
                          and supply.
                        </p>
                      </Grid>
                    </Grid>
                  </AccordionDetails>
                </Accordion>
                <br />
                <Accordion>
                  <AccordionSummary
                    style={{ backgroundColor: "#25707D", color: "#fff" }}
                    expandIcon={<ExpandMoreIcon style={{ color: "#fff" }} />}
                    aria-controls="panel2a-content"
                    id="panel2a-header"
                  >
                    <Typography className={classes.heading}>
                      Are orders executed even when underlying markets are
                      closed?
                    </Typography>
                  </AccordionSummary>
                  <AccordionDetails>
                    <Grid container>
                      <Grid item xs={12}>
                        <p className={classes.para1}>
                          No. At profit4money, orders will not be executed when
                          the underlying markets are closed.
                        </p>
                      </Grid>
                    </Grid>
                  </AccordionDetails>
                </Accordion>
                <br />
                <Accordion>
                  <AccordionSummary
                    style={{ backgroundColor: "#25707D", color: "#fff" }}
                    expandIcon={<ExpandMoreIcon style={{ color: "#fff" }} />}
                    aria-controls="panel2a-content"
                    id="panel2a-header"
                  >
                    <Typography className={classes.heading}>
                      Does profit4money re-quote or change order prices?
                    </Typography>
                  </AccordionSummary>
                  <AccordionDetails>
                    <Grid container>
                      <Grid item xs={12}>
                        <p className={classes.para1}>
                          No. During several market conditions that may be laden
                          with greater volatility due to reasons like
                          communication latency, internet where market orders
                          cannot be executed at the price that may have been
                          requested. In order to avoid any situations where a
                          position may be opened with a market price that is
                          different than the one requested, the order is likely
                          to be rejected. This is done as a precaution against
                          any unwanted market orders that may thereafter allow
                          the trader to execute further trade at a price they
                          may expect. If limited orders cannot be executed at
                          the exact requested price, they are generally known to
                          be executed at the nearest price possible.
                        </p>
                      </Grid>
                    </Grid>
                  </AccordionDetails>
                </Accordion>
                <br />
                <Accordion>
                  <AccordionSummary
                    style={{ backgroundColor: "#25707D", color: "#fff" }}
                    expandIcon={<ExpandMoreIcon style={{ color: "#fff" }} />}
                    aria-controls="panel2a-content"
                    id="panel2a-header"
                  >
                    <Typography className={classes.heading}>
                      What are the costs of Index based CFDs and trading
                      commodity with profit4money?
                    </Typography>
                  </AccordionSummary>
                  <AccordionDetails>
                    <Grid container>
                      <Grid item xs={12}>
                        <p className={classes.para1}>
                          At profit4money, no indirect charges or fees are levied
                          on the basis of Index based CFDs and trading
                          community. The costs for clients are derived solely
                          based on spreads. It may be generally displayed on
                          your trading screen.
                        </p>
                      </Grid>
                    </Grid>
                  </AccordionDetails>
                </Accordion>
                <br />
                <Accordion>
                  <AccordionSummary
                    style={{ backgroundColor: "#25707D", color: "#fff" }}
                    expandIcon={<ExpandMoreIcon style={{ color: "#fff" }} />}
                    aria-controls="panel2a-content"
                    id="panel2a-header"
                  >
                    <Typography className={classes.heading}>
                      What is the validity of a spot transaction and what is
                      automatic rollover?
                    </Typography>
                  </AccordionSummary>
                  <AccordionDetails>
                    <Grid container>
                      <Grid item xs={12}>
                        <p className={classes.para1}>
                          In precious metals spot markets and profit markets,
                          rollover is known to be the procedure of extending the
                          date of settlement for an open position when it may
                          have reached its value date. In case of most of the
                          financial spot trades and currencies, opening a
                          position takes about two business days after the
                          execution date. The settlement period could be
                          extended up to two additional business days by rolling
                          over the position. Like Forward Mechanism, the
                          rollover process is known to involve adjustments
                          because of difference in interest rates based on the
                          type of position you may hold.
                        </p>
                      </Grid>
                    </Grid>
                  </AccordionDetails>
                </Accordion>
                <br />
                <Accordion>
                  <AccordionSummary
                    style={{ backgroundColor: "#25707D", color: "#fff" }}
                    expandIcon={<ExpandMoreIcon style={{ color: "#fff" }} />}
                    aria-controls="panel2a-content"
                    id="panel2a-header"
                  >
                    <Typography className={classes.heading}>
                      What if a CFD position is held open overnight?
                    </Typography>
                  </AccordionSummary>
                  <AccordionDetails>
                    <Grid container>
                      <Grid item xs={12}>
                        <p className={classes.para1}>
                          Any CFD products that may include commodities or
                          future based indices are known to be open until the
                          expiry of its underlying asset and are supposed to be
                          closed after that. CFDs that may be based on these
                          products cannot be rolled over and profit4money does
                          not charge any fees or rollover premiums. Unlike
                          commodity/index based CFDs, CFDs for stocks (shares)
                          are known to be exposed to overnight financing.
                        </p>
                      </Grid>
                    </Grid>
                  </AccordionDetails>
                </Accordion>
                <br />
                <Accordion>
                  <AccordionSummary
                    style={{ backgroundColor: "#25707D", color: "#fff" }}
                    expandIcon={<ExpandMoreIcon style={{ color: "#fff" }} />}
                    aria-controls="panel2a-content"
                    id="panel2a-header"
                  >
                    <Typography className={classes.heading}>
                      What are binary options?
                    </Typography>
                  </AccordionSummary>
                  <AccordionDetails>
                    <Grid container>
                      <Grid item xs={12}>
                        <p className={classes.para1}>
                          Binary options could be expressed as exciting simple
                          methods of trading in the financial markets. However,
                          this is determined by the fact that whether the price
                          of an asset would close above or below the current
                          price within a specific time period. Binary options
                          are often easy to execute and very profitable.
                        </p>
                      </Grid>
                    </Grid>
                  </AccordionDetails>
                </Accordion>
                <br />
                <Accordion>
                  <AccordionSummary
                    style={{ backgroundColor: "#25707D", color: "#fff" }}
                    expandIcon={<ExpandMoreIcon style={{ color: "#fff" }} />}
                    aria-controls="panel2a-content"
                    id="panel2a-header"
                  >
                    <Typography className={classes.heading}>
                      What is a call option?
                    </Typography>
                  </AccordionSummary>
                  <AccordionDetails>
                    <Grid container>
                      <Grid item xs={12}>
                        <p className={classes.para1}>
                          If you think that the price of any asset would close
                          above the current price up to expiration, you may buy
                          a call option.
                        </p>
                      </Grid>
                    </Grid>
                  </AccordionDetails>
                </Accordion>
                <br />
                <Accordion>
                  <AccordionSummary
                    style={{ backgroundColor: "#25707D", color: "#fff" }}
                    expandIcon={<ExpandMoreIcon style={{ color: "#fff" }} />}
                    aria-controls="panel2a-content"
                    id="panel2a-header"
                  >
                    <Typography className={classes.heading}>
                      What is a Put option?
                    </Typography>
                  </AccordionSummary>
                  <AccordionDetails>
                    <Grid container>
                      <Grid item xs={12}>
                        <p className={classes.para1}>
                          If you think that the price of any asset would close
                          down below the current price until expiration you may
                          buy the Put option.
                        </p>
                      </Grid>
                    </Grid>
                  </AccordionDetails>
                </Accordion>
                <br />
                <Accordion>
                  <AccordionSummary
                    style={{ backgroundColor: "#25707D", color: "#fff" }}
                    expandIcon={<ExpandMoreIcon style={{ color: "#fff" }} />}
                    aria-controls="panel2a-content"
                    id="panel2a-header"
                  >
                    <Typography className={classes.heading}>
                      What is meant by ‘in-the money’?
                    </Typography>
                  </AccordionSummary>
                  <AccordionDetails>
                    <Grid container>
                      <Grid item xs={12}>
                        <p className={classes.para1}>
                          In case your speculation for an asset’s price is
                          correct at the time of expiry, you would be
                          ‘in-the-money’.
                        </p>
                      </Grid>
                    </Grid>
                  </AccordionDetails>
                </Accordion>
                <br />
                <Accordion>
                  <AccordionSummary
                    style={{ backgroundColor: "#25707D", color: "#fff" }}
                    expandIcon={<ExpandMoreIcon style={{ color: "#fff" }} />}
                    aria-controls="panel2a-content"
                    id="panel2a-header"
                  >
                    <Typography className={classes.heading}>
                      What is meant by ‘out-of-the-money’?
                    </Typography>
                  </AccordionSummary>
                  <AccordionDetails>
                    <Grid container>
                      <Grid item xs={12}>
                        <p className={classes.para1}>
                          If the market reacts in the opposite direction from
                          your prediction about the price of an asset at the
                          time of expiry; you’re said to be ‘out-of-the-money’.
                        </p>
                      </Grid>
                    </Grid>
                  </AccordionDetails>
                </Accordion>
                <br />
                <Accordion>
                  <AccordionSummary
                    style={{ backgroundColor: "#25707D", color: "#fff" }}
                    expandIcon={<ExpandMoreIcon style={{ color: "#fff" }} />}
                    aria-controls="panel2a-content"
                    id="panel2a-header"
                  >
                    <Typography className={classes.heading}>
                      How much can I lose, if the investment I chose turns out
                      to wrong?
                    </Typography>
                  </AccordionSummary>
                  <AccordionDetails>
                    <Grid container>
                      <Grid item xs={12}>
                        <p className={classes.para1}>
                          Your loses will never exceed the amount of your
                          investment. In addition to this, there are several
                          options that even pay you back 10 percent amount of
                          your investments if they went wrong.
                        </p>
                      </Grid>
                    </Grid>
                  </AccordionDetails>
                </Accordion>
                <br />
                <Accordion>
                  <AccordionSummary
                    style={{ backgroundColor: "#25707D", color: "#fff" }}
                    expandIcon={<ExpandMoreIcon style={{ color: "#fff" }} />}
                    aria-controls="panel2a-content"
                    id="panel2a-header"
                  >
                    <Typography className={classes.heading}>
                      What if my option expires at strike rate?
                    </Typography>
                  </AccordionSummary>
                  <AccordionDetails>
                    <Grid container>
                      <Grid item xs={12}>
                        <p className={classes.para1}>
                          You will be considered “At-the-money” if the option
                          rate and expiration rate of your option are exactly
                          the same. In considerable situations, you may be
                          refunded 50 percent of your initial investments.
                        </p>
                      </Grid>
                    </Grid>
                  </AccordionDetails>
                </Accordion>
                <br />
                <Accordion>
                  <AccordionSummary
                    style={{ backgroundColor: "#25707D", color: "#fff" }}
                    expandIcon={<ExpandMoreIcon style={{ color: "#fff" }} />}
                    aria-controls="panel2a-content"
                    id="panel2a-header"
                  >
                    <Typography className={classes.heading}>
                      How much do I need to invest on trade?
                    </Typography>
                  </AccordionSummary>
                  <AccordionDetails>
                    <Grid container>
                      <Grid item xs={12}>
                        <p className={classes.para1}>
                          You can choose to trade any amount of your choice
                          ranging between $10 and $1000 based on your risk
                          tolerance and confidence level.
                        </p>
                      </Grid>
                    </Grid>
                  </AccordionDetails>
                </Accordion>
                <br />
                <Accordion>
                  <AccordionSummary
                    style={{ backgroundColor: "#25707D", color: "#fff" }}
                    expandIcon={<ExpandMoreIcon style={{ color: "#fff" }} />}
                    aria-controls="panel2a-content"
                    id="panel2a-header"
                  >
                    <Typography className={classes.heading}>
                      What will I receive for a successful investment?
                    </Typography>
                  </AccordionSummary>
                  <AccordionDetails>
                    <Grid container>
                      <Grid item xs={12}>
                        <p className={classes.para1}>
                          If your predictions turn out to be correct, you would
                          receive the payout that may have been set out for that
                          specific asset and time period. It is usually 70-90
                          percent of the trade value.
                        </p>
                      </Grid>
                    </Grid>
                  </AccordionDetails>
                </Accordion>
                <br />
                <Accordion>
                  <AccordionSummary
                    style={{ backgroundColor: "#25707D", color: "#fff" }}
                    expandIcon={<ExpandMoreIcon style={{ color: "#fff" }} />}
                    aria-controls="panel2a-content"
                    id="panel2a-header"
                  >
                    <Typography className={classes.heading}>
                      What options can I trade?
                    </Typography>
                  </AccordionSummary>
                  <AccordionDetails>
                    <Grid container>
                      <Grid item xs={12}>
                        <p className={classes.para1}>
                          At profit4money, you are offered with over 25assets
                          upon which you can enjoy binary option trades. These
                          include EUR/USD, GBP/JPY, GBP/USD, USD/CHF, AUD/USD,
                          EUR/JPY, Gold, Oil, S&P500 and many more.
                        </p>
                      </Grid>
                    </Grid>
                  </AccordionDetails>
                </Accordion>
                <br />
                <Accordion>
                  <AccordionSummary
                    style={{ backgroundColor: "#25707D", color: "#fff" }}
                    expandIcon={<ExpandMoreIcon style={{ color: "#fff" }} />}
                    aria-controls="panel2a-content"
                    id="panel2a-header"
                  >
                    <Typography className={classes.heading}>
                      What is an underlying asset?
                    </Typography>
                  </AccordionSummary>
                  <AccordionDetails>
                    <Grid container>
                      <Grid item xs={12}>
                        <p className={classes.para1}>
                          An underlying asset is the considerable financial
                          instrument on which the price of an option may be
                          based.
                        </p>
                      </Grid>
                    </Grid>
                  </AccordionDetails>
                </Accordion>
                <br />
                <Accordion>
                  <AccordionSummary
                    style={{ backgroundColor: "#25707D", color: "#fff" }}
                    expandIcon={<ExpandMoreIcon style={{ color: "#fff" }} />}
                    aria-controls="panel2a-content"
                    id="panel2a-header"
                  >
                    <Typography className={classes.heading}>
                      What is the expiry time?
                    </Typography>
                  </AccordionSummary>
                  <AccordionDetails>
                    <Grid container>
                      <Grid item xs={12}>
                        <p className={classes.para1}>
                          Expiry time refers to the date and time when an option
                          would expire. Your loss or payout would be calculated
                          based on the expiry time and your account would be
                          updated automatically.
                        </p>
                      </Grid>
                    </Grid>
                  </AccordionDetails>
                </Accordion>
                <br />
                <Accordion>
                  <AccordionSummary
                    style={{ backgroundColor: "#25707D", color: "#fff" }}
                    expandIcon={<ExpandMoreIcon style={{ color: "#fff" }} />}
                    aria-controls="panel2a-content"
                    id="panel2a-header"
                  >
                    <Typography className={classes.heading}>
                      What would be the time zone for expiry time?
                    </Typography>
                  </AccordionSummary>
                  <AccordionDetails>
                    <Grid container>
                      <Grid item xs={12}>
                        <p className={classes.para1}>
                          The time zone set for expiry time is GMT.
                        </p>
                      </Grid>
                    </Grid>
                  </AccordionDetails>
                </Accordion>
              </div>
              <p className={classes.para1}>
                Do you have a question which does not appear in this page?
                Kindly send it to us by E-mail on info@profit4money.com and we
                will be glad to add it.
              </p>
            </div>
          </Grid>
          <Grid item item xs={12} sm={12} md={5} lg={5}>
            <ContactForm />
          </Grid>
        </Grid>
      </Grid>
      <Contact />
      <Footer />
    </div>
  );
}
