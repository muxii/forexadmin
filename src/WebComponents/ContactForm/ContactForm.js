import React from "react";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import { useStyles } from "./css";

export default function FullWidthGrid() {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <div className={classes.paddingDiv}>
            <h2 className={classes.title}>Join now for free</h2>
            <div className={classes.bottomBorder}></div>
            <br />
            <br />
            <form>
              <TextField
                className={classes.inputDiv}
                fullWidth
                id="outlined-basic"
                placeholder="Enter Name"
                variant="outlined"
              />
              <br />
              <br />
              <TextField
                className={classes.inputDiv}
                fullWidth
                id="outlined-basic"
                placeholder="Enter Email Address"
                variant="outlined"
              />
              <br />
              <br />
              <div>
                <select className={classes.dropdown}>
                  <option selected>India</option>
                  <option>Australia</option>
                  <option>America</option>
                  <option>Newyork</option>
                  <option>Hong-Kong</option>
                  <option>Thailand</option>
                </select>
              </div>
              <br />
              <TextField
                className={classes.inputDiv}
                fullWidth
                id="outlined-basic"
                placeholder="Enter Mobile Number"
                variant="outlined"
              />
              <br />
              <br />
              <Button className={classes.joinButton}>
                Join Now &nbsp;
                <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
              </Button>
            </form>
          </div>
        </Grid>
      </Grid>
    </div>
  );
}
