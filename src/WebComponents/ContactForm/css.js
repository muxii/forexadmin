import { makeStyles, fade } from "@material-ui/core/styles";

export const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    "& label.Mui-focused": {
      color: "#25707D"
    },
    "& .MuiInput-underline:after": {
      borderBottomColor: "#25707D"
    },
    "& .MuiOutlinedInput-root": {
      "& fieldset": {
        borderColor: "#25707D"
      },
      "&:hover fieldset": {
        borderColor: "#25707D"
      },
      "&.Mui-focused fieldset": {
        borderColor: "#25707D"
      }
    }
  },
  paddingDiv: {
    backgroundColor: "#25707D",
    color: "#fff",
    padding: "30px",
    marginTop: "200px",
    marginLeft: "30px",
    [theme.breakpoints.down("sm")]: {
      marginLeft: "0px",
      marginTop: "30px"
    }
  },
  title: {
    marginTop: "-5px",
    color: "#fff",
    fontSize: "30px"
  },
  bottomBorder: {
    marginTop: "-10px",
    width: "60px",
    borderBottom: "3px solid #C34245"
  },
  inputDiv: {
    backgroundColor: "#1A5863",
    color: "#fff"
  },
  dropdown: {
    border: "1px solid #25707D",
    backgroundColor: "#1A5863",
    fontSize: "14px",
    color: "#fff",
    width: "100%",
    height: "60px",
    outline: "0px",
    [theme.breakpoints.down("sm")]: {
      width: "99.5%",
      height: "41px"
    }
  },
  joinButton: {
    width: "150px",
    textAlign: "left",
    marginBottom: "50px",
    textTransform: "none",
    backgroundColor: "#C34245",
    color: "#fff",
    padding: "10px",
    fontSize: "17px",
    fontWeight: "900",
    cursor: "pointer",
    "&:hover": {
      backgroundColor: "#fff",
      color: "#25707D",
      width: "100px",
      borderBottom: "1px solid #25707D"
    }
  },
  link: {
    textDecoration: "none"
  }
}));
