import { makeStyles, fade } from "@material-ui/core/styles";

export const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    marginLeft: "8%",
    marginRight: "8%",
    [theme.breakpoints.down("sm")]: {
      marginLeft: "3%",
      marginRight: "3%"
    }
  },
  topDiv1: {
    marginTop: "120px",
    [theme.breakpoints.down("sm")]: {
      marginTop: "50px"
    }
  },
  topDiv: {
    marginTop: "50px",
    [theme.breakpoints.down("sm")]: {
      marginTop: "10px"
    }
  },
  subTitle: {
    color: "#333333"
  },
  title: {
    marginTop: "-10px",
    color: "#25707D",
    fontSize: "40px",
    [theme.breakpoints.down("sm")]: {
      fontSize: "30px"
    }
  },
  bottomBorder: {
    marginTop: "-10px",
    width: "60px",
    borderBottom: "3px solid #C34245"
  },
  cardContainer: {
    border: "1px solid #e6e6e6",
    padding: "15px"
  },
  cardImg: {
    display: "block",
    marginLeft: "auto",
    marginRight: "auto"
  },
  rightContainer: {
    backgroundColor: "#f1f1f1",
    paddingLeft: "65px",
    paddingBottom: "125px",
    paddingTop: " 125px",
    position: "relative",
    [theme.breakpoints.down("sm")]: {
      paddingLeft: "25px"
    }
  },
  protectCard: {
    backgroundColor: "#fff",
    width: "200px",
    height: "100px",
    border: "1px solid #e6e6e6",
    padding: "15px"
  },
  protectPara: {
    color: "#333333",
    textAlign: "justify",
    lineHeight: "1.42857143"
  },
  link: {
    textDecoration: "none"
  }
}));
