import React from "react";
import Grid from "@material-ui/core/Grid";
import { Link } from "react-router-dom";
import { useStyles } from "./css";

export default function FullWidthGrid() {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <Grid container spacing={2}>
        <Grid item lg={6}>
          <div className={classes.topDiv1}>
            <p className={classes.subTitle}>We Accept most of</p>
            <h1 className={classes.title}>Payment Modes</h1>
            <div className={classes.bottomBorder}></div>
            <Grid container spacing={2} className={classes.topDiv}>
              <Grid item xs={12} sm={12} md={4} lg={4}>
                <div className={classes.cardContainer}>
                  <img
                    className={classes.cardImg}
                    src={require("../../../assets/images/paymentImg1.png")}
                  />
                </div>
              </Grid>
              <Grid item xs={12} sm={12} md={4} lg={4}>
                <div className={classes.cardContainer}>
                  <img
                    className={classes.cardImg}
                    src={require("../../../assets/images/paymentImg2.png")}
                  />
                </div>
              </Grid>
              <Grid item xs={12} sm={12} md={4} lg={4}>
                <div className={classes.cardContainer}>
                  <img
                    className={classes.cardImg}
                    src={require("../../../assets/images/paymentImg3.png")}
                  />
                </div>
              </Grid>
              <Grid item xs={12} sm={12} md={4} lg={4}>
                <div className={classes.cardContainer}>
                  <img
                    className={classes.cardImg}
                    src={require("../../../assets/images/paymentImg4.png")}
                  />
                </div>
              </Grid>
              <Grid item xs={12} sm={12} md={4} lg={4}>
                <div className={classes.cardContainer}>
                  <img
                    className={classes.cardImg}
                    src={require("../../../assets/images/paymentImg5.png")}
                  />
                </div>
              </Grid>
              <Grid item xs={12} sm={12} md={4} lg={4}>
                <div className={classes.cardContainer}>
                  <img
                    className={classes.cardImg}
                    src={require("../../../assets/images/paymentImg6.png")}
                  />
                </div>
              </Grid>
              <Grid item xs={12} sm={12} md={4} lg={4}>
                <div className={classes.cardContainer}>
                  <img
                    className={classes.cardImg}
                    src={require("../../../assets/images/paymentImg7.png")}
                  />
                </div>
              </Grid>
            </Grid>
          </div>
        </Grid>
        <Grid item lg={6}>
          <div className={classes.rightContainer}>
            <p className={classes.subTitle}>We put efforts to</p>
            <h1 className={classes.title}>Protect You</h1>
            <div className={classes.bottomBorder}></div>
            <Grid container spacing={2}>
              <p className={classes.subTitle}>
                We have Protected our Customers with
              </p>
              <Grid item xs={12} sm={12} md={6} lg={6}>
                <div className={classes.protectCard}>
                  <img
                    src={require("../../../assets/images/protectImg1.png")}
                  />
                </div>
              </Grid>
              <Grid item xs={12} sm={12} md={6} lg={6}>
                <div className={classes.protectCard}>
                  <img
                    src={require("../../../assets/images/protectImg2.png")}
                  />
                </div>
              </Grid>
              <Grid item lg={8}>
                <p className={classes.protectPara}>
                  profit International Gain Ltd., an investment firm licensed
                  and supervised by the Vanuatu Financial Services Commission
                  under license no. 15001.
                </p>
              </Grid>
              <Grid item lg={4}>
                <img src={require("../../../assets/images/protectImg3.png")} />
              </Grid>
            </Grid>
          </div>
        </Grid>
      </Grid>
    </div>
  );
}
