import React from "react";
import { makeStyles } from "@material-ui/core/styles";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import { Link } from "react-router-dom";

import { useStyles } from "./css";
import { Divider } from "@material-ui/core";

export default function FullWidthGrid() {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Grid container spacing={0} className={classes.footerContainer}>
        <Grid container className={classes.footerPadding1}>
          <Grid item xs={12} sm={12} md={4} lg={4}>
            <h2>profit4money</h2>
            <p className={classes.footerPara}>
              profit4money is amongst the most <br />
              favored and trusted companies <br />
              offering stocks, commodities and cfd <br />
              training.
            </p>

            <div className={classes.icon}>
              <a
                target="_blank"
                style={{ color: "white" }}
                href="https://www.facebook.com/myotopark/"
              >
                <i
                  style={{
                    backgroundColor: "#fff",
                    color: "#164F58",
                    padding: "10px",
                    height: "10px",
                    width: "10px",
                    border: "1px solid #71777D",
                    borderRadius: "50%",
                    fontSize: "15px"
                  }}
                  class="fa fa-facebook"
                  aria-hidden="true"
                ></i>
              </a>
              <a
                target="_blank"
                style={{ color: "white" }}
                href="https://twitter.com/myotopark"
              >
                <i
                  style={{
                    backgroundColor: "#fff",
                    color: "#164F58",
                    padding: "10px",
                    height: "10px",
                    width: "10px",
                    border: "1px solid #71777D",
                    borderRadius: "50%",
                    fontSize: "15px",
                    margin: "5px"
                  }}
                  class="fa fa-twitter"
                  aria-hidden="true"
                ></i>
              </a>
              <a
                target="_blank"
                style={{ color: "white" }}
                href="https://www.linkedin.com/company/myotopark/"
              >
                <i
                  style={{
                    backgroundColor: "#fff",
                    color: "#164F58",
                    padding: "10px",
                    height: "10px",
                    width: "10px",
                    border: "1px solid #71777D",
                    borderRadius: "50%",
                    fontSize: "15px"
                  }}
                  class="fa fa-linkedin"
                  aria-hidden="true"
                ></i>
              </a>
              <a
                target="_blank"
                style={{ color: "white" }}
                href="https://www.instagram.com/myotopark/"
              >
                <i
                  style={{
                    backgroundColor: "#fff",
                    color: "#164F58",
                    padding: "10px",
                    height: "10px",
                    width: "10px",
                    border: "1px solid #71777D",
                    borderRadius: "50%",
                    fontSize: "15px",
                    margin: "5px"
                  }}
                  class="fa fa-instagram"
                  aria-hidden="true"
                ></i>
              </a>
            </div>
          </Grid>
          <Grid item xs={12} sm={12} md={2} lg>
            <h4 className={classes.header}>Learn to Trade</h4>
            <div className={classes.footerList}>
              <p>profit</p>
              <p>Commodities</p>
              <p>Indices</p>
              <p>Stocks</p>
              <p>profit4money Conditions</p>
              <p>Trading FAQs</p>
              <p>Client Agreement</p>
              <p>Registration Process</p>
            </div>
          </Grid>
          <Grid item xs={12} sm={12} md={2} lg>
            <h4 className={classes.header}>Tools for Training</h4>
            <div className={classes.footerList}>
              <p>Daily Market Review</p>
              <p>Financial Calendar</p>
              <p>Real Time Charts</p>
              <p>Signals</p>
              <p>Platforms</p>
              <p>Live Market Rates</p>
              <p>Education Package</p>
            </div>
          </Grid>
          <Grid item xs={12} sm={12} md={2} lg>
            <h4 className={classes.header}>About profit4money</h4>
            <div className={classes.footerList}>
              <p>Our Vision</p>
              <p>Trading Benefits</p>
              <p>Trading Support</p>
              <p>profit4money Help Center</p>
              <p>Our Trading Policy</p>
              <p>Disclaimer</p>
              <p>Trading Terms</p>
              <p>Risk Caution</p>
              <p>Blog</p>
            </div>
          </Grid>
        </Grid>
      </Grid>
      <Grid item xs={12} sm={12} md={12} lg={12} className={classes.ParaFooter}>
        <div className={classes.footerPadding2}>
          <br />
          <div style={{ marginTop: "80px" }}></div>
          <p align="center">This site is operated by profit4money</p>
          <p>
            profit4money will never ask you for the Password of your account.
            For your security, sensitive information including your password
            should never be shared with a 3rd party.
          </p>
          <p>
            Trading financial instruments with margin carries a high level of
            risk, can work both for and against you, and can result in the loss
            of part or all of your investment (deposit). You should not invest
            money that you cannot afford to lose. Should you have any doubts,
            you should seek advice from an independent and suitably licensed
            financial advisor. Furthermore, you should ensure that you have
            sufficient time to manage your investments on an active basis.
            profit4money does not provide investment advice and the information
            provided herein is intended for marketing purposes only and should
            not be relied upon as investment advice. Any indication of past
            performance of a financial instrument is not a reliable indicator of
            current and/or future performance of such financial instrument.
            Please read our Client Agreement and Risk Warning carefully before
            conducting any trades.
          </p>
          <p>
            profit4money is the trading name of Venture Securities Limited and
            is authorized and regulated by the the vanuatu financial services
            commission (Company Reg no : 15001).
          </p>
          <p>
            Risk Warning : Trading profit and CFDs involves significant risk and
            can result in the loss of your invested capital. You should not
            invest more than you can afford to lose and should ensure that you
            fully understand the risks involved. Trading leveraged products may
            not be suitable for all investors. Before trading, please take into
            consideration your level of experience, investment objectives and
            seek independent financial advice if necessary. It is the
            responsibility of the Client to ascertain whether he/she is
            permitted to use the services of the profit4money brand based on the
            legal requirements in his/her country of residence
          </p>
          <p>
            Treasury and back office services are provided by profit
            Interanational gain ltd.
          </p>
          <p>© 2012-2019 Profit4Money. All rights reserved</p>
          <p>
            profit4money is a trademark owned by an entity in the profit4money
            Group. All other trademarks that appear on this website are the
            property of their respective owners.
          </p>
          <p>
            The profit4money Group owns, operates and/or provides services to
            the following brokers and brands:
            <br />
            - profit Internatinal Gain ltd <br />- profit Inernational gain
            ltd., an investment firm licensed and supervised by the vanuatu
            financial services commission. Register Number 15001
          </p>
          <p>
            profit4money is a trademark owned by an entity in the profit4money
            Group. All other trademarks that appear on this website are the
            property of their respective owners.
          </p>
          <div style={{ marginBottom: "100px" }}></div>
          <br />
        </div>
      </Grid>
      <Grid item xs={12} sm={12} md={12} lg={12} className={classes.bottomPara}>
        <div>
          <br />
          <p>
            © 2012-2020{" "}
            <b style={{ fontWeight: "200", color: "#C34245" }}>profit4money</b>.
            All rights reserved
          </p>
        </div>
      </Grid>
    </div>
  );
}
