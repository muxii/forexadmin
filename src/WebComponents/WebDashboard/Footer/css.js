import { makeStyles, fade } from "@material-ui/core/styles";

export const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1
  },
  footerContainer: {
    marginBottom: "-20px",
    backgroundColor: "#164F58",
    color: "#fff"
  },
  footerPara: {
    fontSize: "15px"
  },
  // iconStyle: {
  //   backgroundColor: "#fff",
  //   color: "#164F58",
  //   padding: "10px",
  //   height: "10px",
  //   width: "10px",
  //   border: "1px solid #71777D",
  //   borderRadius: "50%",
  //   fontSize: "15px"
  // },
  fabIcon: {
    border: "1px solid white",
    width: "20px",
    height: "20px",
    borderRadius: "50%"
  },
  header: {
    color: "#fff",
    fontSize: "20px"
  },
  icon: {
    margin: "5px"
  },
  ParaFooter: {
    textAlign: "left",
    backgroundColor: "#0C3035",
    color: "#fff"
  },
  bottomPara: {
    height: "80px",
    textAlign: "center",
    fontSize: "13px",
    backgroundColor: "#061A1D",
    color: "#fff",
 
  },
  footerPadding1: {
    marginTop: "80px",
    marginBottom: "120px",
    marginLeft: "10%",
    marginRight: "10%",
    [theme.breakpoints.down("sm")]: {
      marginTop: "40px",
      marginBottom: "60px",
      marginLeft: "5%",
      marginRight: "5%"
    }
  },
  footerPadding2: {
    marginLeft: "10%",
    marginRight: "10%",
    [theme.breakpoints.down("sm")]: {
      marginLeft: "5%",
      marginRight: "5%"
    }
  }
}));
