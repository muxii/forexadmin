import { makeStyles, fade } from "@material-ui/core/styles";

export const useStyles = makeStyles(theme => ({
  root: {
    maxWidth: "100%",
    flexGrow: 1
  },
  header: {
    display: "flex",
    alignItems: "center",
    height: 50,
    paddingLeft: theme.spacing(4),
    backgroundColor: theme.palette.background.default
  },
  img: {
    height: "500px",
    display: "block",
    maxWidth: "100%",
    overflow: "hidden",
    width: "100%",
    [theme.breakpoints.down("sm")]: {
      width: "100%",
      height: "300px"
    }
  }
}));
