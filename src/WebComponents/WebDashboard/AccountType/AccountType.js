import React from "react";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import { Link } from "react-router-dom";
import { useStyles } from "./css";

export default function FullWidthGrid() {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <p className={classes.subTitle}>Choose from following</p>
      <h1 className={classes.title}>Account Types</h1>
      <div className={classes.bottomBorder}></div>
      <Grid container spacing={2} className={classes.topDiv}>
        <Grid item lg={4}>
          <Card className={classes.cardContainer}>
            <CardContent>
              <div className={classes.imageCircle}>
                <img
                  className={classes.image}
                  src={require("../../../assets/images/accountTypeMini.png")}
                />
              </div>
              <h1 className={classes.packName}>MINI</h1>
              <p className={classes.packPara}>
                These accounts are perfect when the traders are seeking to
                minimize the risk involved.
              </p>
              <div className={classes.borderDiv}></div>
              <p className={classes.packDescription}>4 PIP* TIGHT SPREAD</p>
              <div className={classes.borderDiv}></div>
              <p className={classes.packDescription}>TRAINING - PROVIDED</p>
              <div className={classes.borderDiv}></div>
              <h1 className={classes.packPrize}>$100 - $1,999 MAX</h1>
              <Button className={classes.signUpButton}>
                SIGN UP &nbsp;
                <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
              </Button>
            </CardContent>
          </Card>
        </Grid>
        <Grid item lg={4}>
          <Card className={classes.cardContainer1}>
            <CardContent>
              <div className={classes.imageCircle}>
                <img
                  className={classes.image}
                  src={require("../../../assets/images/accountTypeStandard.png")}
                />
              </div>
              <h1 className={classes.packName}>STANDARD</h1>
              <p className={classes.packPara}>
                These accounts tend to provide with greatest variations
              </p>
              <div className={classes.borderDiv}></div>
              <p className={classes.packDescription}>3 PIP* TIGHT SPREAD</p>
              <div className={classes.borderDiv}></div>
              <p className={classes.packDescription}>TRAINING - PROVIDED</p>
              <div className={classes.borderDiv}></div>
              <h1 className={classes.packPrize}>$2,000 – $4,999 MAX</h1>
              <Button className={classes.signUpButton1}>
                SIGN UP &nbsp;
                <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
              </Button>
            </CardContent>
          </Card>
        </Grid>
        <Grid item lg={4}>
          <Card className={classes.cardContainer}>
            <CardContent>
              <div className={classes.imageCircle}>
                <img
                  className={classes.image}
                  src={require("../../../assets/images/accountTypePremium.png")}
                />
              </div>
              <h1 className={classes.packName}>PREMIUM</h1>
              <p className={classes.packPara}>
                The premium account type is the best for high rollers and
                attracts a lot of bonuses and chances of more profit earnings
                along with greater risk-bearing.
              </p>
              <div className={classes.borderDiv}></div>
              <p className={classes.packDescription}>2 PIP* TIGHT SPREAD</p>
              <div className={classes.borderDiv}></div>
              <p className={classes.packDescription}>TRAINING - PROVIDED</p>
              <div className={classes.borderDiv}></div>
              <h1 className={classes.packPrize}>$5,000 AND ABOVE</h1>
              <Button className={classes.signUpButton}>
                SIGN UP &nbsp;
                <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
              </Button>
            </CardContent>
          </Card>
        </Grid>
      </Grid>
      <div className={classes.bottomLine}></div>
    </div>
  );
}
