import React from "react";
import Grid from "@material-ui/core/Grid";
import { Link } from "react-router-dom";
import { useStyles } from "./css";

export default function FullWidthGrid() {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <div>
            <h1 className={classes.title}>About Us</h1>
            <div className={classes.bottomBorder}></div>
            <p className={classes.para}>
              At profit4money, we continuously strive to provide our customers
              with the services, tools, support and technology, required to take
              advantage of the profit market and the opportunities it offers. We
              are constantly developing new advanced ways to make our client’s
              trading experience even better. This spirit of innovation coupled
              with our superior customer service which is our most important
              core value.
            </p>
            <p className={classes.para}>
              The mission of profit4money is to deliver a secure world class
              trading experience to all clients – both retail and institutional.
              We continually invest in new technologies and people, to provide
              our clients with a diverse range of trading products and an
              exceptional level of customer service.
            </p>
            <Link className={classes.link} to="/">
              <p className={classes.moreButton}>
                Read more &nbsp;
                <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
              </p>
            </Link>
          </div>
        </Grid>
      </Grid>
      <div className={classes.bottomLine}></div>
    </div>
  );
}
