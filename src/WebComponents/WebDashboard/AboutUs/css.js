import { makeStyles, fade } from "@material-ui/core/styles";

export const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    marginLeft: "8%",
    marginRight: "8%"
  },
  title: {
    marginTop: "120px",
    color: "#25707D",
    fontSize: "40px",
    [theme.breakpoints.down("sm")]: {
      marginTop: "40px",
      fontSize: "30px"
    }
  },
  bottomBorder: {
    marginTop: "-10px",
    width: "60px",
    borderBottom: "3px solid #C34245"
  },
  para: {
    marginTop: "40px",
    color: "#333333",
    lineHeight: "30px",
    marginBottom: "27px",
    textAlign: "justify"
  },
  moreButton: {
    textAlign: "left",
    marginBottom: "50px",
    textTransform: "none",
    backgroundColor: "#fff",
    color: "#25707D",
    fontSize: "15px",
    fontWeight: "900",
    cursor: "pointer",
    "&:hover": {
      backgroundColor: "#fff",
      color: "#25707D",
      width: "100px",
      borderBottom: "1px solid #25707D"
    },
    [theme.breakpoints.down("sm")]: {
      marginBottom: "0px"
    }
  },
  link: {
    textDecoration: "none"
  },
  bottomLine: {
    marginTop: "50px",
    borderBottom: "1px solid #25707D",
    [theme.breakpoints.down("sm")]: {
      marginTop: "50px"
    }
  }
}));
