import { makeStyles, fade } from "@material-ui/core/styles";
export const drawerWidth = 240;

export const useStyles = makeStyles(theme => ({
  grow: {
    flexGrow: 1
  },
  menuButton: {
    marginRight: theme.spacing(2)
  },
  appbarContainer: {
    backgroundColor: "#fff",
    color: "black"
  },
  title: {
    fontSize: "25px",
    marginTop: "-15px"
  },
  logo: {
    marginTop: "25px",
    padding: "10px"
  },
  desktopView: {
    display: "block",
    [theme.breakpoints.down("sm")]: {
      display: "none"
    }
  },
  link: {
    textDecoration: "none",
    color: "black"
  }
}));
