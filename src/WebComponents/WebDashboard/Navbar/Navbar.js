import React from "react";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import MenuItem from "@material-ui/core/MenuItem";
import Menu from "@material-ui/core/Menu";
import KeyboardArrowDownIcon from "@material-ui/icons/KeyboardArrowDown";
import { Link } from "react-router-dom";
import { useStyles } from "./css";
import "./Dropdown.css";

export default function PrimarySearchAppBar() {
  const classes = useStyles();
  const [anchorEl, setAnchorEl] = React.useState(null);

  return (
    <div className={classes.desktopView}>
      <div className={classes.grow}>
        <AppBar position="static" className={classes.appbarContainer}>
          <Toolbar>
            <Typography
              style={{ marginLeft: "5%" }}
              edge="start"
              variant="h6"
              noWrap
            >
              <h2>
                <b style={{ color: "#C34245" }}>PROFIT</b>
                <b style={{ color: "#25707D" }}>4MONEY</b>
              </h2>
            </Typography>

            <div className={classes.grow} />
            <div style={{ display: "flex", marginRight: "5%" }}>
              <div class="dropdown">
                <Link to="/">
                  <button class="dropbtn">Home</button>
                </Link>
              </div>
              <div class="dropdown">
                <button class="dropbtn">Forex</button>
                <div class="dropdown-content">
                  <Link to="/forex">
                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                    &nbsp; What is Forex?
                  </Link>
                  <Link to="/commodities">
                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                    &nbsp; Commodities
                  </Link>
                  <Link to="/indices">
                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                    &nbsp; Indices
                  </Link>
                  <Link to="/stocks">
                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                    &nbsp; Stocks
                  </Link>
                  <Link to="/conditions">
                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                    &nbsp; Forex4Money Conditions
                  </Link>
                  <Link to="/faqs">
                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                    &nbsp; Trading FAQs
                  </Link>
                  <Link to="/accountType">
                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                    &nbsp; Account Types
                  </Link>
                  <Link to="/registrationProcess">
                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                    &nbsp; Registration Process
                  </Link>
                </div>
              </div>
              &nbsp;
              <div class="dropdown">
                <button class="dropbtn">About Us</button>
                <div class="dropdown-content">
                  <Link to="/vision">
                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                    &nbsp; Our Vision
                  </Link>
                  <Link to="/tradingBenefits">
                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                    &nbsp; Trading Benefits
                  </Link>
                  <Link to="/tradingSupport">
                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                    &nbsp; Trading Support
                  </Link>
                  <Link to="/helpCenter">
                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                    &nbsp; Forex4Money Help Center
                  </Link>
                </div>
              </div>
              &nbsp;
              <div class="dropdown">
                <button class="dropbtn">Trading</button>
                <div class="dropdown-content">
                  <Link to="/liveAccount">
                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                    &nbsp; Open Live Account
                  </Link>
                  <Link to="/accountComparision">
                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                    &nbsp; Account Comparisions
                  </Link>
                  <Link to="/accountVerification">
                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                    &nbsp; Account Verification
                  </Link>
                  <Link to="/tradingParameter">
                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                    &nbsp; Trading Parameters
                  </Link>
                  <Link to="/tradingSpreads">
                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                    &nbsp; Trading Spreads
                  </Link>
                  <Link to="/stopLoss">
                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                    &nbsp; Stop Loss Policy
                  </Link>
                  <Link to="/premium">
                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                    &nbsp; Premium
                  </Link>
                  <Link to="/withdrawal">
                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                    &nbsp; Withdrawal Process
                  </Link>
                  <Link to="/promotion">
                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                    &nbsp; Promotions
                  </Link>
                </div>
              </div>
              <div class="dropdown">
                <button class="dropbtn">Tools</button>
                <div class="dropdown-content">
                  <Link to="/dailyMarket">
                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                    &nbsp; Daily Market Review
                  </Link>
                  <Link to="/financialCalendar">
                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                    &nbsp; Financial Calendar
                  </Link>
                  <Link to="/realTimeCharts">
                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                    &nbsp; Real Time Charts
                  </Link>
                  <Link to="/signals">
                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                    &nbsp; Signals
                  </Link>
                  <Link to="/platforms">
                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                    &nbsp; Platforms
                  </Link>
                  <Link to="/liveMarketRates">
                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                    &nbsp; Live Market Rate
                  </Link>
                  <Link to="/educationPackage">
                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                    &nbsp; Education Package
                  </Link>
                </div>
              </div>
              <div class="dropdown">
                <button class="dropbtn">Legal</button>
                <div class="dropdown-content">
                  <Link to="/tradingPolicy">
                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                    &nbsp; Our Trading Policy
                  </Link>
                  <Link to="/disclaimer">
                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                    &nbsp; Disclaimer
                  </Link>
                  <Link to="/tradingTerms">
                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                    &nbsp; Trading Terms
                  </Link>
                  <Link to="/riskCaution">
                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                    &nbsp; RisK Caution
                  </Link>
                  {/* <Link to="#">
                    <i class="fa fa-angle-right" aria-hidden="true"></i>
                    &nbsp; Client Agreement
                  </Link> */}
                </div>
              </div>
              <div class="dropdown">
                <Link to="/contact">
                  <button class="dropbtn">Contact</button>
                </Link>
              </div>
            </div>
          </Toolbar>
        </AppBar>
      </div>
    </div>
  );
}
