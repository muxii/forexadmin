import React from "react";
import clsx from "clsx";
import { makeStyles, useTheme } from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import CssBaseline from "@material-ui/core/CssBaseline";
import Grid from "@material-ui/core/Grid";
import AppBar from "@material-ui/core/AppBar";
import Button from "@material-ui/core/Button";
import Toolbar from "@material-ui/core/Toolbar";
import List from "@material-ui/core/List";
import Divider from "@material-ui/core/Divider";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import { Link } from "react-router-dom";
import { useStyles } from "./css";
import "./Dropdown.css";

export default function PersistentDrawerLeft() {
  const classes = useStyles();
  const theme = useTheme();
  const [open, setOpen] = React.useState(false);

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  return (
    <div className={classes.mobileView}>
      <div className={classes.root}>
        <AppBar
          style={{ boxShadow: "2px 2px 2px 2px #f1f1f1" }}
          position="fixed"
          className={clsx(classes.appBar, {
            [classes.appBarShift]: open
          })}
        >
          {/* <AppBar position="static" className={classes.topNavbar}>
            <Toolbar>
              <p style={{ fontSize: "12px" }}>
                <i class="fa fa-envelope"></i>
                &nbsp; info@profit4money.com
              </p>
              <Button className={classes.loginButton}>Register</Button>
              <Button className={classes.loginButton}>Login</Button>
            </Toolbar>
          </AppBar> */}
          <Toolbar>
            <IconButton
              color="inherit"
              aria-label="open drawer"
              onClick={handleDrawerOpen}
              edge="start"
              className={clsx(classes.menuButton, open && classes.hide)}
            >
              <MenuIcon />
            </IconButton>
            <Grid container justify="flex-end">
              <Link to="/" style={{ textDecoration: "none" }}>
                <h2>
                  <b style={{ color: "#C34245" }}>PROFIT</b>
                  <b style={{ color: "#25707D" }}>4MONEY</b>
                </h2>
              </Link>
            </Grid>
          </Toolbar>
        </AppBar>
        <Drawer
          className={classes.drawer}
          variant="temporary"
          onEscapeKeyDown={handleDrawerClose}
          onBackdropClick={handleDrawerClose}
          anchor="left"
          open={open}
          classes={{
            paper: classes.drawerPaper
          }}
        >
          <div className={classes.drawerHeader}>
            <IconButton onClick={handleDrawerClose}>
              {theme.direction === "ltr" ? (
                <ChevronLeftIcon style={{ color: "black" }} />
              ) : (
                <ChevronRightIcon style={{ color: "black" }} />
              )}
            </IconButton>
          </div>
          <Divider />
          {/* <List>
            <ListItem
              button
              className={classes.listBtn}
              onClick={handleDrawerClose}
            >
              <ListItemText className={classes.listBtn} primary="Login" />
            </ListItem>
          </List> */}
          <div>
            <div class="dropdown">
              <Link to="/">
                <button class="dropbtn">Home</button>
              </Link>
            </div>
            <br />
            <div class="dropdown">
              <button class="dropbtn">
                Forex &nbsp;{" "}
                <i
                  align="right"
                  class="fa fa-angle-down"
                  aria-hidden="true"
                ></i>
              </button>
              <div class="dropdown-content">
                <Link to="/forex">
                  <i class="fa fa-angle-right" aria-hidden="true"></i>
                  &nbsp; What is Forex?
                </Link>
                <Link to="/commodities">
                  <i class="fa fa-angle-right" aria-hidden="true"></i>
                  &nbsp; Commodities
                </Link>
                <Link to="/indices">
                  <i class="fa fa-angle-right" aria-hidden="true"></i>
                  &nbsp; Indices
                </Link>
                <Link to="/stocks">
                  <i class="fa fa-angle-right" aria-hidden="true"></i>
                  &nbsp; Stocks
                </Link>
                <Link to="/conditions">
                  <i class="fa fa-angle-right" aria-hidden="true"></i>
                  &nbsp; Forex4Money Conditions
                </Link>
                <Link to="/faqs">
                  <i class="fa fa-angle-right" aria-hidden="true"></i>
                  &nbsp; Trading FAQs
                </Link>
                <Link to="/accountType">
                  <i class="fa fa-angle-right" aria-hidden="true"></i>
                  &nbsp; Account Types
                </Link>
                <Link to="/registrationProcess">
                  <i class="fa fa-angle-right" aria-hidden="true"></i>
                  &nbsp; Registration Process
                </Link>
              </div>
            </div>
            <br />
            <div class="dropdown">
              <button class="dropbtn">
                About Us &nbsp;{" "}
                <i
                  align="right"
                  class="fa fa-angle-down"
                  aria-hidden="true"
                ></i>
              </button>
              <div class="dropdown-content">
                <Link to="/vision">
                  <i class="fa fa-angle-right" aria-hidden="true"></i>
                  &nbsp; Our Vision
                </Link>
                <Link to="/tradingBenefits">
                  <i class="fa fa-angle-right" aria-hidden="true"></i>
                  &nbsp; Trading Benefits
                </Link>
                <Link to="/tradingSupport">
                  <i class="fa fa-angle-right" aria-hidden="true"></i>
                  &nbsp; Trading Support
                </Link>
                <Link to="/helpCenter">
                  <i class="fa fa-angle-right" aria-hidden="true"></i>
                  &nbsp; Forex4Money Help Center
                </Link>
              </div>
            </div>
            <br />
            <div class="dropdown">
              <button class="dropbtn">
                Trading &nbsp;{" "}
                <i
                  align="right"
                  class="fa fa-angle-down"
                  aria-hidden="true"
                ></i>
              </button>
              <div class="dropdown-content">
                <Link to="/liveAccount">
                  <i class="fa fa-angle-right" aria-hidden="true"></i>
                  &nbsp; Open Live Account
                </Link>
                <Link to="/accountComparision">
                  <i class="fa fa-angle-right" aria-hidden="true"></i>
                  &nbsp; Account Comparisions
                </Link>
                <Link to="/accountVerification">
                  <i class="fa fa-angle-right" aria-hidden="true"></i>
                  &nbsp; Account Verification
                </Link>
                <Link to="/tradingParameter">
                  <i class="fa fa-angle-right" aria-hidden="true"></i>
                  &nbsp; Trading Parameters
                </Link>
                <Link to="/tradingSpreads">
                  <i class="fa fa-angle-right" aria-hidden="true"></i>
                  &nbsp; Trading Spreads
                </Link>
                <Link to="/stopLoss">
                  <i class="fa fa-angle-right" aria-hidden="true"></i>
                  &nbsp; Stop Loss Policy
                </Link>
                <Link to="/premium">
                  <i class="fa fa-angle-right" aria-hidden="true"></i>
                  &nbsp; Premium
                </Link>
                <Link to="/withdrawal">
                  <i class="fa fa-angle-right" aria-hidden="true"></i>
                  &nbsp; Withdrawal Process
                </Link>
                <Link to="/promotion">
                  <i class="fa fa-angle-right" aria-hidden="true"></i>
                  &nbsp; Promotions
                </Link>
              </div>
            </div>
            <br />
            <div class="dropdown">
              <button class="dropbtn">
                Tools &nbsp;{" "}
                <i
                  align="right"
                  class="fa fa-angle-down"
                  aria-hidden="true"
                ></i>
              </button>
              <div class="dropdown-content">
                <Link to="/dailyMarket">
                  <i class="fa fa-angle-right" aria-hidden="true"></i>
                  &nbsp; Daily Market Review
                </Link>
                <Link to="/financialCalendar">
                  <i class="fa fa-angle-right" aria-hidden="true"></i>
                  &nbsp; Financial Calendar
                </Link>
                <Link to="/realTimeCharts">
                  <i class="fa fa-angle-right" aria-hidden="true"></i>
                  &nbsp; Real Time Charts
                </Link>
                <Link to="/signals">
                  <i class="fa fa-angle-right" aria-hidden="true"></i>
                  &nbsp; Signals
                </Link>
                <Link to="/platforms">
                  <i class="fa fa-angle-right" aria-hidden="true"></i>
                  &nbsp; Platforms
                </Link>
                <Link to="/liveMarketRates">
                  <i class="fa fa-angle-right" aria-hidden="true"></i>
                  &nbsp; Live Market Rate
                </Link>
                <Link to="/educationPackage">
                  <i class="fa fa-angle-right" aria-hidden="true"></i>
                  &nbsp; Education Package
                </Link>
              </div>
            </div>
            <br />
            <div class="dropdown">
              <button class="dropbtn">
                Legal &nbsp;{" "}
                <i
                  align="right"
                  class="fa fa-angle-down"
                  aria-hidden="true"
                ></i>
              </button>
              <div class="dropdown-content">
                <Link to="/tradingPolicy">
                  <i class="fa fa-angle-right" aria-hidden="true"></i>
                  &nbsp; Our Trading Policy
                </Link>
                <Link to="/disclaimer">
                  <i class="fa fa-angle-right" aria-hidden="true"></i>
                  &nbsp; Disclaimer
                </Link>
                <Link to="/tradingTerms">
                  <i class="fa fa-angle-right" aria-hidden="true"></i>
                  &nbsp; Trading Terms
                </Link>
                <Link to="/riskCaution">
                  <i class="fa fa-angle-right" aria-hidden="true"></i>
                  &nbsp; RisK Caution
                </Link>
                {/* <Link to="#">
                  <i class="fa fa-angle-right" aria-hidden="true"></i>
                  &nbsp; Client Agreement
                </Link> */}
              </div>
            </div>
            <br />
            <div class="dropdown">
              <Link to="/contact">
                <button class="dropbtn">Contact</button>
              </Link>
            </div>
            <br />
            <Link
              style={{ textDecoration: "none" }}
              to="/forex4money/admin/register"
            >
              <Button className={classes.loginButton}>Register</Button>
            </Link>
            <Link
              style={{ textDecoration: "none" }}
              to="/forex4money/admin/login"
            >
              <Button className={classes.loginButton}>Login</Button>
            </Link>
          </div>
        </Drawer>
        <main
          className={clsx(classes.content, {
            [classes.contentShift]: open
          })}
        >
          {/* <div className={classes.drawerHeader} /> */}
        </main>
      </div>
    </div>
  );
}
