import { makeStyles, fade } from "@material-ui/core/styles";

export const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1
  },
  mainContainer: {
    padding: "24px",
    backgroundColor: "#25707D",
    color: "#fff"
  },
  flexContainer: {
    marginLeft: "8%",
    marginRight: "8%",
    display: "flex",
    justifyContent: "space-between",
    [theme.breakpoints.down("sm")]: {
      marginLeft: "0%",
      marginRight: "0%"
    }
  },
  title: {
    color: "#fff",
    fontSize: "25px",
    [theme.breakpoints.down("sm")]: {
      fontSize: "15px"
    }
  },
  contactButton: {
    width: "170px",
    marginTop: "10px",
    textTransform: "none",
    backgroundColor: "#c34245",
    height: "55px",
    fontSize: "16px",
    fontWeight: "bold",
    color: "#fff",
    textAlign: "left",
    borderRadius: "2px",
    position: "relative",
    cursor: "pointer",
    "&:hover": {
      backgroundColor: "#c34245",
      color: "#fff"
    },
    [theme.breakpoints.down("sm")]: {
      textAlign: "center",
      width: "100%",
      height: "40px"
    }
  },
  link: {
    textDecoration: "none"
  }
}));
