import React from "react";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import { Link } from "react-router-dom";
import { useStyles } from "./css";

export default function FullWidthGrid() {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <Grid container spacing={0}>
        <Grid item xs={12}>
          <div className={classes.mainContainer}>
            <div className={classes.flexContainer}>
              <h2 className={classes.title}>
                Need a help? Get a first-class finance consultant.
              </h2>
              <div>
                <Button className={classes.contactButton}>
                  <b>Contact</b> &nbsp;
                  <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                </Button>
              </div>
            </div>
          </div>
        </Grid>
      </Grid>
    </div>
  );
}
