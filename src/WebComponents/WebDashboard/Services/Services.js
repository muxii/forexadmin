import React from "react";
import Grid from "@material-ui/core/Grid";
import { Link } from "react-router-dom";
import { useStyles } from "./css";

export default function FullWidthGrid() {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <Grid container spacing={2} className={classes.topDiv}>
        <Grid item lg={4}>
          <div className={classes.thumbnail}>
            <div className={classes.overlay}>
              <p className={classes.serviceSlogan}>
                “25 years of experiences in Financial Fields”
              </p>
            </div>
          </div>
        </Grid>
        <Grid item lg={8}>
          <div className={classes.rightContainer}>
            <p className={classes.subTitle}>We offer different services</p>
            <h1 className={classes.title}>Our services</h1>
            <div className={classes.bottomBorder}></div>
            <Grid container spacing={2}>
              <Grid item xs={12} sm={12} md={6} lg={6}>
                <div className={classes.cardContainer}>
                  <h4 className={classes.cardHeading}>Commodities</h4>
                  <p className={classes.cardPara}>
                    It has become a popular form of investment amongst
                    individual investors as they allow the use of leverage.
                  </p>
                </div>
              </Grid>
              <Grid item xs={12} sm={12} md={6} lg={6}>
                <div className={classes.cardContainer}>
                  <h4 className={classes.cardHeading}>Indices</h4>
                  <p className={classes.cardPara}>
                    Trading major market indices has been a popular way of
                    investing for nearly a century.
                  </p>
                </div>
              </Grid>
              <Grid
                item
                xs={12}
                sm={12}
                md={6}
                lg={6}
                style={{ marginTop: "-20px" }}
              >
                <div className={classes.cardContainer}>
                  <h4 className={classes.cardHeading}>Stocks</h4>
                  <p className={classes.cardPara}>
                    The importance of being a shareholder is that you are
                    entitled to a portion of the company's profits.
                  </p>
                </div>
              </Grid>
            </Grid>
          </div>
        </Grid>
      </Grid>
      <div className={classes.bottomLine}></div>
    </div>
  );
}
