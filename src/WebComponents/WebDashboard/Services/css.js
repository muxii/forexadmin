import { makeStyles, fade } from "@material-ui/core/styles";
import Image from "../../../assets/images/servicesImg.png";

export const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    marginLeft: "8%",
    marginRight: "8%"
  },
  topDiv: {
    marginTop: "100px",
    [theme.breakpoints.down("sm")]: {
      marginTop: "50px"
    }
  },
  subTitle: {
    color: "#333333"
  },
  title: {
    marginTop: "-10px",
    color: "#25707D",
    fontSize: "40px",
    [theme.breakpoints.down("sm")]: {
      fontSize: "30px"
    }
  },
  bottomBorder: {
    marginTop: "-10px",
    width: "60px",
    borderBottom: "3px solid #C34245"
  },
  thumbnail: {
    backgroundImage: `url(${Image})`,
    backgroundPosition: "center",
    backgroundRepeat: "no-repeat",
    backgroundSize: "cover",
    [theme.breakpoints.down("sm")]: {
      marginBottom: "50px",
      width: "100%",
      height: "300px"
    }
  },
  overlay: {
    border: "8px solid #c34245",
    backgroundColor: "rgba(0,46,91,0.8)",
    display: "block",
    width: "100%",
    height: "500px",
    [theme.breakpoints.down("sm")]: {
      width: "100%",
      height: "300px"
    }
  },
  serviceSlogan: {
    padding: "25px",
    fontSize: "20px",
    fontWeight: "bold",
    color: " #fff",
    fontStyle: "italic",
    position: "relative",
    top: "50%",
    transform: "translateY(-50%)",
    zIndex: "10"
  },
  rightContainer: {
    marginLeft: "50px",
    [theme.breakpoints.down("sm")]: {
      marginLeft: "0px"
    }
  },
  cardContainer: {
    marginTop: "40px",
    border: "1px solid #ededed",
    padding: "30px",
    position: "relative",
    height: "100px",
    "&:hover": {
      border: "1px solid #25707D"
    }
  },
  cardHeading: {
    marginTop: "-5px",
    color: "#25707D",
    fontSize: "18px"
  },
  cardPara: {
    fontSize: "14px",
    color: "#666666",
    lineHeight: "20px"
  },
  link: {
    textDecoration: "none"
  },
  bottomLine: {
    marginTop: "100px",
    borderBottom: "1px solid #25707D",
    [theme.breakpoints.down("sm")]: {
      marginTop: "50px"
    }
  }
}));
