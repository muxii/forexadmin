import React from "react";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import { Link } from "react-router-dom";
import { useStyles } from "./css";

export default function PrimarySearchAppBar() {
  const classes = useStyles();

  return (
    <div className={classes.desktopView}>
      <div className={classes.grow}>
        <AppBar position="static" className={classes.appbarContainer}>
          <Toolbar>
            <Typography
              edge="start"
              className={classes.title}
              variant="h9"
              noWrap
            >
              <i class="fa fa-envelope-o" aria-hidden="true"></i>
              &nbsp; info@profit4money.com
            </Typography>

            <div className={classes.grow} />
            <div style={{ display: "flex", marginRight: "5%" }}>
              <Link className={classes.link} to="/forex4money/admin/register">
                <Button className={classes.RegisterButton}>Register</Button>
              </Link>
              <Link className={classes.link} to="/forex4money/admin/login">
                <Button className={classes.LoginButton}>Login</Button>
              </Link>
            </div>
          </Toolbar>
        </AppBar>
      </div>
    </div>
  );
}
