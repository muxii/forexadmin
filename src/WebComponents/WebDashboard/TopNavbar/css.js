import { makeStyles, fade } from "@material-ui/core/styles";
export const drawerWidth = 240;

export const useStyles = makeStyles(theme => ({
  grow: {
    flexGrow: 1
  },
  appbarContainer: {
    backgroundColor: "#25707D",
    color: "white",
    height: "50px"
  },
  title: {
    marginTop: "-15px",
    marginLeft: "5%",
    fontSize: "15px"
  },
  RegisterButton: {
    marginTop: "-15px",
    width: "120px",
    height: "30px",
    textTransform: "none",
    backgroundColor: "#C34245",
    color: "white",
    marginRight: "10px",
    fontSize: "13px",
    "&:hover": {
      backgroundColor: "#C34245",
      color: "white"
    }
  },
  LoginButton: {
    marginTop: "-15px",
    width: "120px",
    height: "30px",
    textTransform: "none",
    backgroundColor: "#C34245",
    color: "white",
    marginRight: "10px",
    fontSize: "13px",
    "&:hover": {
      backgroundColor: "#C34245",
      color: "white"
    }
  },
  link: {
    color: "#fff",
    textDecoration: "none"
  },
  desktopView: {
    display: "block",
    [theme.breakpoints.down("sm")]: {
      display: "none"
    }
  }
}));
