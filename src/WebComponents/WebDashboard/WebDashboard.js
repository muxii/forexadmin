import React from "react";
import Grid from "@material-ui/core/Grid";
import MobNavbar from "./MobNavbar/MobNavbar";
import TopNavbar from "./TopNavbar/TopNavbar";
import Navbar from "./Navbar/Navbar";
import Carousel from "./Carousel/Carousel";
import AboutUs from "./AboutUs/AboutUs";
import Services from "./Services/Services";
import AccountType from "./AccountType/AccountType";
import Payment from "./Payment/Payment";
import Contact from "./Contact/Contact";
import Footer from "./Footer/Footer";

export default function FullWidthGrid() {
  return (
    <div style={{ flexGrow: "1" }}>
      <Grid container spacing={0}>
        <Grid item xs={12} sm={12} md={12} lg={12}>
          <MobNavbar />
          <TopNavbar />
          <Navbar />
          <Carousel />
          <AboutUs />
          <Services />
          <AccountType />
          <Payment />
          <Contact />
          <Footer />
        </Grid>
      </Grid>
    </div>
  );
}
