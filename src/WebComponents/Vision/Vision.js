import React from "react";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Breadcrumbs from "@material-ui/core/Breadcrumbs";
import MobNavbar from "../WebDashboard/MobNavbar/MobNavbar";
import TopNavbar from "../WebDashboard/TopNavbar/TopNavbar";
import Navbar from "../WebDashboard/Navbar/Navbar";
import Contact from "../WebDashboard/Contact/Contact";
import Footer from "../WebDashboard/Footer/Footer";
import ContactForm from "../ContactForm/ContactForm";
import { useStyles } from "./css";
import { Link } from "react-router-dom";

export default function FullWidthGrid() {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <MobNavbar />
      <TopNavbar />
      <Navbar />
      <Grid container spacing={0}>
        <Grid container className={classes.paddingDiv}>
          <Grid item xs={12} sm={12} md={7} lg={7}>
            <div>
              <Breadcrumbs aria-label="breadcrumb" className={classes.titleTop}>
                <Link to="/" className={classes.link}>
                  <b className={classes.breadHome}>Home</b>
                </Link>
                <Typography color="textPrimary">
                  <b className={classes.breadText}>Tools</b>
                </Typography>
                <Typography color="textPrimary">
                  <b className={classes.breadText}>Our Vision</b>
                </Typography>
              </Breadcrumbs>
              <h1 className={classes.title}>Our Vision</h1>
              <div className={classes.bottomBorder}></div>
              <p className={classes.para}>
                With every hour that is passing by, there are increasing
                opportunities of profit earning in the prevailing financial
                markets. At profit4money, we offer the most relevant{" "}
                <b>customer support</b> for trading in the profit market. When
                clients arrive to us for reaching out to reliable earning
                opportunities we make sure to keep ourselves updated with the
                most relevant tools and guidance for them.
              </p>
              <p className={classes.para}>
                We come up with the most relevant technologies that could help
                clients make the best out of available opportunities to earn
                considerable profits. We’re prepared with various quantitative
                spreads that could support quality trade execution.
              </p>
            </div>
          </Grid>
          <Grid item item xs={12} sm={12} md={5} lg={5}>
            <ContactForm />
          </Grid>
          <Grid item item xs={12} sm={12} md={12} lg={12}>
            <div>
              <h2 className={classes.subTitle}>Secure Trading Options</h2>
              <div className={classes.bottomBorder}></div>
              <p className={classes.para}>
                Although there are endless opportunities of earning with{" "}
                <b>trade finance</b>, it is quite hard to find trading platforms
                and technologies that are secure. Every trader wants to proceed
                with considerably lower risk fetch out maximum profits. We
                therefore determine stronger core values that can encourage and
                empower people from different backgrounds to trade with
                confidence.
              </p>
              <p className={classes.para}>
                We continuously bring in our efforts to unleash the actual
                potential of desiring traders and enter the world of trading
                with a sense of reliability and security for the money they are
                going to invest.
              </p>
              <p className={classes.para}>
                More than anything, our major concern is account security along
                with exceptional <b>customer support</b> service. For this
                purpose we come up with advanced tools and the most suitable
                encryption protocols. This helps in attaining suitable market
                liquidity and ensures investment capital for the traders.
              </p>
              <p className={classes.para}>
                profit4money handles situations and cases under great market
                volumes with strict policies and measures for accounts security.
                They continually come up with better ways of connecting traders
                to the fast paced global markets.
              </p>
              <h4>Dependable Customer Support System</h4>
              <p className={classes.para}>
                We are available to our clients through various mediums so that
                we are reachable at the very moment they need us. Our
                representatives are perfect at handling different cases in ways
                that could keep everything well in place. We proceed with our
                activities keeping in mind the vision of creating and extending
                a secure and safe trading environment to our clients and help
                them explore the lucrative proximities of trade finance.
              </p>
              <p className={classes.para}>
                With proper licensing and regulations we tend to make out the
                ultimate best returns for <b>profit currency</b>. Continuous
                research and reliable decision making is preferable to ensure
                that traders operate the market suitably with considerable
                returns. We constantly endeavor towards creating an environment
                that is perfect for realizing the most appropriate trade
                practices. While many may be unaware of the actual operations of
                profit trading, with profit4money we tend to provide them a
                secure platform which is reliable for both investing and
                learning.
              </p>
            </div>
          </Grid>
        </Grid>
      </Grid>
      <Contact />
      <Footer />
    </div>
  );
}
