import React from "react";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Breadcrumbs from "@material-ui/core/Breadcrumbs";
import MobNavbar from "../WebDashboard/MobNavbar/MobNavbar";
import TopNavbar from "../WebDashboard/TopNavbar/TopNavbar";
import Navbar from "../WebDashboard/Navbar/Navbar";
import Contact from "../WebDashboard/Contact/Contact";
import Footer from "../WebDashboard/Footer/Footer";
import ContactForm from "../ContactForm/ContactForm";
import { useStyles } from "./css";
import { Link } from "react-router-dom";

export default function FullWidthGrid() {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <MobNavbar />
      <TopNavbar />
      <Navbar />
      <Grid container spacing={0}>
        <Grid container className={classes.paddingDiv}>
          <Grid item xs={12} sm={12} md={7} lg={7}>
            <div>
              <Breadcrumbs aria-label="breadcrumb" className={classes.titleTop}>
                <Link to="/" className={classes.link}>
                  <b className={classes.breadHome}>Home</b>
                </Link>
                <Typography color="textPrimary">
                  <b className={classes.breadText}>Tools</b>
                </Typography>
                <Typography color="textPrimary">
                  <b className={classes.breadText}>Daily Market Review</b>
                </Typography>
              </Breadcrumbs>
              <h1 className={classes.title}>Daily Market Review</h1>
              <div className={classes.bottomBorder}></div>
              <p className={classes.para}>
                profit4money is the lot of trusted and well-managed profit
                Trading Services. Since we are adapted brokers, the aim is to
                advice our barter to acquire best profits at all levels. We
                accomplish it accessible for you to play confidently in the
                cutting-edge allowance FX marketplace.
              </p>
              <h2 className={classes.subTitle}>Have a sound grip</h2>
              <div className={classes.bottomBorder}></div>
              <p className={classes.para}>
                Have a valid grip over foreign exchange market with daily market
                review presented by way of us
              </p>

              <p className={classes.para}>
                Are you absorbed to apperceive the latest able opinions about
                Euro, USD or Pound? Or you wish to see the ups and downs in the
                prices of a accurate currency? Or you are added aflame about the
                reviews and assay of Gold and Silver?
              </p>
              <p className={classes.para}>
                Regardless of your best or breadth of specialization,
                profit4money is the one-stop band-aid for you. Our detailed,
                articular assay helps you in compassionate the ups and downs of
                the bazaar and an absolute anchor on the trends.
              </p>
              <p className={classes.para}>
                We accompany to-the-point circadian assay so that you attending
                into the data of profit bazaar regularly. The added abreast you
                are, the college are the accumulation margins. Therefore, we
                accompany weighty, abundant and absolute letters on bazaar
                trends, fluctuations, and forecasts.
              </p>
              <p className={classes.para}>
                The letters are in appliance to the latest bazaar affairs and
                bread-and-butter events. We associate all aspects that affect
                the profit markets and their trends. Not alone we accord the
                ‘bigger picture’ to you, but we accomplish you fully-equipped
                with the trading signals so that you are on top of the bazaar
                volatility.
              </p>
              <h2 className={classes.subTitle}>Be knowledgeable</h2>
              <div className={classes.bottomBorder}></div>
              <p className={classes.para}>
                Expertise is strength”, the cliché is going proper as a ways as
                foreign exchange buying and selling is concerned. As a result,
                we bring exact information on currency pairs, indices, and
                commodities so you take an informed choice. Our reports are
                subsidized-up with the sound know-how and enjoy of foreign
                exchange trading stalwarts who bring a rich revel in inside the
                area of interest.
              </p>
              <p className={classes.para}>
                Investors often jump into trading strategiesoptions with little
                understanding of alternatives techniques. There are many
                techniques available that restriction chance and maximize
                return. CoinMarketCapis a belvedere created to clue the assets
                of altered cryptocurrencies, the bulk of trades that use them
                and the accepted amount adapted into authorization currencies.
              </p>
              <p className={classes.para}>
                profit4money gives you gigantic trading protection and
                confidence whether or not you're a novice player or expert! Your
                authorized private foreign exchange dealor or profit Dealoronly
                a click away. Have a look at the current USD and Indian Rupee.
              </p>
            </div>
          </Grid>
          <Grid item item xs={12} sm={12} md={5} lg={5}>
            <ContactForm />
          </Grid>
        </Grid>
      </Grid>
      <Contact />
      <Footer />
    </div>
  );
}
