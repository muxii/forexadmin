import React from "react";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Breadcrumbs from "@material-ui/core/Breadcrumbs";
import MobNavbar from "../WebDashboard/MobNavbar/MobNavbar";
import TopNavbar from "../WebDashboard/TopNavbar/TopNavbar";
import Navbar from "../WebDashboard/Navbar/Navbar";
import Contact from "../WebDashboard/Contact/Contact";
import Footer from "../WebDashboard/Footer/Footer";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import { useStyles } from "./css";
import { Link } from "react-router-dom";

export default function FullWidthGrid() {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <MobNavbar />
      <TopNavbar />
      <Navbar />
      <Grid container spacing={0}>
        <Grid container className={classes.paddingDiv}>
          <Grid item xs={12} sm={12} md={12} lg={12}>
            <div>
              <Breadcrumbs aria-label="breadcrumb" className={classes.titleTop}>
                <Link to="/" className={classes.link}>
                  <b className={classes.breadHome}>Home</b>
                </Link>
                <Typography color="textPrimary">
                  <b className={classes.breadText}>Tools</b>
                </Typography>
                <Typography color="textPrimary">
                  <b className={classes.breadText}>Help Center</b>
                </Typography>
              </Breadcrumbs>
              <h1 className={classes.title}>profit4money HelpCenter</h1>
              <div className={classes.bottomBorder}></div>
              <p className={classes.para}>
                profit trading is not a piece of cake. If you really want to
                become a successful trader, then you must be ready to invest not
                only money but also hard work and time to obtain these three
                success factors- Experience, knowledge, and emotion charge.
              </p>
              <p className={classes.para}>
                profit4money staff help center is here round the clock to assist
                you to reach your goals, offer you <b>technical assistance</b>{" "}
                remarkably from a learning view, but deciding if a profit trading
                regiment is ideal for you will not come easy. It only comes
                after hours of training, practice with real-time and virtual
                cash on a profit demo account.
              </p>
              <h1 className={classes.subTitle}>
                Making a Better Online Trading Experience Is Our Aspiration
              </h1>
              <p className={classes.para}>
                At profit4money, we consider that victorious <b>profit</b>{" "}
                trading needs experience, knowledge as well as a disciplined
                approach towards the market. Every page on our website is
                dedicated to these main factors in some way. whether you think
                you are a newbie, intermediate or a professional trader, we
                assure that there is something exciting and new for you to study
                here. We are one of the most trusted profit brokers on the
                internet today.
              </p>
              <p className={classes.para}>
                Give clients what they want- easy and quick profit solutions to
                their queries. profit4money help desk assists you offer
                personalized support whenever they need, customers satisfaction
                is our priority.
              </p>
              <p className={classes.para}>
                We are committed to steadily growing and expanding our
                offerings; profit4 money team of ex[prts are accessible over
                email, live chat, and phone, so you can contact us anytime. You
                can also find us on social media networks, our help team manage
                and respond to your queries or comments on our social media
                handle.
              </p>
              <h4>Technical Assistance</h4>
              <p className={classes.para}>
                We know Good and 24/7 customer support is very important in
                profit, regardless of how expert you are but specifically if
                you're a newbie. if you will not get support by a broker when
                needed, then you may fail to solve any issue that you may face
                during your trading career.
              </p>
              <p className={classes.para}>
                On the other hand, technical assistance is also very important.
                all-round assistance is needed, but especially{" "}
                <b>support technical</b>, if you're a beginner. We understand
                how solid support is needed for successful trading. Our customer
                support will never let you and your hopes down.
              </p>
              <p className={classes.para}>
                profit4money <b>help center</b> is not only effective but also
                efficient. you will get the instant help on demand, as we know
                poor support could lead our customers to make unnecessary loss
                of money.
              </p>
              <p className={classes.para}>
                Whenever you need our support, just fill your details in the
                form below and send your queries. We will respond to you as soon
                as possible.
              </p>
              <Grid container spacing={0}>
                <Grid item xs={12} sm={12} md={4} lg={4}>
                  <TextField
                    className={classes.inputDiv}
                    fullWidth
                    id="outlined-basic"
                    placeholder="Enter Name"
                    variant="outlined"
                  />
                </Grid>
                <Grid item xs={12} sm={12} md={4} lg={4}>
                  <TextField
                    className={classes.inputDiv}
                    fullWidth
                    id="outlined-basic"
                    placeholder="Enter Email Address"
                    variant="outlined"
                  />
                </Grid>
                <Grid item xs={12} sm={12} md={4} lg={4}>
                  <TextField
                    className={classes.inputDiv}
                    fullWidth
                    id="outlined-basic"
                    placeholder="Enter Mobile Number"
                    variant="outlined"
                  />
                </Grid>
                <Grid item xs={12} sm={12} md={12} lg={12}>
                  <br />
                  <br />
                  <TextField
                    multiline
                    rows={8}
                    fullWidth
                    id="outlined-basic"
                    placeholder="Enter Message"
                    variant="outlined"
                  />
                  <br />
                  <br />
                </Grid>
                <Button className={classes.submitButton}>
                  Submit &nbsp;
                  <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                </Button>
              </Grid>
            </div>
          </Grid>
        </Grid>
      </Grid>
      <Contact />
      <Footer />
    </div>
  );
}
