import React from "react";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Breadcrumbs from "@material-ui/core/Breadcrumbs";
import MobNavbar from "../WebDashboard/MobNavbar/MobNavbar";
import TopNavbar from "../WebDashboard/TopNavbar/TopNavbar";
import Navbar from "../WebDashboard/Navbar/Navbar";
import Contact from "../WebDashboard/Contact/Contact";
import Footer from "../WebDashboard/Footer/Footer";
import ContactForm from "../ContactForm/ContactForm";
import { useStyles } from "./css";
import { Link } from "react-router-dom";

export default function FullWidthGrid() {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <MobNavbar />
      <TopNavbar />
      <Navbar />
      <Grid container spacing={0}>
        <Grid container className={classes.paddingDiv}>
          <Grid item xs={12} sm={12} md={7} lg={7}>
            <div>
              <Breadcrumbs aria-label="breadcrumb" className={classes.titleTop}>
                <Link to="/" className={classes.link}>
                  <b className={classes.breadHome}>Home</b>
                </Link>
                <Typography color="textPrimary">
                  <b className={classes.breadText}>Tools</b>
                </Typography>
                <Typography color="textPrimary">
                  <b className={classes.breadText}>Stop Loss Policy</b>
                </Typography>
              </Breadcrumbs>
              <h1 className={classes.title}>What is Stop Loss policy?</h1>
              <div className={classes.bottomBorder}></div>
              <p className={classes.para}>
                A <b>Stop loss</b> refers to a risk management tool which
                intends to add security to your fund investment. It is necessary
                on every position except for real assets. and cryptocurrencies.
              </p>
              <p className={classes.para}>
                It's an order to end the trade when the profit market moves a
                defined amount upon your position and assist you to reduce your
                losses when the market moves in the odd flow to what you
                anticipated.
              </p>
              <p className={classes.para}>
                You as a trader can set your <b>Stop loss</b> as a monetary
                amount or as a specific level in the market, which is shown as
                an initial investment percentage.
              </p>
              <p className={classes.para}>
                During normal market conditions, when the market exceeds your
                requested rate and you lost the decided amount, then the Stop
                Loss will target and close your position automatically.
              </p>
              <p className={classes.para}>
                Stop Losses are not secured. In Volatile periods market gaps,
                slippage and spikes may occur as of changing market conditions,
                sometimes <b>Stop loss</b> fail to trigger and stop at your
                requested rate.
              </p>
              <p className={classes.para}>
                we do not remunerate for these occurrences as we do not conflict
                with market events or conditions.
              </p>
              <p className={classes.para}>
                With so many important aspects to view at and consider when
                considering a stock buy, its easy to forget about minor things.
                And the Stop-loss is one of those minor things, however, it can
                make a huge difference. Each trader can benefit from this
                trading tool.
              </p>
              <p className={classes.para}>
                profit4money Stop-loss let you focus on other important tasks,
                and in the meantime, it will monitor how your stock is
                performing. this tool is extremely beneficial when you are on a
                vacation that stops you from viewing your stocks position for a
                long period.
              </p>
              <p className={classes.para}>
                Last, but Most importantly, a stop-loss enables decision-making
                process to be freed from any emotional impacts. Sometimes people
                think t give a stock new change, which causes delay and
                procrastination, giving the stock another chance only cause
                losses.
              </p>
              <p className={classes.para}>
                Overall, <b>Stop loss</b> is one of the most important tools
                offered by us. It's very easy to use the tool. Whether you want
                to lock in profits or prevent losses, You can benefit from this
                tool. It's like an insurance policy: You think you never need
                it, but it's great to know you have the security should you
                require it.
              </p>
              <p className={classes.para}>
                Stop-loss orders can assist you to stay on the right track
                without overshadowing your decision with emotion.
              </p>
              <p className={classes.para}>
                It's important to know that Stop-loss policy does not guarantee
                you'll gain profit in the trading.
              </p>
            </div>
          </Grid>
          <Grid item item xs={12} sm={12} md={5} lg={5}>
            <ContactForm />
          </Grid>
        </Grid>
      </Grid>
      <Contact />
      <Footer />
    </div>
  );
}
