import { makeStyles, fade } from "@material-ui/core/styles";

export const useStyles = makeStyles(theme => ({
    root: {
      flexGrow: 1,
      marginLeft: "8%",
      marginRight: "8%"
    },
    title: {
      marginTop: "120px",
      color: "#25707D",
      fontSize: "40px"
    },
    bottomBorder: {
        marginTop: "-10px",
        width: "60px",
        borderBottom: "3px solid #C34245"
      }
}))