import { Grid } from "@material-ui/core";
import React from "react";
import PrimarySearchAppBar from "../WebDashboard/TopNavbar/TopNavbar";
import { useStyles } from "./css";

export const OurVision = () => {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <PrimarySearchAppBar />
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <div>
            <h1 className={classes.title}>About Us</h1>
            <div className={classes.bottomBorder}></div>
          </div>
        </Grid>
      </Grid>
    </div>
  );
};
export default OurVision;
