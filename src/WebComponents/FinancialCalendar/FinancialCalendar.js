import React from "react";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Breadcrumbs from "@material-ui/core/Breadcrumbs";
import MobNavbar from "../WebDashboard/MobNavbar/MobNavbar";
import TopNavbar from "../WebDashboard/TopNavbar/TopNavbar";
import Navbar from "../WebDashboard/Navbar/Navbar";
import Contact from "../WebDashboard/Contact/Contact";
import Footer from "../WebDashboard/Footer/Footer";
import ContactForm from "../ContactForm/ContactForm";
import { useStyles } from "./css";
import { Link } from "react-router-dom";

export default function FullWidthGrid() {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <MobNavbar />
      <TopNavbar />
      <Navbar />
      <Grid container spacing={0}>
        <Grid container className={classes.paddingDiv}>
          <Grid item xs={12} sm={12} md={7} lg={7}>
            <div>
              <Breadcrumbs aria-label="breadcrumb" className={classes.titleTop}>
                <Link to="/" className={classes.link}>
                  <b className={classes.breadHome}>Home</b>
                </Link>
                <Typography color="textPrimary">
                  <b className={classes.breadText}>Tools</b>
                </Typography>
                <Typography color="textPrimary">
                  <b className={classes.breadText}>Financial Calendar</b>
                </Typography>
              </Breadcrumbs>
              <h1 className={classes.title}>profit calendar and its use</h1>
              <div className={classes.bottomBorder}></div>
              <p className={classes.para}>
                <b>profit calendar</b> is a very useful profit tool. It is a
                customized tool that provides detailed information regarding
                profit trading. It will inform traders about upcoming important
                economic information, like <b>commodity live market rate</b>,
                non farm payroll etc. As our regular calendars assist us to
                remind the day and date, Our profit calendar assists the traders
                similarly.
              </p>
              <p className={classes.para}>
                In short, the Foreign calendar is One of the best and most
                useful profit tools that offer traders with relatable
                information, such as changes expected to have a great impact on
                the economic markets to make the right predictions.
              </p>
              <p className={classes.para}>
                This tool helps profit traders in maintaining track of the
                latest
                <b>profit news</b>, and events happening in the world of
                economics.
              </p>
              <p className={classes.para}>
                By using our <b>profit calendar</b>, profit traders have the
                chance to get informative details about economic and
                non-economic signs and stay in touch with current profit trends,
                and what can change the movement of any particular currency.
              </p>
              <p className={classes.para}>
                With this, you can keep an eye on how the economy is moving, it
                will hint to you when the market is down and up. It saves a lot
                you from risky trade, and much more useful for beginner traders.
              </p>
              <p className={classes.para}>
                You can rely on our <b>profit calendar</b> that will offer you
                great benefit in your trading. It works as a powerful tool that
                helps you to know when to trade or not.
              </p>
              <h2 className={classes.subTitle}>
                Tips to use an Economic Calendar
              </h2>
              <div className={classes.bottomBorder}></div>
              <h4>Recognize the right indicators?</h4>
              <p className={classes.para}>
                There are basically two types of indicators.
              </p>
              <p className={classes.para}>
                <b>Lagging Indicators:</b> These indicators show past economic
                performance. Changes to this type of indicators are known only
                after an economic tendency has been formed already. The best
                example of lagging indicators is Unemployment rate.
              </p>
              <p className={classes.para}>
                <b>Leading Indicators:</b> this type of indicators get changed
                when a huge change or adjustment made in the economy is formed.
                so, this type of indicator could be used to predict upcoming
                trends. the best example of Leading indicators is retail sales.
              </p>
              <p className={classes.para}>
                A profit calendar should be made in a manner that economic
                indicators are ranked accordingly. Few traders rank these
                indicators as per the volatility that these economic indicators
                may cause in the trading market.
              </p>
              <p className={classes.para}>
                Almost every time when market-changing news comes out, traders
                analyze the present figure with the previous ones also taking
                into analyst's account estimates for the same. These figures
                when set together will assist a trader to become informed
                whether the new information surpasses expectations or going to
                disappoint. This will help traders know their new move.
              </p>
              <p className={classes.para}>
                In this case, A profit calendar can assist a trader get informed
                about all the possible change and so they can act accordingly,
                quicker than their competitors in the trading market. economic
                announcements and political news can change the flow of a
                particular currency pair. Sometimes this flow can change in no
                time and this is where our tool <b>profit calendar</b> comes in
                use.
              </p>
            </div>
          </Grid>
          <Grid item item xs={12} sm={12} md={5} lg={5}>
            <ContactForm />
          </Grid>
        </Grid>
      </Grid>
      <Contact />
      <Footer />
    </div>
  );
}
