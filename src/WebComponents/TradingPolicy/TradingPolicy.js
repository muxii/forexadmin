import React from "react";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Breadcrumbs from "@material-ui/core/Breadcrumbs";
import MobNavbar from "../WebDashboard/MobNavbar/MobNavbar";
import TopNavbar from "../WebDashboard/TopNavbar/TopNavbar";
import Navbar from "../WebDashboard/Navbar/Navbar";
import Contact from "../WebDashboard/Contact/Contact";
import Footer from "../WebDashboard/Footer/Footer";
import ContactForm from "../ContactForm/ContactForm";
import { useStyles } from "./css";
import { Link } from "react-router-dom";

export default function FullWidthGrid() {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <MobNavbar />
      <TopNavbar />
      <Navbar />
      <Grid container spacing={0}>
        <Grid container className={classes.paddingDiv}>
          <Grid item xs={12} sm={12} md={7} lg={7}>
            <div>
              <Breadcrumbs aria-label="breadcrumb" className={classes.titleTop}>
                <Link to="/" className={classes.link}>
                  <b className={classes.breadHome}>Home</b>
                </Link>
                <Typography color="textPrimary">
                  <b className={classes.breadText}>Tools</b>
                </Typography>
                <Typography color="textPrimary">
                  <b className={classes.breadText}>Our Trading Policy</b>
                </Typography>
              </Breadcrumbs>
              <h1 className={classes.title}>Our Trading Policy</h1>
              <div className={classes.bottomBorder}></div>
              <p className={classes.para}>
                profit International Gain Ltd., an investment firm licensed and
                supervised by the Vanuatu Financial Services("vfs") offers
                investment services (the "services") strictly under the
                following terms and conditions, which are non - negotiable and
                may be amended after proper notice has been given to you (the
                "client") at the absolute discretion of vfs subject to the
                provisions of the terms below..
              </p>
              <p className={classes.para}>
                By accepting and agreeing to the terms and conditions of this
                agreement and opening an account, the client accepts the
                following terms and conditions. subsequent to that and (i)
                subject to vfs’s final approval and (ii) upon the client's
                funding of his account, the client enters into a legal and
                binding agreement with vfs.
              </p>
              <p className={classes.para}>
                Whereas, VFS allows the carrying out of transactions in foreign
                currency exchange rates, in contracts for differences ("CFDs")
                based on foreign exchange rates and other financial assets,
                including, without limitation, commodities, shares and indices,
                as well as in options and other Financial Instruments, all as
                may be offered from time to time by FIH at its sole discretion,
                all subject to the terms and conditions set forth in this
                Agreement; and Whereas The Client wishes to open an account with
                FIH for purposes of carrying out such Transactions ("Account");
              </p>
              <ul>
                <li>
                  <p className={classes.para}>
                    Scope of Agreement & Definitions
                  </p>
                </li>
                <li>
                  <p className={classes.para}>
                    Opening of Account & Appointment of Agent
                  </p>
                </li>
                <li>
                  <p className={classes.para}>Trading</p>
                </li>
                <li>
                  <p className={classes.para}>
                    Guarantee and Margin Requirement; Limit on Transactions; No
                    Interest Bearing Account
                  </p>
                </li>
                <li>
                  <p className={classes.para}>
                    EXPIRY OF TRANSACTIONS; ROLLOVER
                  </p>
                </li>
                <li>
                  <p className={classes.para}>
                    MARKET SUSPENSION AND DELISTING; CORPORATE ACTIONS
                  </p>
                </li>
                <li>
                  <p className={classes.para}>
                    Use of the Internet Trading Platform and Website.
                  </p>
                </li>
                <li>
                  <p className={classes.para}>
                    Withdrawal from and Closing of the Account by Client
                  </p>
                </li>
                <li>
                  <p className={classes.para}>Pricing & Commissions</p>
                </li>
                <li>
                  <p className={classes.para}>Reports</p>
                </li>
                <li>
                  <p className={classes.para}>Risks & Indemnification.</p>
                </li>
                <li>
                  <p className={classes.para}>Telephone & Recording</p>
                </li>
                <li>
                  <p className={classes.para}>Law and Jurisdiction</p>
                </li>
                <li>
                  <p className={classes.para}>Entire Agreement & Amendments</p>
                </li>
                <li>
                  <p className={classes.para}>Assignment</p>
                </li>
                <li>
                  <p className={classes.para}>Termination & Force Major</p>
                </li>
                <li>
                  <p className={classes.para}>Severability</p>
                </li>
                <li>
                  <p className={classes.para}>Communications</p>
                </li>
                <li>
                  <p className={classes.para}>Trading Service Provider (TSP)</p>
                </li>
                <li>
                  <p className={classes.para}>
                    iPayments only allow a maximum deposit of USD/EUR/GBP 5000.
                  </p>
                </li>
              </ul>
            </div>
          </Grid>
          <Grid item item xs={12} sm={12} md={5} lg={5}>
            <ContactForm />
          </Grid>
        </Grid>
      </Grid>
      <Contact />
      <Footer />
    </div>
  );
}
