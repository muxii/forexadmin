import React from "react";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Breadcrumbs from "@material-ui/core/Breadcrumbs";
import MobNavbar from "../WebDashboard/MobNavbar/MobNavbar";
import TopNavbar from "../WebDashboard/TopNavbar/TopNavbar";
import Navbar from "../WebDashboard/Navbar/Navbar";
import Contact from "../WebDashboard/Contact/Contact";
import Footer from "../WebDashboard/Footer/Footer";
import ContactForm from "../ContactForm/ContactForm";
import { useStyles } from "./css";
import { Link } from "react-router-dom";

export default function FullWidthGrid() {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <MobNavbar />
      <TopNavbar />
      <Navbar />
      <Grid container spacing={0}>
        <Grid container className={classes.paddingDiv}>
          <Grid item xs={12} sm={12} md={7} lg={7}>
            <div>
              <Breadcrumbs aria-label="breadcrumb" className={classes.titleTop}>
                <Link to="/" className={classes.link}>
                  <b className={classes.breadHome}>Home</b>
                </Link>
                <Typography color="textPrimary">
                  <b className={classes.breadText}>Tools</b>
                </Typography>
                <Typography color="textPrimary">
                  <b className={classes.breadText}>Live Market Rates</b>
                </Typography>
              </Breadcrumbs>
              <h1 className={classes.title}>Live Market Rates</h1>
              <div className={classes.bottomBorder}></div>
              <p className={classes.para}>
                This text help to introduce the principles, terminology and code
                systems required to expand packages that utilise actual-time
                inventory marketplace statistics (e.g. buying and selling
                programs). It discusses buying and selling standards, the
                extraordinary forms of market facts available, and affords a
                practical instance on how to manner statistics feed occasions
                right into a market object version.
              </p>
              <p className={classes.para}>
                The object is aimed at intermediate to advanced builders who
                wish to advantage an expertise of basic economic market records
                processing. i advocate that people who are already familiar with
                buying and selling terminology skip ahead to the marketplace
                statistics phase.
              </p>
              <p className={classes.para}>
                A stack trade is an alternate in which inventory agents and
                investors can buy and promote shares of inventory, bonds, and
                different securities. Many huge organizations have their stocks
                indexed on a stock change.
              </p>
              <p className={classes.para}>
                In india, there are two most important inventory exchanges – BSE
                (Bomaby Stock Exchange) and NSE ( National Stock Exchange). BSE
                is the oldest stock trade in asia and NSE is the largest
              </p>
              <h4>In the country.</h4>
              <p className={classes.para}>
                View live foreign exchange rates at a look. We replace our facts
                in actual-time to reveal live rates on currencies, indices and
                commodities – and help you determine the opportune second to go
                into or go out a change.
              </p>
              <p className={classes.para}>
                Globally, the foreign exchange market is open across the clock
                24 hours a day from Monday to Friday. profit trading in India,
                exchange is possible from 9 am to five pm it’s in Indian pairs
                and from 9 am to 7:30 pm in international pairs. Foreign
                exchange trading is accomplished through Indian foreign exchange
                brokers and trading occurs on the NSE and MCX-SX.
              </p>
              <p className={classes.para}>
                Trading Platform plays an important role if it comes to
                accomplishing online trading successfully. On line buying and
                selling may be completed via the use of both a web based trading
                platform or computing device based platform and these days going
                famous cell app based buying and selling platform. In this
                article, we're consisting of the ones brokers who offer
                exceptional terminal based trading platforms. Learn to Trade
                with profit4money Beginner guide to profit trading. If you are
                in India, log on to Equitymaster today! Equitymaster is India's
                arch absolute disinterestedness analysis initiative.
              </p>
            </div>
          </Grid>
          <Grid item item xs={12} sm={12} md={5} lg={5}>
            <ContactForm />
          </Grid>
        </Grid>
      </Grid>
      <Contact />
      <Footer />
    </div>
  );
}
