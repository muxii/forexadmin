import React from "react";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Breadcrumbs from "@material-ui/core/Breadcrumbs";
import Accordion from "@material-ui/core/Accordion";
import AccordionSummary from "@material-ui/core/AccordionSummary";
import AccordionDetails from "@material-ui/core/AccordionDetails";
import ExpandMoreIcon from "@material-ui/icons/ExpandMore";
import MobNavbar from "../WebDashboard/MobNavbar/MobNavbar";
import TopNavbar from "../WebDashboard/TopNavbar/TopNavbar";
import Navbar from "../WebDashboard/Navbar/Navbar";
import Contact from "../WebDashboard/Contact/Contact";
import Footer from "../WebDashboard/Footer/Footer";
import ContactForm from "../ContactForm/ContactForm";
import { useStyles } from "./css";
import { Link } from "react-router-dom";

export default function FullWidthGrid() {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <MobNavbar />
      <TopNavbar />
      <Navbar />
      <Grid container spacing={0}>
        <Grid container className={classes.paddingDiv}>
          <Grid item xs={12} sm={12} md={7} lg={7}>
            <div>
              <Breadcrumbs aria-label="breadcrumb" className={classes.titleTop}>
                <Link to="/" className={classes.link}>
                  <b className={classes.breadHome}>Home</b>
                </Link>
                <Typography color="textPrimary">
                  <b className={classes.breadText}>profit</b>
                </Typography>
                <Typography color="textPrimary">
                  <b className={classes.breadText}>profit4money Conditions</b>
                </Typography>
              </Breadcrumbs>
              <h1 className={classes.title}>profit4money Conditions</h1>
              <div className={classes.bottomBorder}></div>
              <p className={classes.para}>
                We endeavor to bring forward all the best elements, tools,
                support and services so that our clients could make out the most
                in prevailing financial markets with smart investments and
                appreciable returns. However, while you make your investments
                with us, there are several important terms and conditions and
                regulations that need to be considered. Before you dive into the{" "}
                <b>profit market</b>, it is suggestible to get through these.
              </p>
              <div className={classes.root}>
                <Accordion>
                  <AccordionSummary
                    style={{ backgroundColor: "#25707D", color: "#fff" }}
                    expandIcon={<ExpandMoreIcon style={{ color: "#fff" }} />}
                    aria-controls="panel1a-content"
                    id="panel1a-header"
                  >
                    <Typography className={classes.heading}>
                      Trading Hours
                    </Typography>
                  </AccordionSummary>
                  <AccordionDetails>
                    <p className={classes.para}>
                      With profit4money almost all products are known to be
                      traded over a period of 24 hours without a break. This
                      considers market opening on Sunday 22:00 GMT, and closing
                      on Friday 21:00 GMT. There are considerable variations in
                      trading hours depending on public holidays, seasonal time
                      variations, unusual liquidity due to unexpected events.
                      There could however be several products that are traded
                      only during specific hours.
                    </p>
                  </AccordionDetails>
                </Accordion>
                <br />
                <Accordion>
                  <AccordionSummary
                    style={{ backgroundColor: "#25707D", color: "#fff" }}
                    expandIcon={<ExpandMoreIcon style={{ color: "#fff" }} />}
                    aria-controls="panel2a-content"
                    id="panel2a-header"
                  >
                    <Typography className={classes.heading}>
                      Margin Requirements
                    </Typography>
                  </AccordionSummary>
                  <AccordionDetails>
                    <Grid container>
                      <Grid item xs={12}>
                        <p className={classes.para}>
                          At profit4money you would be offered with leveraged
                          trading services with respect to a wide range of
                          financial instruments. These include profit,
                          commodities, precious metals and index based CFDs.
                          This way you can trade for significant positions with
                          accounts that are modestly sized.
                        </p>
                        <p className={classes.para}>
                          “Margin” turns out to be collateral that could cover
                          up losses that you incur and lets your hold and manage
                          a value that is much greater than your actual account.
                          Thus you have a chance of generating significant
                          profits. Leverage sometimes acts like a double-edged
                          sword, just the way it improves profits, it can also
                          amplify losses. Excessive leverage could create
                          greater chances of risks.
                        </p>
                        <p className={classes.para}>
                          If you hold an account with us, you will be entitled
                          to the Negative Balance Protection program. Your
                          margin requirements are automatically calculated by
                          our platform way before the execution of any orders
                          and the availability of funds is also checked.
                          Considering potential risks, specific margin
                          requirements would be taken into account. This is
                          probably the reason for increased margin requirements
                          around or during trading breaks.
                        </p>
                      </Grid>
                    </Grid>
                  </AccordionDetails>
                </Accordion>
                <br />
                <Accordion>
                  <AccordionSummary
                    style={{ backgroundColor: "#25707D", color: "#fff" }}
                    expandIcon={<ExpandMoreIcon style={{ color: "#fff" }} />}
                    aria-controls="panel2a-content"
                    id="panel2a-header"
                  >
                    <Typography className={classes.heading}>
                      CFD Expiration Dates
                    </Typography>
                  </AccordionSummary>
                  <AccordionDetails>
                    <p className={classes.para}>
                      Clients are given the benefits of CFD online along with
                      other considerable trading options. CFDs are known to be
                      limited to a certain amount of time and come along a date
                      of expiry. After the expiry date no such accounts could be
                      traded and all positions need to be closed.
                    </p>
                  </AccordionDetails>
                </Accordion>
                <br />
                <Accordion>
                  <AccordionSummary
                    style={{ backgroundColor: "#25707D", color: "#fff" }}
                    expandIcon={<ExpandMoreIcon style={{ color: "#fff" }} />}
                    aria-controls="panel2a-content"
                    id="panel2a-header"
                  >
                    <Typography className={classes.heading}>
                      Trading Rules
                    </Typography>
                  </AccordionSummary>
                  <AccordionDetails>
                    <Grid container>
                      <Grid item xs={12}>
                        <h2 className={classes.subTitle}>
                          Order Execution Policy
                        </h2>
                        <p className={classes.para1}>
                          When the market price reaches the price indicated
                          within an order like buy stop, buy limit, sell stop or
                          sell limit, the orders are instantly executed with
                          regards to the price shown in the order. Executing
                          orders could be impossible on client’s request under
                          specific or prevailing market conditions.
                        </p>
                        <h2 className={classes.subTitle}>
                          Closing profit Transactions
                        </h2>
                        <p className={classes.para1}>
                          If in case the client does not complete the open
                          Forward and Spot Transactions until they reach their
                          value date, they are likely to be rolled over for two
                          additional business days. Consequently, the open
                          transactions would be rolled over for two additional
                          business days until FIH closes the account as per
                          agreed terms of the contract.
                        </p>
                        <h2 className={classes.subTitle}>Hedging</h2>
                        <p className={classes.para1}>
                          Hedging refers to opening two transactions with the
                          same underlying asset or currency pair in different
                          directions irrespective of time or quantity. Hedging
                          transactions may be taken into consideration for
                          calculating the minimum margin on “net” or cumulative
                          basis.
                        </p>
                        <p className={classes.para1}>
                          While you may be planning to enter the{" "}
                          <b>profit market</b>, these guidelines are likely to
                          hold importance.
                        </p>
                      </Grid>
                    </Grid>
                  </AccordionDetails>
                </Accordion>
              </div>
            </div>
          </Grid>
          <Grid item item xs={12} sm={12} md={5} lg={5}>
            <ContactForm />
          </Grid>
        </Grid>
      </Grid>
      <Contact />
      <Footer />
    </div>
  );
}
