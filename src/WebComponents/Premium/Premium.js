import React from "react";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Breadcrumbs from "@material-ui/core/Breadcrumbs";
import MobNavbar from "../WebDashboard/MobNavbar/MobNavbar";
import TopNavbar from "../WebDashboard/TopNavbar/TopNavbar";
import Navbar from "../WebDashboard/Navbar/Navbar";
import Contact from "../WebDashboard/Contact/Contact";
import Footer from "../WebDashboard/Footer/Footer";
import ContactForm from "../ContactForm/ContactForm";
import { useStyles } from "./css";
import { Link } from "react-router-dom";

export default function FullWidthGrid() {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <MobNavbar />
      <TopNavbar />
      <Navbar />
      <Grid container spacing={0}>
        <Grid container className={classes.paddingDiv}>
          <Grid item xs={12} sm={12} md={7} lg={7}>
            <div>
              <Breadcrumbs aria-label="breadcrumb" className={classes.titleTop}>
                <Link to="/" className={classes.link}>
                  <b className={classes.breadHome}>Home</b>
                </Link>
                <Typography color="textPrimary">
                  <b className={classes.breadText}>Tools</b>
                </Typography>
                <Typography color="textPrimary">
                  <b className={classes.breadText}>Premium</b>
                </Typography>
              </Breadcrumbs>
              <h1 className={classes.title}>Premium</h1>
              <div className={classes.bottomBorder}></div>
              <p className={classes.para}>
                profit4money premium trading account enables traders to get
                access to services and benefits that are not provided on other
                trading accounts. Premium account is made to serve the needs of
                profit traders with more active accounts. no doubt each trader
                want to become wealthy through trading. And our team will assist
                you to lead the right path. Our premium accounts increase the
                chances of huge earning.
              </p>
              <h2 className={classes.subTitle}>
                Our premium clients benefit from:
              </h2>
              <div className={classes.bottomBorder}></div>
              <h4>Free services and reduced charges</h4>
              <p className={classes.para1}>
                At profit4money, we offer premium clients with cutting-edge
                technologies and advanced charting options to real-time data
                feeds, we try to offer the best experience to our premium as
                well as non-premium clients. we even negotiate charges for our
                premium clients.
              </p>
              <h4>Super fast service</h4>
              <p className={classes.para1}>
                Get benefit from our rocket service for your profit trades and
                requests you might have.
              </p>
              <h4>VIP treatment</h4>
              <p className={classes.para1}>
                Get advantage from our rapid response times to your emails,
                msgs, and calls. premium clients will also get invited to
                private events.
              </p>
              <h4>Higher interested</h4>
              <p className={classes.para1}>
                You get higher interests on your account balance.
              </p>
              <h2 className={classes.subTitle}>
                profit4money team of experts:
              </h2>
              <div className={classes.bottomBorder}></div>
              <p className={classes.para}>
                Our expert team knows your trading needs and financial
                aspirations, so we can offer tailored solutions that work best
                for you. We can assist you to get the most out of your trading,
                we focus to bring you the best solutions by our premium
                services.
              </p>

              <p className={classes.para}>
                Our managers for <b>premium accounts</b> are here to create
                long-lasting relationships with our clients, by offering them
                with specialist and informed services.
              </p>
              <p className={classes.para}>
                All pur Premium Clients are assigned to manager. You can contact
                to your manager through email, phone, or face-to-face,to offer
                you expert guidance as well as support whenever you require.
                They can also get expert investment advice from our experts to
                assist them find the right place to invest their hard earned
                money.
              </p>
              <p className={classes.para}>
                profit4money wants you to become a knowledgeable and successful
                trader in the profit market. If you are a beginner, but trading
                as a premium investor, worry not! As we are there to make every
                step simple to you. to earn high profit, become a premium
                investor today at profit4money.
              </p>
              <p className={classes.para}>
                At profit4money, you earn the real benefits of profit Trading!
                Open your account today with us and get the best trading
                experience. The process to sign up is very simple and
                straightforward. For any query, contact us through gmail or
                contact number. You can also subscribe to our newsletter, its
                free. You will get updated by trading tricks, tips and
                strategies to maximize your earning.
              </p>
            </div>
          </Grid>
          <Grid item item xs={12} sm={12} md={5} lg={5}>
            <ContactForm />
          </Grid>
        </Grid>
      </Grid>
      <Contact />
      <Footer />
    </div>
  );
}
