import React from "react";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Breadcrumbs from "@material-ui/core/Breadcrumbs";
import MobNavbar from "../WebDashboard/MobNavbar/MobNavbar";
import TopNavbar from "../WebDashboard/TopNavbar/TopNavbar";
import Navbar from "../WebDashboard/Navbar/Navbar";
import Contact from "../WebDashboard/Contact/Contact";
import Footer from "../WebDashboard/Footer/Footer";
import ContactForm from "../ContactForm/ContactForm";
import { useStyles } from "./css";
import { Link } from "react-router-dom";

export default function FullWidthGrid() {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <MobNavbar />
      <TopNavbar />
      <Navbar />
      <Grid container spacing={0}>
        <Grid container className={classes.paddingDiv}>
          <Grid item xs={12} sm={12} md={7} lg={7}>
            <div>
              <Breadcrumbs aria-label="breadcrumb" className={classes.titleTop}>
                <Link to="/" className={classes.link}>
                  <b className={classes.breadHome}>Home</b>
                </Link>
                <Typography color="textPrimary">
                  <b className={classes.breadText}>Tools</b>
                </Typography>
                <Typography color="textPrimary">
                  <b className={classes.breadText}>Account Comparisions</b>
                </Typography>
              </Breadcrumbs>
              <h1 className={classes.title}>Account Comparisions</h1>
              <div className={classes.bottomBorder}></div>
              <p className={classes.para}>
                profit4money is committed to offering its potential clients with
                the best trading experience. we offer types of trading accounts,
                all with distinct and highly competitive trading conditions,
                they are made to meet the requirements and demands of each
                trader.
              </p>
              <h2 className={classes.subTitle}>Demo Account</h2>
              <div className={classes.bottomBorder}></div>
              <p className={classes.para}>
                One of the best ways for a profit beginner to enter the currency
                worlds is to begin practicing on a demo account. as the saying
                goes, “Practice makes perfect and this saying goes best and this
                couldn’t be more accurate with profit trading. So, if you want
                to test your trading skills, choose from one of our profit demo
                accounts and start trading.
              </p>
              <h2 className={classes.subTitle}>
                Benefits of profit Demo Account:
              </h2>
              <h2 className={classes.subTitle}>
                Offers a great learning experience
              </h2>
              <p className={classes.para}>
                Demo trading platforms are basically made to offer newbie
                traders with great learning experiences. They let every trader
                learn the basics from how to enter and exit foreign exchange
                positions without bearing any loss.
              </p>
              <p className={classes.para}>
                if you want to go profitable in the trade market, then you
                should know all the strategies and techniques involved. knowing
                these profit tricks is very important since you can not perform
                with them using authentic money. Without getting fully aware of
                the impacts that come about using such techniques you may end up
                losing money in trading.
              </p>
              <h2 className={classes.subTitle}>Live account</h2>
              <div className={classes.bottomBorder}></div>
              <p className={classes.para}>
                Fore4money live accounts have their own goal and personality;
                each account is reliable to a different type of trader or
                investor and each shows a new window of opportunity to the
                profit currency world. from newbies to expert traders, from
                minor depositors to major, the type of live accounts we provide
                is a way to your financial goals.
              </p>
              <p className={classes.para}>
                A trading live account is where you trade using real money. Any
                profit or loss is real. Live account is different from a demo
                account in terms of execution, spreads, and slippage.
              </p>
              <p className={classes.para}>
                <b>Spreads are normally tighter in a demo</b> trading condition.
                Analyze this to a live trading account and you may observe the
                difference in spreads in the demo is almost 1 pip.
              </p>
              <p className={classes.para}>
                profit4money offers types of live accounts for each trader with
                varying stop levels, deposits, spreads, and commissions. You are
                supposed to choose the one that suits you best and fulfills your
                goals. The best way to choose the right one for you is to
                compare them and choose the best one.
              </p>
            </div>
          </Grid>
          <Grid item item xs={12} sm={12} md={5} lg={5}>
            <ContactForm />
          </Grid>
        </Grid>
      </Grid>
      <Contact />
      <Footer />
    </div>
  );
}
