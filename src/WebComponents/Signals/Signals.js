import React from "react";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Breadcrumbs from "@material-ui/core/Breadcrumbs";
import MobNavbar from "../WebDashboard/MobNavbar/MobNavbar";
import TopNavbar from "../WebDashboard/TopNavbar/TopNavbar";
import Navbar from "../WebDashboard/Navbar/Navbar";
import Contact from "../WebDashboard/Contact/Contact";
import Footer from "../WebDashboard/Footer/Footer";
import ContactForm from "../ContactForm/ContactForm";
import { useStyles } from "./css";
import { Link } from "react-router-dom";

export default function FullWidthGrid() {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <MobNavbar />
      <TopNavbar />
      <Navbar />
      <Grid container spacing={0}>
        <Grid container className={classes.paddingDiv}>
          <Grid item xs={12} sm={12} md={7} lg={7}>
            <div>
              <Breadcrumbs aria-label="breadcrumb" className={classes.titleTop}>
                <Link to="/" className={classes.link}>
                  <b className={classes.breadHome}>Home</b>
                </Link>
                <Typography color="textPrimary">
                  <b className={classes.breadText}>Tools</b>
                </Typography>
                <Typography color="textPrimary">
                  <b className={classes.breadText}>Signals</b>
                </Typography>
              </Breadcrumbs>
              <h1 className={classes.title}>ADVANCED TRADING SIGNALS</h1>
              <div className={classes.bottomBorder}></div>
              <p className={classes.para}>
                At profit4money, we are here to prove that our profit signals
                will guide you with professional advice and strategies regarding
                when to purchase and sell currency pairs without hurting your
                pocket.
              </p>
              <p className={classes.para}>
                profit signals is a popular software made by trading experts who
                trade in the profit Market. This tool made to make the process
                of trading currencies available for everyone- no matter the
                trader has enough knowledge about the market. The analyst will
                collect data from the profit market and offer suggestions on how
                to earn profit from it, which is afterwards forwarded to users
                through various methods.
              </p>
              <h2 className={classes.subTitle}>
                How you can benefit from our Advanced Trading Signals?
              </h2>
              <div className={classes.bottomBorder}></div>
              <p className={classes.para}>
                Our Advanced profit signal tool comes with in-depth tutorials
                about market trends so that users can watch to polish their
                knowledge in the profit market. this software gives users a huge
                amount of time to boost their knowledge of the market and
                software itself. Using this software will surely increase the
                chance of becoming a successful trader.profit4money suggest you
                make the most of the best profit signals.
              </p>
              <p className={classes.para}>
                its very simple to use our Advanced Trading Signals: use trading
                signals to understand when your trades are generating fund or
                copy recommended trading tips to your trading account.
              </p>
              <h2 className={classes.subTitle}>List of Trading Signals:</h2>
              <div className={classes.bottomBorder}></div>
              <h2 className={classes.subTitle}>Currencies</h2>
              <p className={classes.para}>
                AUD/CHF AUD/JPY AUD/AD EUR/CAD AUD/USD CAD/JPY CHF/JPY CHF/AUD
                EUR/CCHF EUR/CZK EUR/GBP EUR/HUF EUR/JPY EUR/NOK EUR/JPY NZD/NZD
                EUR/CAD NZD/PLN EUR/SEK EUR/TRY EUR/USD GBP/AUD GBP/CAD GBP/CHF
                GBP/JPY GBP/NZD GBP/NZD AUD/USD NZD/CHF NZD/USD SGD/JPY USD/PLN
                EUR/CAD USD/CHF USD/CZK USD/DKK USD/HKD USD/HUF USD/ILS USD/JPY
                USD/MXN USD/NOK USD/PLN USD/SEK USD/SGD USD/TRY USD/ZAR
              </p>
              <h2 className={classes.subTitle}>Commodities</h2>
              <p className={classes.para}>
                COF/USD OIL/USD SOY/COF/USD XPT/USD SUG/USD XAG/USD COR/USD
                XAU/USD/USD GAS/
              </p>
              <h2 className={classes.subTitle}>Indices</h2>
              <p className={classes.para}>
                USD GAS/COF/COF/USD XAU/USD SOY/USD SUG/USD XAG/USD COR/USD/USD
                OIL/USD XPT/
              </p>
              <p className={classes.para}>
                profit4money Advanced Trading Signals are highly customizable,
                letting you manage them as per your trading needs as well as
                assisting you make informed trading decisions. Our Trading
                signals are broadcast straight on your devices for your
                ease.these days thousands of traders are using this tool, no
                matter how much experienced they are.using Trading signals will
                enhance the probability of earning profit and get you on proper
                track to profit trading success.
              </p>
            </div>
          </Grid>
          <Grid item item xs={12} sm={12} md={5} lg={5}>
            <ContactForm />
          </Grid>
        </Grid>
      </Grid>
      <Contact />
      <Footer />
    </div>
  );
}
