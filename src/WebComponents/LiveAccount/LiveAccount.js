import React from "react";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Breadcrumbs from "@material-ui/core/Breadcrumbs";
import MobNavbar from "../WebDashboard/MobNavbar/MobNavbar";
import TopNavbar from "../WebDashboard/TopNavbar/TopNavbar";
import Navbar from "../WebDashboard/Navbar/Navbar";
import Contact from "../WebDashboard/Contact/Contact";
import Footer from "../WebDashboard/Footer/Footer";
import ContactForm from "../ContactForm/ContactForm";
import { useStyles } from "./css";
import { Link } from "react-router-dom";

export default function FullWidthGrid() {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <MobNavbar />
      <TopNavbar />
      <Navbar />
      <Grid container spacing={0}>
        <Grid container className={classes.paddingDiv}>
          <Grid item xs={12} sm={12} md={7} lg={7}>
            <div>
              <Breadcrumbs aria-label="breadcrumb" className={classes.titleTop}>
                <Link to="/" className={classes.link}>
                  <b className={classes.breadHome}>Home</b>
                </Link>
                <Typography color="textPrimary">
                  <b className={classes.breadText}>Tools</b>
                </Typography>
                <Typography color="textPrimary">
                  <b className={classes.breadText}>Open Live Account</b>
                </Typography>
              </Breadcrumbs>
              <h1 className={classes.title}>
                Open Live Account with profit4money
              </h1>
              <div className={classes.bottomBorder}></div>
              <p className={classes.para}>
                Open profit trading account in profit4money. We train traders on
                how to open an account and help each client to turn their
                financial aspirations into reality by training them. open your
                live trading account today. Trade on our platform, highly
                optimized.
              </p>
              <h2 className={classes.subTitle}>
                Difference between Demo account and Live Account
              </h2>
              <div className={classes.bottomBorder}></div>
              <p className={classes.para}>
                Do not get confused between two Live accounts and Demo Account.
                Trading with a demo account means no profit and loss is real.
                but trading with a live account means all profit and loss are
                real. You can practice using Demo account, where you can learn
                the tricks, basics, and techniques that are important learning
                in the world of profit trading.
              </p>
              <p className={classes.para}>
                A profit live account is the main account. Open it with
                profit4money today to start live trading. Our process of
                creating an account is very simple. If you want to create a
                corporate account or an individual account, our process makes it
                a piece of make. Don't worry about your strategic knowledge,
                skill level or budget. We have a suited solution for each
                trader. it is secure, reliable and fast. there is no hidden
                terms and conditions and hidden cost. We believe is an open and
                fair approach.
              </p>
              <p className={classes.para}>
                When you open a live trading account with profit4money, we
                ensure that you will gain maximum profits. As you know the
                condition in a demo account is changed from live accounts, like
                margin and leverage setting, market volatility and pricing, so
                do not expect the results in the demo account to get shown in
                the live account similarly.
              </p>
              <p className={classes.para}>
                No doubt, demo accounts are best for those who are new to profit
                and want to learn the basics of trading. Here, at profit4money,
                our skilled professionals are always there to assist you. we
                would love to help you round the clock to let our users earn a
                good amount of money. wait no more! And get started with profit
                trading today.
              </p>
              <h2 className={classes.subTitle}>
                Simple and straight process for opening an account
              </h2>
              <p className={classes.para}>
                Its very simple to open a live account and start trading with
                profit4money/ Just fill the details, confirm your ID online,
                then use the most flexible and reliable payment method to
                deposit capital into your trading account.
              </p>
              <h2 className={classes.subTitle}>
                Before you open an account, make sure to read our terms and
                conditions. As Investing in CFDs carries certain risks and is
                not appropriate for all investors.
              </h2>
            </div>
          </Grid>
          <Grid item item xs={12} sm={12} md={5} lg={5}>
            <ContactForm />
          </Grid>
        </Grid>
      </Grid>
      <Contact />
      <Footer />
    </div>
  );
}
