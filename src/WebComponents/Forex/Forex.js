import React from "react";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Breadcrumbs from "@material-ui/core/Breadcrumbs";
import MobNavbar from "../WebDashboard/MobNavbar/MobNavbar";
import TopNavbar from "../WebDashboard/TopNavbar/TopNavbar";
import Navbar from "../WebDashboard/Navbar/Navbar";
import Contact from "../WebDashboard/Contact/Contact";
import Footer from "../WebDashboard/Footer/Footer";
import ContactForm from "../ContactForm/ContactForm";
import { useStyles } from "./css";
import { Link } from "react-router-dom";

export default function FullWidthGrid() {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <MobNavbar />
      <TopNavbar />
      <Navbar />
      <Grid container spacing={0}>
        <Grid container className={classes.paddingDiv}>
          <Grid item xs={12} sm={12} md={7} lg={7}>
            <div>
              <Breadcrumbs aria-label="breadcrumb" className={classes.titleTop}>
                <Link to="/" className={classes.link}>
                  <b className={classes.breadHome}>Home</b>
                </Link>
                <Typography color="textPrimary">
                  <b className={classes.breadText}>profit</b>
                </Typography>
                <Typography color="textPrimary">
                  <b className={classes.breadText}>What is profit?</b>
                </Typography>
              </Breadcrumbs>
              <h1 className={classes.title}>What is profit?</h1>
              <div className={classes.bottomBorder}></div>
              <p className={classes.para}>
                While you may be handling your business deals well enough, you
                certainly know enough about foreign trade and the various
                peculiarities it may poise. For beginners who might be looking
                for profit meaning, it is nothing very different but a commonly
                used abbreviation for foreign exchange. This single term
                determines the various activities that would take place in the
                foreign exchange market among investors and speculators
                including others important sources that may be effective
                accordingly.
              </p>
              <h2 className={classes.subTitle}>
                Foreign Exchange Rate and Its Significance for trade
              </h2>
              <div className={classes.bottomBorder}></div>
              <p className={classes.para}>
                You may already know well about the concept of foreign exchange.
                In the decentralized global foreign exchange marketplace, the
                relative values of different currencies are considered to avail
                dealers with appropriate foreign currency. There are no
                centralized depositories where all these transactions and
                procedures could be conducted. Instead, there are several
                parties that do these things at considerable locations. Here, an
                important consideration is that there are rarely any two
                currencies that are identical to one another. Also, the exchange
                rate for currencies keeps changing.
              </p>
              <h2 className={classes.subTitle}>What is profit Trading?</h2>
              <div className={classes.bottomBorder}></div>
              <p className={classes.para}>
                This is being discussed since the utmost beginning. In simple
                words, profit Trading refers to the trading of different
                currencies across respective countries. Consider an example, of
                US Dollar against Euro. You would be in need of foreign currency
                for anything you’re seeking to do in other country. It could be
                traveling, trading, education etc. Since here our main concern
                is about trade, we proceed with reference to that.
              </p>
              <p className={classes.para}>
                To make payments at an organization in Dubai, you will require
                UAE Dhirams, not Indian Rupees. You will therefore need to buy
                UAE Dhirams against Indian Rupees. Here, relative values of
                these currencies would again be considered to make out how many
                UAE Dhirams could be obtained for specific amount of Indian
                Rupee. The relative values of currencies continually fluctuate
                based on the GDP or national income and various indices that
                determine these aspects. For this exchange, brokers are likely
                to leverage suitable rate considering the size of your trade.
                However, among any other brokers, profit trading brokers offer
                the best leverage.
              </p>
              <p className={classes.para}>
                The market keeps trending up and down and you are likely to
                encounter losses and gains accordingly. Moreover, trading with
                profit4money could let you move towards safer trading and better
                risk management. Certainly, you can look up to obtaining best
                profit exchange and maximize gains for your trade considerably.
                Finding guidance with respect to the same from time to time
                could help in maintaining a proper check on everything.
              </p>
            </div>
          </Grid>
          <Grid item item xs={12} sm={12} md={5} lg={5}>
            <ContactForm />
          </Grid>
        </Grid>
      </Grid>
      <Contact />
      <Footer />
    </div>
  );
}
