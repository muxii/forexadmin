import React from "react";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Breadcrumbs from "@material-ui/core/Breadcrumbs";
import MobNavbar from "../WebDashboard/MobNavbar/MobNavbar";
import TopNavbar from "../WebDashboard/TopNavbar/TopNavbar";
import Navbar from "../WebDashboard/Navbar/Navbar";
import Contact from "../WebDashboard/Contact/Contact";
import Footer from "../WebDashboard/Footer/Footer";
import ContactForm from "../ContactForm/ContactForm";
import { useStyles } from "./css";
import { Link } from "react-router-dom";

export default function FullWidthGrid() {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <MobNavbar />
      <TopNavbar />
      <Navbar />
      <Grid container spacing={0}>
        <Grid container className={classes.paddingDiv}>
          <Grid item xs={12} sm={12} md={7} lg={7}>
            <div>
              <Breadcrumbs aria-label="breadcrumb" className={classes.titleTop}>
                <Link to="/" className={classes.link}>
                  <b className={classes.breadHome}>Home</b>
                </Link>
                <Typography color="textPrimary">
                  <b className={classes.breadText}>Tools</b>
                </Typography>
                <Typography color="textPrimary">
                  <b className={classes.breadText}>Trading Support</b>
                </Typography>
              </Breadcrumbs>
              <h1 className={classes.title}>Trading Support</h1>
              <div className={classes.bottomBorder}></div>
              <p className={classes.para}>
                The principles of support and resistance are surely two of the
                maximum relatively discussed attributes of technical evaluation.
                Part of studying chart styles, these terms are used by universal
                traders to consult charge levels on charts that have a tendency
                to behave as boundaries, preventing the price of an asset from
                getting pushed in a positive course. At first, the reason and
                concept in the back of figuring out those tiers appear smooth,
                however as you'll discover, guide and resistance can are
                available various forms, and the concept is extra hard to grasp
                than it first appears.
              </p>
              <h4>What is Support?</h4>
              <p className={classes.para}>
                Support is fee degree wherein a downtrend may be expected to
                pause due to a concentration of demand. As the price of assets
                or securities drops, call for for the shares will increase, as a
                result forming the assist line.
              </p>
              <h4>What is Resistance?</h4>
              <p className={classes.para}>
                Resistance zones arise due to a sell-off when charges boom.
              </p>
              <p className={classes.para}>
                Support andResistanceranges are key concepts that will acutely
                clarify your avenue decisions to a accomplished point. At
                aboriginal glance, the estimated attributes of ranges may arise
                to be a damage to precision, but your abaft stops will ensure
                some advantage for this abeyant problem. Consistently use abaft
                stops if departure a position and consistently be abiding to
                alter them according to the adapted binding or atrocity of your
                avenue strategy.
              </p>
              <p className={classes.para}>
                Support andResistancelevels exist most effective via virtue of
                smart investors' and smart traders' reminiscences in their
                stories with trading a given protection at positive ranges
                within the beyond. Investors could be greater willing to lend
                their help on the equal degree at which a huge crowd of
                investors once purchased the stock. Buyershave a tendency to
                have the expectation that a stock will upward thrust again from
                the same degree because it did in the beyond. Even if those
                investorsdid not maintain the security in a previous
                consultation, they'll nevertheless appearance to history to be
                their guide, examining previous examples of consultation bottoms
                and stages of assist.
              </p>
              <p className={classes.para}>
                You must also notice that particular zones of support or
                resistance are predicted to shift to their opposite as soon as
                their zones have been breached. For example, a robust sector of
                resistance, once breached, becomes a mental victory for smart
                traders and fast turns into a region of support for the ensuing
                fashion as traders maintain to celebrate the positive breakout.
                By evaluation, as soon as a sector of aid is destroyed, the
                mental deflation is all too real, as the chart stubbornly
                refuses to touch this area again within the near future.
              </p>
              <p className={classes.para}>
                If a stock fee is moving between support and resistance ranges,
                then a smart investment strategy generally used by smart buyers,
                is to shop for a stock at assist and sell at resistance
              </p>
              <p className={classes.para}>
                The fundamentals of guide and resistance consist of a help
                degree, which may be thought of because the floor under smart
                trading costs, and a resistance stage, which may be idea of
                because the ceiling. Prises fall and test the guide degree, with
                the intention to both "maintain," and the price will bounce back
                up, or the assist degree will be violated, and the prise will
                drop via the aid and possibly keep decrease to the subsequent
                help stage. Many Universal trading solution companiesalso helps
                in funds investment and trading.The Dax Index is one of the
                international’s maximum actively traded indices and offers
                investors with a high degree of liquidity.
              </p>
            </div>
          </Grid>
          <Grid item item xs={12} sm={12} md={5} lg={5}>
            <ContactForm />
          </Grid>
        </Grid>
      </Grid>
      <Contact />
      <Footer />
    </div>
  );
}
