import React from "react";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Breadcrumbs from "@material-ui/core/Breadcrumbs";
import MobNavbar from "../WebDashboard/MobNavbar/MobNavbar";
import TopNavbar from "../WebDashboard/TopNavbar/TopNavbar";
import Navbar from "../WebDashboard/Navbar/Navbar";
import Contact from "../WebDashboard/Contact/Contact";
import Footer from "../WebDashboard/Footer/Footer";
import ContactForm from "../ContactForm/ContactForm";
import { useStyles } from "./css";
import { Link } from "react-router-dom";

export default function FullWidthGrid() {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <MobNavbar />
      <TopNavbar />
      <Navbar />
      <Grid container spacing={0}>
        <Grid container className={classes.paddingDiv}>
          <Grid item xs={12} sm={12} md={7} lg={7}>
            <div>
              <Breadcrumbs aria-label="breadcrumb" className={classes.titleTop}>
                <Link to="/" className={classes.link}>
                  <b className={classes.breadHome}>Home</b>
                </Link>
                <Typography color="textPrimary">
                  <b className={classes.breadText}>Tools</b>
                </Typography>
                <Typography color="textPrimary">
                  <b className={classes.breadText}>Education Package</b>
                </Typography>
              </Breadcrumbs>
              <h1 className={classes.title}>
                Select profit4money’s online trading courses and learn how to
                trade
              </h1>
              <div className={classes.bottomBorder}></div>
              <p className={classes.para}>
                Traders planning to enter in the market of profit can find
                themselves irritated and steadily going downward, losing
                optimism fast and money even faster. profit investing, whether
                in options, spot contracts or futures- provides huge
                opportunity, however, its a completely new environment as
                compared to the equities market.
              </p>
              <p className={classes.para}>
                Sometimes even the expert trader can fail badly in the field of
                profit by managing the markets likewise. As equity markets
                revolve around the transfer of ownership, on the other hand, the
                currency market runs around by only speculations. However, there
                are some solutions to assist new and old investors to get on the
                right track through Trading Courses.
              </p>
              <p className={classes.para}>
                profit4money provide online courses for traders to learn more
                about trading. Our team will be proving you trading simulations,
                tutorials, training videos, PDF guides and so on. For a beginner
                trader, training programs and tutorials videos like this can be
                too valuable.
              </p>
              <h2 className={classes.subTitle}>
                Our education package includes these three types of courses:
              </h2>
              <ul>
                <li>
                  <p className={classes.para}>
                    EAP Training Program:- Minimum Amount - 300 Dollar (19577)
                    INR
                  </p>
                </li>
                <li>
                  <p className={classes.para}>
                    Advanced Pattern Mastery Course 500 Dollar ( 34000) INR
                  </p>
                </li>
                <li>
                  <p className={classes.para}>
                    FREE training/email list - 100 dollars For Email update.
                  </p>
                </li>
              </ul>
              <p className={classes.para}>
                If you are a newbie in trading, use the best use of our Demo
                accounts. this will let you easily learn about profit trading.
                if you are an intimidate trader, you can switch to mini accounts
                or standard accounts.
              </p>
              <p className={classes.para}>
                With the help of these accounts, you will get everything you are
                seeking to elaborate your trading knowledge like support
                services and useful resources.
              </p>
              <p className={classes.para}>
                Our section of FAQs doesn't contain all the solutions of the
                queries you might have, so ask for help from our help center
                staff who are always at your service to assist you clear your
                doubts you may have.
              </p>
              <h2 className={classes.subTitle}>
                profit4money education training schedule:
              </h2>
              <div className={classes.bottomBorder}></div>
              <ul>
                <li>
                  <p className={classes.para}>
                    We provide online training from Monday to Friday. The
                    timings are from 11:00 am to 8.00pm.
                  </p>
                </li>
                <li>
                  <p className={classes.para}>
                    We offer profit training of 22 working days so that you can
                    take the best advantage of it.
                  </p>
                </li>
              </ul>
              <h2 className={classes.subTitle}>Payment and refund:</h2>
              <div className={classes.bottomBorder}></div>
              <ul>
                <li>
                  <p className={classes.para}>
                    You are requested to make payment prior to the commencement
                    of your training.
                  </p>
                </li>
                <li>
                  <p className={classes.para}>
                    Once you paid for your education package, then there is no
                    cancellation option and no refund of payment.
                  </p>
                </li>
              </ul>
              <p className={classes.para}>
                profit4money offers several types of trading courses, so choose
                wisely. If you are a beginner, don't forget to begin with Demo
                account option. As trading straight with real money can lead to
                money loss and might give you the worst trading experience.
              </p>
            </div>
          </Grid>
          <Grid item item xs={12} sm={12} md={5} lg={5}>
            <ContactForm />
          </Grid>
        </Grid>
      </Grid>
      <Contact />
      <Footer />
    </div>
  );
}
