import React from "react";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Breadcrumbs from "@material-ui/core/Breadcrumbs";
import MobNavbar from "../WebDashboard/MobNavbar/MobNavbar";
import TopNavbar from "../WebDashboard/TopNavbar/TopNavbar";
import Navbar from "../WebDashboard/Navbar/Navbar";
import Contact from "../WebDashboard/Contact/Contact";
import Footer from "../WebDashboard/Footer/Footer";
import ContactForm from "../ContactForm/ContactForm";
import { useStyles } from "./css";
import { Link } from "react-router-dom";

export default function FullWidthGrid() {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <MobNavbar />
      <TopNavbar />
      <Navbar />
      <Grid container spacing={0}>
        <Grid container className={classes.paddingDiv}>
          <Grid item xs={12} sm={12} md={7} lg={7}>
            <div>
              <Breadcrumbs aria-label="breadcrumb" className={classes.titleTop}>
                <Link to="/" className={classes.link}>
                  <b className={classes.breadHome}>Home</b>
                </Link>
                <Typography color="textPrimary">
                  <b className={classes.breadText}>profit</b>
                </Typography>
                <Typography color="textPrimary">
                  <b className={classes.breadText}>Commodities</b>
                </Typography>
              </Breadcrumbs>
              <h1 className={classes.title}>What is a Commodity?</h1>
              <div className={classes.bottomBorder}></div>
              <p className={classes.para}>
                A commodity could be expressed as a group of goods/assets that
                you may deal with in everyday life. These may include metals,
                energy or food items. Commodities are regarded as exchangeable
                in nature and alternate. These could be simply expressed as
                movable goods that can be purchased and sold except for money
                and actionable claims.
              </p>
              <p className={classes.para}>
                Commodities have long found their way into the Indian trading
                market, much before any other countries. However, foreign rule
                and invasion, different government systems, policies and natural
                calamities led to decrease in commodity trading. Presently,
                although there are share market trades or the stock market,
                commodity trade continues to gain back its importance.
              </p>
              <h2 className={classes.subTitle}>
                Difference between profit trading and Commodity trading
              </h2>
              <div className={classes.bottomBorder}></div>
              <p className={classes.para}>
                <b>Difference in Regulation:</b> the markets for commodity
                trading are known to be way more sophisticated than profit
                trading. Although there are regulations with profit too, there
                is still much scope for better implementations.
              </p>
              <p className={classes.para}>
                <b>Leverage:</b> although both markets offer considerable
                leverage, you will not need to jump over hoops in profit trading
                to obtain it. All need to do is to fund your respective accounts
                with a few hundred dollars and manage thousands. profit trading
                offers spectacular leverage than the commodities market.
              </p>
              <p className={classes.para}>
                <b>Exchange Limits:</b> commodities are traded on exchange and
                have daily range limits unlike foreign exchange that is traded
                through brokers in the interbank market. You are rather not
                allowed to exceed limits with commodities trading.
              </p>
            </div>
          </Grid>
          <Grid item item xs={12} sm={12} md={5} lg={5}>
            <ContactForm />
          </Grid>
          <Grid item item xs={12} sm={12} md={12} lg={12}>
            <div>
              <h2 className={classes.subTitle}>
                Advantages of commodities trading with profit4money
              </h2>
              <p>
                Investing in commodities could often hold several advantages.
                Here are a few:
              </p>
              <p className={classes.para1}>
                <b>Refuge during crisis:</b> although investors do not seem to
                be secure about investing in commodities. It could be a great
                way of investment during tough times.
              </p>
              <p className={classes.para1}>
                <b>Diversified Investment Portfolio:</b> Diversified portfolio
                means a perfect plan for asset allocation. This is one of the
                best aspects with commodity trading since you can keep track of
                the various changes that may occur in the economic-scenarios and
                let you make smart investment with appropriate trading
                strategies.
              </p>
              <p className={classes.para1}>
                <b>Profitable returns</b> commodities are known to be quite
                riskier forms of investments. You could soon come up with great
                investments provided the fact that you’ve made your investment
                right.
              </p>
              <p className={classes.para1}>
                <b>Protection against inflation:</b> Inflation could hit any
                economy very badly. A few commodities you’ve invested in could
                help you maintain the upswing.
              </p>
            </div>
          </Grid>
        </Grid>
      </Grid>
      <Contact />
      <Footer />
    </div>
  );
}
