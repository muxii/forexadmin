import React from "react";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Breadcrumbs from "@material-ui/core/Breadcrumbs";
import MobNavbar from "../WebDashboard/MobNavbar/MobNavbar";
import TopNavbar from "../WebDashboard/TopNavbar/TopNavbar";
import Navbar from "../WebDashboard/Navbar/Navbar";
import Contact from "../WebDashboard/Contact/Contact";
import Footer from "../WebDashboard/Footer/Footer";
import ContactForm from "../ContactForm/ContactForm";
import { useStyles } from "./css";
import { Link } from "react-router-dom";

export default function FullWidthGrid() {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <MobNavbar />
      <TopNavbar />
      <Navbar />
      <Grid container spacing={0}>
        <Grid container className={classes.paddingDiv}>
          <Grid item xs={12} sm={12} md={7} lg={7}>
            <div>
              <Breadcrumbs aria-label="breadcrumb" className={classes.titleTop}>
                <Link to="/" className={classes.link}>
                  <b className={classes.breadHome}>Home</b>
                </Link>
                <Typography color="textPrimary">
                  <b className={classes.breadText}>Tools</b>
                </Typography>
                <Typography color="textPrimary">
                  <b className={classes.breadText}>Disclaimer</b>
                </Typography>
              </Breadcrumbs>
              <h1 className={classes.title}>
                profit4money Disclaimer for profit Trader
              </h1>
              <div className={classes.bottomBorder}></div>
              <p className={classes.para}>
                This legal disclaimer refers to the use of profit4money.com and
                its associated services.
              </p>
              <p className={classes.para}>
                Trading profit on margin contains a certain amount of risk, and
                may not be ideal for all trading investors. The high amount of
                capital can work in favor as well as against you. So, before
                choosing to invest in the profit market you should consider and
                clarify your investment goals, risk appetite as well as a level
                of experience.
              </p>
              <p className={classes.para}>
                Decisions to Sell, purchase, trade or hold insecurities and
                several other types of investments consist of high-level of risk
                and are made based on the guidance of financial experts. Any
                investments or trading securities contain a certain risk of
                loss.
              </p>
              <p className={classes.para}>
                Before undertaking any profit program, you should talk to a
                professional first to avoid a level of risk. profit trading for
                beginners, Must consider whether such trading type is right for
                you as per your financial condition as well as the ability to
                bear monetary risks.
              </p>
              <h2 className={classes.subTitle}>
                profit4money Market opinions:
              </h2>
              <div className={classes.bottomBorder}></div>
              <p className={classes.para}>
                Any news, opinions, analysis or any other data contained on this
                website is served as general market commentary and doesn't form
                investment advice. profit4money will not be responsible for any
                damage or loss, which may result from the use of reliance on
                such data.
              </p>
              <p className={classes.para}>
                This website is not aimed for use or distribution by, any person
                from any country where such use or distribution would be
                obstinate to local regulation.
              </p>
              <h2 className={classes.subTitle}>Distribution</h2>
              <p className={classes.para}>
                At profit4money, none of the investments or services are
                accessible to persons living in any state or country where the
                use of such investments or services would be against local law
                and regulation. Its the site visitors responsibility to read the
                terms and conditions, and obey it.
              </p>
              <h2 className={classes.subTitle}>Information Accuracy</h2>
              <div className={classes.bottomBorder}></div>
              <p className={classes.para}>
                The content on this can be alter or change, remove any content,
                or take strict action for violating any rules at anytime without
                notice and is offered to help profit traders to build
                independent and informed investment decisions. profit4money has
                taken the right measures to assure the accuracy of the data or
                information on the website, but doesn't ensure its accuracy.
              </p>
              <p className={classes.para}>
                We recommend you to get expert financial advice to make sure
                investing or trading in any product is appropriate for your
                concerns, and make sure you read and know any applicable offer
                piece of document.
              </p>
            </div>
          </Grid>
          <Grid item item xs={12} sm={12} md={5} lg={5}>
            <ContactForm />
          </Grid>
        </Grid>
      </Grid>
      <Contact />
      <Footer />
    </div>
  );
}
