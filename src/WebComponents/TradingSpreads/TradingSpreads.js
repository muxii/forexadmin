import React from "react";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Breadcrumbs from "@material-ui/core/Breadcrumbs";
import MobNavbar from "../WebDashboard/MobNavbar/MobNavbar";
import TopNavbar from "../WebDashboard/TopNavbar/TopNavbar";
import Navbar from "../WebDashboard/Navbar/Navbar";
import Contact from "../WebDashboard/Contact/Contact";
import Footer from "../WebDashboard/Footer/Footer";
import ContactForm from "../ContactForm/ContactForm";
import { useStyles } from "./css";
import { Link } from "react-router-dom";

export default function FullWidthGrid() {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <MobNavbar />
      <TopNavbar />
      <Navbar />
      <Grid container spacing={0}>
        <Grid container className={classes.paddingDiv}>
          <Grid item xs={12} sm={12} md={7} lg={7}>
            <div>
              <Breadcrumbs aria-label="breadcrumb" className={classes.titleTop}>
                <Link to="/" className={classes.link}>
                  <b className={classes.breadHome}>Home</b>
                </Link>
                <Typography color="textPrimary">
                  <b className={classes.breadText}>Tools</b>
                </Typography>
                <Typography color="textPrimary">
                  <b className={classes.breadText}>Trading Spreads</b>
                </Typography>
              </Breadcrumbs>
              <h1 className={classes.title}>Trading Spreads</h1>
              <div className={classes.bottomBorder}></div>
              <p className={classes.para}>
                If you are new in profit trading, you may not be familiar with
                the vocabulary used in the world of profit. Few times even the
                easiest concepts can have concealed complexities- this is same
                the case with spreads and Pips.
              </p>
              <h2 className={classes.subTitle}>
                What does Pip mean in Trading?
              </h2>
              <div className={classes.bottomBorder}></div>
              <p className={classes.para}>
                A pip is known to be an incremental price movement which carries
                a specific value which is dependent on the considerable market.
                In simple words, it is the standard unit of measurement for
                recognizing the change in value in the exchange rate.
              </p>
              <p className={classes.para}>
                Originally, pip was considered as the smallest that would be
                done to a FX price. However, with the arrival of other precise
                pricing methods, this original definition does not hold
                importance. Traditionally, FX prices had been quoted for a set
                of decimal places that most commonly include four decimal
                places. A pip was originally considered as a one point movement
                in the last decimal place that has been quoted.
              </p>
              <p className={classes.para}>
                Many brokers are now known to quote profit price for an extra
                decimal value. However, it may now be clear that pip is no
                longer the final decimal place value. It continues to be a
                standardized value across various platforms and among different
                brokers which makes it an important unit of measure. Without
                such a unit of measure, there are chances that apples would be
                compared to oranges. When considering generic points pip
                certainly holds relevance. This measuring unit is essential for
                margin calculation at various points.
              </p>
              <h2 className={classes.subTitle}>What is Spread?</h2>
              <div className={classes.bottomBorder}></div>
              <p className={classes.para}>
                In profit trading, the spread is the difference between two
                things. the difference in the ask/bid/buy/sell price that is
                offered by the broker to the potential traders.
              </p>
              <p className={classes.para}>
                Spreads are usually of two types, Fixed spread and variable
                spread;
              </p>
              <h2 className={classes.subTitle}>WVariable Spread:</h2>
              <div className={classes.bottomBorder}></div>
              <p className={classes.para}>
                In profit trading, a variable spread is a uniformly changing
                price between the bid and ask prices. In simple words, the
                spread you spend on buying a currency pair changes as of things
                such as demand, supply as well as total trading activity.
              </p>
              <h2 className={classes.subTitle}>
                What influences the spread in profit trading?
              </h2>
              <div className={classes.bottomBorder}></div>
              <p className={classes.para}>
                There are various factors which can affect the spread. Some
                factors are;
              </p>
              <ul>
                <li>
                  <p>Market liquidity</p>
                </li>
                <li>
                  <p>Market volatility</p>
                </li>
                <li>
                  <p>market uncertainty</p>
                </li>
                <li>
                  <p>Time of day</p>
                </li>
              </ul>
              <p className={classes.para}>
                Each profit trader should pay significant attention to spread
                management. High performance and results can only be obtained
                when a high quantity of market conditions is taken into
                consideration. A strong trading technique is based on a proper
                evaluation of the specific financial condition and market
                indicators of a deal. Spreads tend to fluctuate, spread
                management policy must be flexible enough to settle to market
                movement.
              </p>
              <h2 className={classes.subTitle}>Withdrawal</h2>
              <div className={classes.bottomBorder}></div>
              <p className={classes.para}>
                Withdrawing funds from your profit4money account is easy and
                quick. Withdrawal requests received before 1pm AEST will be
                acted on the same day, with all others being acted on within 24
                hours. profit4money only uses secure methods for sending your
                funds to you, and fully complies with internal and regulatory
                procedures to ensure the funds are safely processed. For more
                details on withdrawal procedures, see below or contact us for
                more information.
              </p>
            </div>
          </Grid>
          <Grid item item xs={12} sm={12} md={5} lg={5}>
            <ContactForm />
          </Grid>
        </Grid>
      </Grid>
      <Contact />
      <Footer />
    </div>
  );
}
