import React from "react";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Breadcrumbs from "@material-ui/core/Breadcrumbs";
import MobNavbar from "../WebDashboard/MobNavbar/MobNavbar";
import TopNavbar from "../WebDashboard/TopNavbar/TopNavbar";
import Navbar from "../WebDashboard/Navbar/Navbar";
import Contact from "../WebDashboard/Contact/Contact";
import Footer from "../WebDashboard/Footer/Footer";
import ContactForm from "../ContactForm/ContactForm";
import { useStyles } from "./css";
import { Link } from "react-router-dom";

export default function FullWidthGrid() {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <MobNavbar />
      <TopNavbar />
      <Navbar />
      <Grid container spacing={0}>
        <Grid container className={classes.paddingDiv}>
          <Grid item xs={12} sm={12} md={7} lg={7}>
            <div>
              <Breadcrumbs aria-label="breadcrumb" className={classes.titleTop}>
                <Link to="/" className={classes.link}>
                  <b className={classes.breadHome}>Home</b>
                </Link>
                <Typography color="textPrimary">
                  <b className={classes.breadText}>Tools</b>
                </Typography>
                <Typography color="textPrimary">
                  <b className={classes.breadText}>Withdrawl Process</b>
                </Typography>
              </Breadcrumbs>
              <h1 className={classes.title}>Foreign exchange withdrawal</h1>
              <div className={classes.bottomBorder}></div>
              <p className={classes.para}>
                Considering the fact that income-making is the primary goal of
                profit trading, you need to be capable of harvest the earnings
                in a clean-going way. At profit4money, we believe in easy and
                effective ways of trading in overseas currency. For this reason,
                our withdrawal procedure is also brief and clean. Budget may be
                withdrawn as much as the balance restricts set on your account
                which varies in every case.
              </p>
              <p className={classes.para}>
                There are numerous approaches of retreating the quantity.
                Normally, it takes up to three running days to technique your
                withdrawal request. But, it could get prolonged in case there
                may be a few technical issues with the transaction. We keep you
                published approximately the progress of the transaction.
              </p>
              <h2 className={classes.subTitle}>It’s easy</h2>
              <div className={classes.bottomBorder}></div>
              <p className={classes.para}>
                Checking out money out of your buying and selling account isn't
                a huge deal with profit4money. The steps are quite
                user-pleasant.
              </p>

              <p className={classes.para}>
                You want to login to the account at the website. Click at the
                ‘withdrawal of funds’ alternative in the menu. Fill in the easy
                and brief request shape for withdrawal and put up. We will right
                away renowned the receipt and let you know about the popularity
                time to time. Withdrawal hyperlink can be accessed from the
                login every time and everywhere.
              </p>
              <p className={classes.para}>
                We want to make every procedure convenient so that you get the
                fine blessings of profit buying and selling.
              </p>
              <h2 className={classes.subTitle}>It’s secure</h2>
              <div className={classes.bottomBorder}></div>
              <p className={classes.para}>
                Within the present day era of era, your trading account must be
                secure and comfy from unwanted or unauthorized get entry to. Its
                miles our primary subject to offer the exceptional protection to
                you. We’re compliant with the regulatory norms and inner
                approaches so that each penny of you is safe.
              </p>
              <p className={classes.para}>
                You need to take into account that we accept as true with in
                fare and felony strategies of doing commercial enterprise.
                Therefore, each transaction made via you gets documented and
                recorded. We do not switch the funds to the 0.33-birthday
                celebration account. it gets credited to you inside the
                stipulated time. Our withdrawal procedure makes profit trading a
                satisfaction!
              </p>
              <p className={classes.para}>
                The data in real-time to reveal live rates on currencies,
                indices and commodities – and assist you decide the opportune
                second to go into or go out a trade.MCX(Multi Commodity
                Exchange) rates are also available here. ZerodhaTradinghas
                lowest, cheapest brokerage rates can also be done and it’s very
                easy.Portfolio investment, investment done by individual or
                company.
              </p>
            </div>
          </Grid>
          <Grid item item xs={12} sm={12} md={5} lg={5}>
            <ContactForm />
          </Grid>
        </Grid>
      </Grid>
      <Contact />
      <Footer />
    </div>
  );
}
