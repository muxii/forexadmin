import React from "react";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Breadcrumbs from "@material-ui/core/Breadcrumbs";
import MobNavbar from "../WebDashboard/MobNavbar/MobNavbar";
import TopNavbar from "../WebDashboard/TopNavbar/TopNavbar";
import Navbar from "../WebDashboard/Navbar/Navbar";
import Contact from "../WebDashboard/Contact/Contact";
import Footer from "../WebDashboard/Footer/Footer";
import ContactForm from "../ContactForm/ContactForm";
import { useStyles } from "./css";
import { Link } from "react-router-dom";

export default function FullWidthGrid() {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <MobNavbar />
      <TopNavbar />
      <Navbar />
      <Grid container spacing={0}>
        <Grid container className={classes.paddingDiv}>
          <Grid item xs={12} sm={12} md={7} lg={7}>
            <div>
              <Breadcrumbs aria-label="breadcrumb" className={classes.titleTop}>
                <Link to="/" className={classes.link}>
                  <b className={classes.breadHome}>Home</b>
                </Link>
                <Typography color="textPrimary">
                  <b className={classes.breadText}>profit</b>
                </Typography>
                <Typography color="textPrimary">
                  <b className={classes.breadText}>Registration Process</b>
                </Typography>
              </Breadcrumbs>
              <h1 className={classes.title}>
                Step by Step Tutorial for Registration
              </h1>
              <div className={classes.bottomBorder}></div>
              <h2 className={classes.subTitle}>Process of registration</h2>
              <p className={classes.para}>
                The main question that beginners ask is How to start profit
                trading. The first step of trading starts with having an account
                on the Best profit trading platform. The online registration
                process is a quick and easier method to start profit trading.
                Here are the steps that are very basic and will step by step
                guide you in the process of online registration of a trading
                account.
              </p>
              <h2 className={classes.subTitle}>
                STEP 1: Please fill in your application
              </h2>
              <p className={classes.para}>
                To start with the online registration process, start with the
                filling up your application form. This can be one by clicking on
                the ‘Open Account’ option that appears on the right corner of
                the screen. You can also start this process by choosing the
                Sign-Up option.
              </p>
              <h2 className={classes.subTitle}>
                STEP 2: Submit verification documents
              </h2>
              <p className={classes.para}>
                The next step will ask you to fill up your personal information.
                The personal data is essential and should be filled carefully.
                The data entered should be updated and relevant. You should also
                fit in the age criteria to open the profit trading account. This
                is the KYC step where you will choose your username and set a
                password for your account. Make sure the name matches with
                identity proof you have. Every detail you enter should be double
                checked.
              </p>
              <h2 className={classes.subTitle}>
                STEP 3: Receiving login information
              </h2>
              <p className={classes.para}>
                Check your mail as you will receive the confirmation link after
                you have successfully entered your username and password. In
                case you do not receive any mail, check the spam folder. Your
                verification will only be completed after you have confirmed
                your mail ID.
              </p>
              <h2 className={classes.subTitle}>
                STEP 4: First deposit is necessary for trading
              </h2>
              <p className={classes.para}>
                After you have completed the verification process, you need to
                make your first deposit. Choose the type of account you want and
                then deposit your first amount. The details of your account will
                contain details such as:
              </p>
              <ul>
                <li className={classes.para1}>Account Number</li>
                <li className={classes.para1}>
                  Type of Account (Real or Demo)
                </li>
                <li className={classes.para1}>Currency(Euro or USD)</li>
                <li className={classes.para1}>Leverage</li>
                <li className={classes.para1}>Account Balance</li>
              </ul>
              <h2 className={classes.subTitle}>STEP 5: Start trading</h2>
              <p className={classes.para}>
                Once you have finished all the above registration processes, you
                are now free to trade worldwide. profit is a very convenient way
                of trading. Connect with the best profit traders and enjoy
                trading as never before.
              </p>
              <p className={classes.para}>
                If you follow this step by step guide, you can effortlessly
                start trading from your online account.
              </p>
            </div>
          </Grid>
          <Grid item item xs={12} sm={12} md={5} lg={5}>
            <ContactForm />
          </Grid>
        </Grid>
      </Grid>
      <Contact />
      <Footer />
    </div>
  );
}
