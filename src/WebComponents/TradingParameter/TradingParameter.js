import React from "react";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Breadcrumbs from "@material-ui/core/Breadcrumbs";
import MobNavbar from "../WebDashboard/MobNavbar/MobNavbar";
import TopNavbar from "../WebDashboard/TopNavbar/TopNavbar";
import Navbar from "../WebDashboard/Navbar/Navbar";
import Contact from "../WebDashboard/Contact/Contact";
import Footer from "../WebDashboard/Footer/Footer";
import ContactForm from "../ContactForm/ContactForm";
import { useStyles } from "./css";
import { Link } from "react-router-dom";

export default function FullWidthGrid() {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <MobNavbar />
      <TopNavbar />
      <Navbar />
      <Grid container spacing={0}>
        <Grid container className={classes.paddingDiv}>
          <Grid item xs={12} sm={12} md={7} lg={7}>
            <div>
              <Breadcrumbs aria-label="breadcrumb" className={classes.titleTop}>
                <Link to="/" className={classes.link}>
                  <b className={classes.breadHome}>Home</b>
                </Link>
                <Typography color="textPrimary">
                  <b className={classes.breadText}>Tools</b>
                </Typography>
                <Typography color="textPrimary">
                  <b className={classes.breadText}>Trading Parameters</b>
                </Typography>
              </Breadcrumbs>
              <h1 className={classes.title}>Trading Parameters</h1>
              <div className={classes.bottomBorder}></div>
              <p className={classes.para}>
                profit4money has these days multiplied its buying and selling
                endeavours by means of including commodities and indices trading
                via CFDs and now stocks trading and bit coin. Our buying and
                selling situations have modified as a consequence with leverage
                and margin capabilities now available in line with symbol. This
                brings exceptional advantage to your buying and selling
                experience.
              </p>
              <p className={classes.para}>
                Now change with new contraptions with get admission to the
                shares marketplace, the destiny marketplace or even the digital
                marketplace.
              </p>
              <h2 className={classes.subTitle}>
                Risk Process Management& Risk Finance Management
              </h2>
              <div className={classes.bottomBorder}></div>
              <p className={classes.para}>
                Your account safety has multiplied, as the new system tracking
                your chance elements per image, thereby supporting to prevent
                your account from being calibrated.
              </p>
              <p className={classes.para}>
                With the margin calculations, and preservation margin in line
                with symbol with particular parameters consistent with image –
                chance calculations are according to positions are increasingly
                more correct and defend your consistent with symbol trading in
                addition to your whole account.
              </p>
              <h2 className={classes.subTitle}>Leverage& its symbol</h2>
              <div className={classes.bottomBorder}></div>
              <p className={classes.para}>
                Leverage lets in you to change with quantities a lot higher than
                your preliminary investment amount, which will increase the
                potential, go back of your funding. although the potential to
                earn full-size earnings by way of using leverage is huge,
                leverage can also work in opposition to you if the market goes
                within the path opposite of your trade. Buyers commonly use
                prevent loss & take profit to keep away from such situations. Up
                to now, the leverage offered becomes assigned in your entire
                equity.
              </p>
              <p className={classes.para}>
                Leverage in the proportion market has a tendency to be plenty
                decrease than in the foreign exchange market. Therefore, as we
                input this market, leverage will now be offered in keeping with
                symbol. Because of this every function will be leveraged in
                keeping with the leverage assigned to the unique symbol in that
                you choose to open a position.
              </p>
              <h2 className={classes.subTitle}>Available Balance</h2>
              <div className={classes.bottomBorder}></div>
              <p className={classes.para}>
                We having advantage per attribute as well allows you to
                carefully adviser the absolute bulk of your trades. Anniversary
                time you accessible a position, the absolute bulk of your
                investment in this position [non leveraged bulk according to the
                advantage per symbol] is deducted from your Accessible Balance.
                This agency that you may now appearance the actual Accessible
                Balance of your disinterestedness in absolute time and in
                non-leveraged ethics (available for trading).
              </p>
              <h2 className={classes.subTitle}>Margin</h2>
              <div className={classes.bottomBorder}></div>
              <p className={classes.para}>
                The allowance constant shows you the arrangement amid your
                disinterestedness and your net acknowledgment (the sum of all
                accessible positions adapted into the annual abject currency).
                Once your allowance exceeds the accustomed limit, your annual is
                at accident of arrangement and ALL of your positions or
                allotment of your trades are automatically closed. For this
                reason, your allowance is monitored in absolute time.
              </p>
              <h2 className={classes.subTitle}>Margin per Symbol</h2>
              <div className={classes.bottomBorder}></div>
              <p className={classes.para}>
                As we admission the allotment bazaar with advantage per symbol,
                the allowance activated is as well getting offered per symbol.
                Anniversary attribute will now accept its own allowance banned
                according to the accident agency it represents and according to
                the advantage applied. This is in accession to the absolute
                annual allowance [mentioned above] which will abide to
                accomplish as an indicator for your annual status. In adjustment
                to advice adviser your Allowance per Symbol, a ecology
                arrangement has been placed to adviser your Allowance per
                Symbol; the Maintenance Allowance System.
              </p>
              <h2 className={classes.subTitle}>Maintenance Allowance System</h2>
              <div className={classes.bottomBorder}></div>
              <p className={classes.para}>
                The Maintenance Allowance is an automated arrangement set to
                assure your annual investments, and anticipate your absolute
                annual from getting calibrated by advancement your allowance
                levels per symbol. The Maintenance Allowance will affectation
                the about sum of all margins and is affected anniversary time a
                position is opened. By celebratory your disinterestedness and
                Maintenance Allowance ethics you can adviser your ambit from
                allowance alarm status.
              </p>
              <h2 className={classes.subTitle}>Margin Call</h2>
              <div className={classes.bottomBorder}></div>
              <p className={classes.para}>
                The margin parameter shows you the ratio between your equity and
                your net exposure (the sum of all open positions converted into
                the account base currency). Once your margin exceeds the allowed
                limit, your account is at risk of calibration and ALL of your
                positions or part of your trades are automatically closed. For
                this reason, your margin is monitored in real time
              </p>
              <h2 className={classes.subTitle}>Margin per Symbol</h2>
              <div className={classes.bottomBorder}></div>
              <p className={classes.para}>
                As we enter the share market with leverage per symbol, the
                margin applied is also being offered per symbol. Each symbol
                will now receive its own margin limits according to the risk
                factor it represents and according to the leverage applied. This
                is in addition to the entire account margin [mentioned above]
                which will continue to operate as an indicator for your account
                status. In order to help monitor your Margin per Symbol, a
                monitoring system has been placed to monitor your Margin per
                Symbol; the Maintenance Margin System.
              </p>
              <h2 className={classes.subTitle}>Maintenance Margin System</h2>
              <div className={classes.bottomBorder}></div>
              <p className={classes.para}>
                The Maintenance Margin is an automatic system set to protect
                your account investments, and prevent your entire account from
                being calibrated by maintaining your margin levels per symbol.
                The Maintenance Margin will display the relative sum of all
                margins and is calculated each time a position is opened. By
                observing your equity and Maintenance Margin values you can
                monitor your distance from margin call status.
              </p>
              <h2 className={classes.subTitle}>Margin Call</h2>
              <div className={classes.bottomBorder}></div>
              <p className={classes.para}>
                Once the sum of positions from an accurate symbol, in an
                accurate bazaar direction, hits the accident allowance for that
                symbol, positions from that attribute will automatically
                activate to abutting until an acceptable allowance is restored.
                Positions will activate to abutting until an acceptable
                allowance for that attribute is achieved; thereby accretion
                aegis of added positions as able-bodied as the account’s
                absolute margin. The activity of closing position per attribute
                is alleged Allowance Call.
              </p>
              <h2 className={classes.subTitle}>Equity</h2>
              <div className={classes.bottomBorder}></div>
              <p className={classes.para}>
                The accepted absolute bulk in the account, affected by this
                formula: (Total Annual Deposits) bare (Total Annual Withdraws)
                additional (Closed Profit and Loss) additional (Open Profit and
                Loss) additional Bonus, adapted into the Annual Abject Currency.
              </p>
              <h2 className={classes.subTitle}>Open P&L</h2>
              <div className={classes.bottomBorder}></div>
              <p className={classes.para}>
                Open P & L: Total Profit and Loss of all accessible positions
                (Including Premium Charges) adapted into the Annual Abject
                Currency.
              </p>
              <h2 className={classes.subTitle}>Net Exposure</h2>
              <div className={classes.bottomBorder}></div>
              <p className={classes.para}>
                Net Exposure: The sum of all accessible positions adapted into
                the Annual Abject Currency.
              </p>
              <h2 className={classes.subTitle}>Pending Bonus</h2>
              <div className={classes.bottomBorder}></div>
              <p className={classes.para}>
                Pending Bonus: Displays the Bonus bulk that will be transferred
                into the Equity, provided that you beat the "Volume to Reach"
                aural the set period.
              </p>
              <h2 className={classes.subTitle}>XPoints to Reach</h2>
              <div className={classes.bottomBorder}></div>
              <p className={classes.para}>
                XPoints to Reach: Presents the XPoints you charge to ability
                aural the set aeon in adjustment to accept the awaiting Bonus
                transferred into the Equity.
              </p>
              <h2 className={classes.subTitle}>Bonus Codes</h2>
              <div className={classes.bottomBorder}></div>
              <p className={classes.para}>
                Bonus Codes: Bonus Codes add assorted banknote bonuses to a
                player's account, depending on the blazon of the code. To redeem
                a Bonus, traders accept to use the agnate Bonus Cipher if
                authoritative a deposit. To admission the Drop page, login to
                the WebTrader belvedere and bang the Drop Tab. On the Drop page,
                blazon a accurate Bonus Cipher in the 'Bonus Code' field. If you
                do not accept a accurate Bonus Code, this acreage will abide
                blank.
              </p>
              <p className={classes.para}>
                Currency exchange records with our live charts. Our free trading
                charts make it easy for you to get admission to live alternate
                quotes and historical rate styles of global currencies.
                Calculate foreign money and profit prices with this free
                currency converter or currency calculater.
              </p>
            </div>
          </Grid>
          <Grid item item xs={12} sm={12} md={5} lg={5}>
            <ContactForm />
          </Grid>
        </Grid>
      </Grid>
      <Contact />
      <Footer />
    </div>
  );
}
