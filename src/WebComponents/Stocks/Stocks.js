import React from "react";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Breadcrumbs from "@material-ui/core/Breadcrumbs";
import MobNavbar from "../WebDashboard/MobNavbar/MobNavbar";
import TopNavbar from "../WebDashboard/TopNavbar/TopNavbar";
import Navbar from "../WebDashboard/Navbar/Navbar";
import Contact from "../WebDashboard/Contact/Contact";
import Footer from "../WebDashboard/Footer/Footer";
import ContactForm from "../ContactForm/ContactForm";
import { useStyles } from "./css";
import { Link } from "react-router-dom";

export default function FullWidthGrid() {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <MobNavbar />
      <TopNavbar />
      <Navbar />
      <Grid container spacing={0}>
        <Grid container className={classes.paddingDiv}>
          <Grid item xs={12} sm={12} md={7} lg={7}>
            <div>
              <Breadcrumbs aria-label="breadcrumb" className={classes.titleTop}>
                <Link to="/" className={classes.link}>
                  <b className={classes.breadHome}>Home</b>
                </Link>
                <Typography color="textPrimary">
                  <b className={classes.breadText}>profit</b>
                </Typography>
                <Typography color="textPrimary">
                  <b className={classes.breadText}>Stocks</b>
                </Typography>
              </Breadcrumbs>
              <h1 className={classes.title}>Stocks</h1>
              <div className={classes.bottomBorder}></div>
              <p className={classes.para}>
                Stock trading had been one of the most popular ways of
                investments over all this time since business and trading has
                gained pace. Stock represents a share in the company’s earnings
                and assets. Holding a company’s share provides a trader the
                decision making rights proportionately in the company allowing
                them to attend the meetings of the board of directors.
              </p>
              <h2 className={classes.subTitle}>What is stock trading?</h2>
              <div className={classes.bottomBorder}></div>
              <p className={classes.para}>
                Stock trading in simple words could be defined as buying and
                selling of stocks in order to capitalize on regular price
                fluctuations. The short-term traders involved in this tend to
                bet about being able to make up a few bucks within the next
                hour, day or minute. There are the following types of stock
                trading:
              </p>
              <p className={classes.para}>
                <b>Active Trading:</b> this one is generally conducted by
                traders who place about 10 or more trades every month. Their
                strategies are dependent greatly on the right timing so that the
                investments could actually make up to suitable profits.
              </p>
              <p className={classes.para}>
                <b>Day trading:</b> this one is a popular strategy determined by
                traders who generally prefer playing hot potato with stocks i.e.
                buying and selling stocks and closing up their positions. All
                this is done within a single trading day without considering the
                effects within the underlying market. The general aim of
                investors pursuing day trading is to earn a few bucks in the
                next few hours.
              </p>
            </div>
          </Grid>
          <Grid item item xs={12} sm={12} md={5} lg={5}>
            <ContactForm />
          </Grid>
          <Grid item item xs={12} sm={12} md={12} lg={12}>
            <div>
              <h2 className={classes.subTitle}>Advantages of stock trading</h2>
              <p className={classes.para}>
                Stock trading prevails as one of the most popular investment
                <b>trade India.</b> Here are a few important benefits of it:
              </p>
              <p className={classes.para}>
                <b>Take advantage of the growing economy:</b> With the growth of
                the economy, corporate earnings also tend to increase. It could
                be a good way to make profit out of suitable situations.
              </p>
              <p className={classes.para}>
                <b>Stay ahead of inflation:</b> As per past records, stocks had
                been able to make out the best returns. Even when they perform
                the minimum, they can earn the investors considerable much
                profit to beat up the effects of inflation in various cases.
              </p>
              <p className={classes.para}>
                <b>Easy:</b> the regulations have made it very easy to buy
                stocks of considerable companies. You can purchase stocks
                through a broker, online or a financial broker. Just like
                purchasing a few, you can sell out shares very easily whenever
                you desire.
              </p>
              <p className={classes.para}>
                With <b>profitlive</b> updates you may catch up with market
                conditions and give a better direction to your investments,
                bringing out appropriate profit. You can easily manage risks and
                get on to the most successful spheres of investment recognizing
                growth in earnings.
              </p>
            </div>
          </Grid>
        </Grid>
      </Grid>
      <Contact />
      <Footer />
    </div>
  );
}
