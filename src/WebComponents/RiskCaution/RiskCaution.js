import React from "react";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Breadcrumbs from "@material-ui/core/Breadcrumbs";
import MobNavbar from "../WebDashboard/MobNavbar/MobNavbar";
import TopNavbar from "../WebDashboard/TopNavbar/TopNavbar";
import Navbar from "../WebDashboard/Navbar/Navbar";
import Contact from "../WebDashboard/Contact/Contact";
import Footer from "../WebDashboard/Footer/Footer";
import ContactForm from "../ContactForm/ContactForm";
import { useStyles } from "./css";
import { Link } from "react-router-dom";

export default function FullWidthGrid() {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <MobNavbar />
      <TopNavbar />
      <Navbar />
      <Grid container spacing={0}>
        <Grid container className={classes.paddingDiv}>
          <Grid item xs={12} sm={12} md={7} lg={7}>
            <div>
              <Breadcrumbs aria-label="breadcrumb" className={classes.titleTop}>
                <Link to="/" className={classes.link}>
                  <b className={classes.breadHome}>Home</b>
                </Link>
                <Typography color="textPrimary">
                  <b className={classes.breadText}>Tools</b>
                </Typography>
                <Typography color="textPrimary">
                  <b className={classes.breadText}>Risk Caution</b>
                </Typography>
              </Breadcrumbs>
              <h1 className={classes.title}>Risk Caution</h1>
              <div className={classes.bottomBorder}></div>
              <p className={classes.para}>
                Before you head into stock and foreign markets, remember that
                trading currencies, as well as other investment products, always
                involves a certain amount of risk. Sometimes due to financial
                fluctuation, you may not just suddenly boost your capital, but
                also lose it completely.
              </p>
              <p className={classes.para}>
                Thus, our potential clients have to make sure profit4money that
                they know all the possible risks and their consequences, they
                should be aware of all the rules and regulations governing the
                utilization of investment products. Our clients know that there
                are some features and risks that affect exchange rates, prices,
                as well as investment products. these risks include price
                changes, decrease in liquidity, high volatility, etc.
              </p>
              <p className={classes.para}>
                As of high volatility , there is a high amount of risk in both
                profit and loss in the financial market. The prices for
                underlying assets and derivative instruments may intensively
                change in wide ranges because of unexpected events and
                modifications in trading system. The company, nor the clients
                can control these changes. So, clients should understand
                specific market conditions, to avoid any losses.
              </p>
              <p className={classes.para}>
                Before you sign up with us, assure to go through the user
                agreement properly, and the basic rules and regulations of the
                financial markets.
              </p>
              <p className={classes.para}>
                profit4money is not responsible for any losses that happened as
                a result of regulations of stock markets, military actions,
                government restrictions and suspension of trading.
              </p>
              <p className={classes.para}>
                Any news, opinions, prices, analysis, and other information on
                this ire are offered as general market fact and does not
                constitute investment advice. profit4money has taken prudent
                measures to ensure the authority of the information and
                knowledge on the website. the content on our site to subject to
                modify at any moment without intimation.
              </p>
              <p className={classes.para}>
                Do not get into risky CFD or spot profit trading unless you know
                the basic information of such type of trading and its risks-
                like, how losses and profits are made, how positions are done,
                and many more.
              </p>
              <p className={classes.para}>
                CFDs and spot profit trading is a very risky type of trading as
                it involves a certain amount of risk. thus, trading in these
                sections is ideal only for those who already know how much risk
                involved in this trading.
              </p>
              <p className={classes.para}>
                Before start trading in any of these contracts, make sure to get
                advice from financial experts.
              </p>
              <p className={classes.para}>
                Former success in Trading Is Not necessarily Indicative Of
                further Results. Potential clients should not reply on investing
                in any type of trading only based on their past performance.
                Past success doesn't come with a guarantee of future success.
              </p>
              <p className={classes.para}>
                Furthermore, Clients must also depend on their review if the
                Entity or Person making the decisions and the Advisory Agreement
                terms including the benefits and risks involved.
              </p>
            </div>
          </Grid>
          <Grid item item xs={12} sm={12} md={5} lg={5}>
            <ContactForm />
          </Grid>
        </Grid>
      </Grid>
      <Contact />
      <Footer />
    </div>
  );
}
