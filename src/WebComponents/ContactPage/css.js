import { makeStyles, fade } from "@material-ui/core/styles";

export const useStyles = makeStyles(theme => ({
  root: {
    flexGrow: 1,
    "& label.Mui-focused": {
      color: "#25707D"
    },
    "& .MuiInput-underline:after": {
      borderBottomColor: "#25707D"
    },
    "& .MuiOutlinedInput-root": {
      "& fieldset": {
        borderColor: "#25707D"
      },
      "&:hover fieldset": {
        borderColor: "#25707D"
      },
      "&.Mui-focused fieldset": {
        borderColor: "#25707D"
      }
    }
  },
   titleTop: {
    marginTop: "150px",
    [theme.breakpoints.down("sm")]: {
      marginTop: "50px"
    }
  },
  breadHome: {
    color: "#25707D"
  },
  breadText: {
    color: "#333333",
    fontWeight: "200"
  },
  paddingDiv: {
    marginLeft: "10%",
    marginRight: "10%",
    marginBottom: "150px",
    [theme.breakpoints.down("sm")]: {
      marginLeft: "5%",
      marginRight: "5%",
      marginBottom: "50px"
    }
  },
  title: {
    marginTop: "20px",
    color: "#25707D",
    fontSize: "40px",
    [theme.breakpoints.down("sm")]: {
      fontSize: "30px"
    }
  },
  subTitle: {
    color: "#25707D",
    fontSize: "25px",
    [theme.breakpoints.down("sm")]: {
      fontSize: "20px"
    }
  },
  bottomBorder: {
    marginTop: "-10px",
    width: "60px",
    borderBottom: "3px solid #C34245"
  },
  para: {
    marginTop: "40px",
    color: "#333333",
    lineHeight: "30px",
    marginBottom: "27px",
    textAlign: "justify"
  },
  para1: {
    color: "#333333",
    lineHeight: "30px",
    textAlign: "justify"
  },
  moreButton: {
    textAlign: "left",
    marginBottom: "50px",
    textTransform: "none",
    backgroundColor: "#fff",
    color: "#25707D",
    fontSize: "15px",
    fontWeight: "900",
    cursor: "pointer",
    "&:hover": {
      backgroundColor: "#fff",
      color: "#25707D",
      width: "100px",
      borderBottom: "1px solid #25707D"
    }
  },
  addressCard: {
    marginTop: "-55px",
    marginLeft: "20px",
    padding: "42px",
    backgroundColor: "#25707D",
    color: "#fff",
    [theme.breakpoints.down("sm")]: {
      marginLeft: "0px",
      marginTop: "5px",
      marginBottom: "20px"
    }
  },
  cardContainer: {
    marginRight: "30px",
    marginTop: "40px",
    border: "1px solid #ededed",
    padding: "30px",
    position: "relative",
    height: "100px",
    "&:hover": {
      border: "1px solid #25707D"
    },
    [theme.breakpoints.down("sm")]: {
      marginRight: "0px"
    }
  },
  cardHeading: {
    marginTop: "-5px",
    color: "#25707D",
    fontSize: "18px"
  },
  cardPara: {
    fontSize: "14px",
    color: "#666666",
    lineHeight: "20px"
  },
  inputDiv: {
    width: "98%",
    [theme.breakpoints.down("sm")]: {
      marginTop: "20px"
    }
  },
  submitButton: {
    width: "150px",
    textAlign: "left",
    marginBottom: "50px",
    textTransform: "none",
    backgroundColor: "#25707D",
    color: "#fff",
    padding: "10px",
    fontSize: "17px",
    fontWeight: "900",
    cursor: "pointer",
    "&:hover": {
      backgroundColor: "#25707D",
      color: "#fff"
    }
  },
  link: {
    textDecoration: "none"
  },
  bottomLine: {
    marginTop: "100px",
    borderBottom: "1px solid #25707D"
  }
}));
