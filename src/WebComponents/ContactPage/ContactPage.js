import React from "react";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Breadcrumbs from "@material-ui/core/Breadcrumbs";
import MobNavbar from "../WebDashboard/MobNavbar/MobNavbar";
import TopNavbar from "../WebDashboard/TopNavbar/TopNavbar";
import Navbar from "../WebDashboard/Navbar/Navbar";
import Contact from "../WebDashboard/Contact/Contact";
import Footer from "../WebDashboard/Footer/Footer";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import { useStyles } from "./css";
import { Link } from "react-router-dom";

export default function FullWidthGrid() {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <MobNavbar />
      <TopNavbar />
      <Navbar />
      <Grid container spacing={0}>
        <Grid container className={classes.paddingDiv}>
          <Grid item xs={12} sm={12} md={12} lg={12}>
            <div>
              <Breadcrumbs aria-label="breadcrumb" className={classes.titleTop}>
                <Link to="/" className={classes.link}>
                  <b className={classes.breadHome}>Home</b>
                </Link>
                <Typography color="textPrimary">
                  <b className={classes.breadText}>Contact</b>
                </Typography>
              </Breadcrumbs>
              <h1 className={classes.title}>
                Start profit Trading Contact profit4money
              </h1>
              <div className={classes.bottomBorder}></div>
              <p className={classes.para}>
                profit4money support team is there to assist you round the
                clock. Our support team members are dedicated to helping clients
                and clarify their doubts in no time. Contact us through email,
                phone or chat anytime, Our FAQs also offer you useful
                information to some extent.
              </p>
              <p className={classes.para}>
                The simplest way to convey your query with us is by filling the
                following form: Just fills your Name, contact number, Gmail id
                and the suggestion or query you have, click on the send message.
                As soon we receive your query, we will respond to you within 24
                hours.
              </p>

              <Grid container spacing={0}>
                <Grid item xs={12} sm={12} md={3} lg={3}>
                  <TextField
                    className={classes.inputDiv}
                    fullWidth
                    id="outlined-basic"
                    placeholder="Enter Name"
                    variant="outlined"
                  />
                </Grid>
                <Grid item xs={12} sm={12} md={3} lg={3}>
                  <TextField
                    className={classes.inputDiv}
                    fullWidth
                    id="outlined-basic"
                    placeholder="Enter Email Address"
                    variant="outlined"
                  />
                </Grid>
                <Grid item xs={12} sm={12} md={3} lg={3}>
                  <TextField
                    className={classes.inputDiv}
                    fullWidth
                    id="outlined-basic"
                    placeholder="Enter Mobile Number"
                    variant="outlined"
                  />
                </Grid>
                <Grid item xs={12} sm={12} md={9} lg={9}>
                  <br />
                  <br />
                  <TextField
                    multiline
                    rows={8}
                    fullWidth
                    id="outlined-basic"
                    placeholder="Enter Message"
                    variant="outlined"
                  />
                  <br />
                  <br />
                </Grid>
                <Grid item xs={12} sm={12} md={3} lg={3}>
                  <div className={classes.addressCard}>
                    <p>
                      <i class="fa fa-map-marker"></i>
                      &nbsp; 133 Santina Parde Ellluk, Vantu House, Port Vila
                      Efate, Vanuatu
                    </p>
                    <p>
                      <i class="fa fa-mobile"></i>
                      &nbsp; +1 (727) 260-4364
                    </p>
                    <p>
                      <i class="fa fa-envelope"></i>
                      &nbsp; info@profit4money.com
                    </p>
                  </div>
                </Grid>
              </Grid>
              <Button className={classes.submitButton}>
                Submit &nbsp;
                <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
              </Button>
            </div>
          </Grid>
          <Grid item xs={12} sm={12} md={6} lg={6}>
            <div className={classes.cardContainer}>
              <h4 className={classes.cardHeading}>
                profit International Gain LTD
              </h4>
              <p className={classes.cardPara}>
                133 Santina Parde Ellluk, Vantu House, Port Vila Efate, Vanuatu
              </p>
            </div>
          </Grid>
          <Grid item xs={12} sm={12} md={6} lg={6}>
            <div className={classes.cardContainer}>
              <h4 className={classes.cardHeading}>Help Center</h4>
              <p className={classes.cardPara}>
                profit4money - Technical assistance and customer support 24/5
              </p>
            </div>
          </Grid>
        </Grid>
      </Grid>
      <Contact />
      <Footer />
    </div>
  );
}
