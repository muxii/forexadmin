import React from "react";
import Grid from "@material-ui/core/Grid";
import Typography from "@material-ui/core/Typography";
import Breadcrumbs from "@material-ui/core/Breadcrumbs";
import MobNavbar from "../WebDashboard/MobNavbar/MobNavbar";
import TopNavbar from "../WebDashboard/TopNavbar/TopNavbar";
import Navbar from "../WebDashboard/Navbar/Navbar";
import Contact from "../WebDashboard/Contact/Contact";
import Footer from "../WebDashboard/Footer/Footer";
import ContactForm from "../ContactForm/ContactForm";
import { useStyles } from "./css";
import { Link } from "react-router-dom";

export default function FullWidthGrid() {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <MobNavbar />
      <TopNavbar />
      <Navbar />
      <Grid container spacing={0}>
        <Grid container className={classes.paddingDiv}>
          <Grid item xs={12} sm={12} md={7} lg={7}>
            <div>
              <Breadcrumbs aria-label="breadcrumb" className={classes.titleTop}>
                <Link to="/" className={classes.link}>
                  <b className={classes.breadHome}>Home</b>
                </Link>
                <Typography color="textPrimary">
                  <b className={classes.breadText}>Tools</b>
                </Typography>
                <Typography color="textPrimary">
                  <b className={classes.breadText}>Account Verification</b>
                </Typography>
              </Breadcrumbs>
              <h1 className={classes.title}>Account Verification</h1>
              <div className={classes.bottomBorder}></div>
              <p className={classes.para}>
                If your account on profit4money is not verified yet, you can
                only take 60% benefit of all the services we offer you. Although
                verification of the account is not compulsory, it offers you
                access to the full use of the services. Its good to verify your
                identity, as it will improve your trading way.
              </p>
              <p className={classes.para}>
                The verification process is very simple. all you need to do is
                to upload the softcopy on your Id proof for passport. After
                that, your uploaded documents will be cross-checked.
              </p>
              <p className={classes.para}>
                Within 3 days after sending the verification documents, you will
                get an email verifying trading status or requesting for the
                additional documentation required. We have mentioned about all
                the required documents for different level of verification.
              </p>
              <p className={classes.para}>
                You have to come across 2 levels of account verification: the
                first one is already mentioned, and to pass the second level,
                you need to verify the first level mandatorily. Both the levels
                are simple, you only have to upload valid documents. Do not use
                false documents while verification, this can lead to an account
                ban.
              </p>
              <h2 className={classes.subTitle}>Points to ponder</h2>
              <div className={classes.bottomBorder}></div>
              <p className={classes.para}>
                Always keep the following things into consideration while
                sending verification documents:
              </p>
              <ul>
                <li>
                  <p className={classes.para1}>
                    The document must be in color and clarity.
                  </p>
                </li>
                <li>
                  <p className={classes.para1}>
                    The scanned document must have two blank pages
                  </p>
                </li>
                <li>
                  <p className={classes.para1}>
                    TDon't forget to scan both sides of the document
                  </p>
                </li>
                <li>
                  <p className={classes.para1}>
                    No scratch-outs and cancellations are allowed
                  </p>
                </li>
                <li>
                  <p className={classes.para1}>
                    upload only high-resolution photos
                  </p>
                </li>
                <li>
                  <p className={classes.para1}>
                    Name on the trading account should match the name used in
                    the document
                  </p>
                </li>
                <li>
                  <p className={classes.para1}>
                    Don't forget to upload proof of address for verification
                  </p>
                </li>
                <li>
                  <p className={classes.para1}>
                    Submitted documents should be valid for a maximum of 6
                    months
                  </p>
                </li>
                <li>
                  <p className={classes.para1}>
                    Accepted file types; jpeg, png, pdf, gif. The document size
                    must not cross 2MB.
                  </p>
                </li>
                <li>
                  <p className={classes.para1}>
                    Do not type name in Block letters. You are supposed to write
                    only the first letter of the word in Block letters.
                  </p>
                </li>
              </ul>
              <p className={classes.para}>
                By verifying address proof, you will assure that the local or
                permanent address you filled in your scanned documents is
                correct. You can upload your internet bill, mobile bill as the
                address proof. Make sure the document you upload for
                verification is not 6 months old.
              </p>
              <p className={classes.para}>
                if in any case, your verification request is rejected, you will
                get a notification to your gmail id; the cause of rejection will
                be informed in your place.
              </p>
              <p className={classes.para}>
                It's beneficial to get your account verified on profit4money, it
                means you have joined hands with us and our trading is really
                good. Make sure to read the procedure of uploading documents to
                avoid error.
              </p>
            </div>
          </Grid>
          <Grid item item xs={12} sm={12} md={5} lg={5}>
            <ContactForm />
          </Grid>
        </Grid>
      </Grid>
      <Contact />
      <Footer />
    </div>
  );
}
