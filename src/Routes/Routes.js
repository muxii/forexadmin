import React, { Suspense, lazy } from "react";
import { HashRouter, Route, Switch } from "react-router-dom";

import Loader from "../util/Loader/Loader";
// start
const WebDashboard = lazy(() =>
  import("../WebComponents/WebDashboard/WebDashboard")
);
const vision = lazy(() => import("../WebComponents/OurVision/OurVision"));
const Forex = lazy(() => import("../WebComponents/Forex/Forex"));
const Commodities = lazy(() =>
  import("../WebComponents/Commodities/Commodities")
);
const Indices = lazy(() => import("../WebComponents/Indices/Indices"));
const Stocks = lazy(() => import("../WebComponents/Stocks/Stocks"));
const Conditions = lazy(() => import("../WebComponents/Conditions/Conditions"));
const FAQs = lazy(() => import("../WebComponents/FAQs/FAQs"));
const AccountType = lazy(() =>
  import("../WebComponents/AccountType/AccountType")
);
const Registration = lazy(() =>
  import("../WebComponents/Registration/Registration")
);

const Vision = lazy(() => import("../WebComponents/Vision/Vision"));
const TradingBenefits = lazy(() =>
  import("../WebComponents/TradingBenefits/TradingBenefits")
);
const TradingSupport = lazy(() =>
  import("../WebComponents/TradingSupport/TradingSupport")
);
const HelpCenter = lazy(() => import("../WebComponents/HelpCenter/HelpCenter"));

const LiveAccount = lazy(() =>
  import("../WebComponents/LiveAccount/LiveAccount")
);
const AccountComparision = lazy(() =>
  import("../WebComponents/AccountComparision/AccountComparision")
);
const AccountVerification = lazy(() =>
  import("../WebComponents/AccountVerification/AccountVerification")
);
const TradingParameter = lazy(() =>
  import("../WebComponents/TradingParameter/TradingParameter")
);
const TradingSpreads = lazy(() =>
  import("../WebComponents/TradingSpreads/TradingSpreads")
);
const StopLoss = lazy(() => import("../WebComponents/StopLoss/StopLoss"));
const Premium = lazy(() => import("../WebComponents/Premium/Premium"));
const Withdrawal = lazy(() => import("../WebComponents/Withdrawal/Withdrawal"));
const Promotion = lazy(() => import("../WebComponents/Promotion/Promotion"));

const DailyMarket = lazy(() =>
  import("../WebComponents/DailyMarket/DailyMarket")
);
const FinancialCalendar = lazy(() =>
  import("../WebComponents/FinancialCalendar/FinancialCalendar")
);
const RealTime = lazy(() => import("../WebComponents/RealTime/RealTime"));
const Signals = lazy(() => import("../WebComponents/Signals/Signals"));
const Platforms = lazy(() => import("../WebComponents/Platforms/Platforms"));
const LiveMarket = lazy(() => import("../WebComponents/LiveMarket/LiveMarket"));
const EducationPackage = lazy(() =>
  import("../WebComponents/EducationPackage/EducationPackage")
);

const TradingPolicy = lazy(() =>
  import("../WebComponents/TradingPolicy/TradingPolicy")
);
const Disclaimer = lazy(() => import("../WebComponents/Disclaimer/Disclaimer"));
const TradingTerms = lazy(() =>
  import("../WebComponents/TradingTerms/TradingTerms")
);
const RiskCaution = lazy(() =>
  import("../WebComponents/RiskCaution/RiskCaution")
);

const ContactPage = lazy(() =>
  import("../WebComponents/ContactPage/ContactPage")
);
// end
const Login = lazy(() => import("../AdminComponents/Login/Login"));
const LiveDashboard = lazy(() =>
  import("../AdminComponents/LiveDashboard/LiveDashboard")
);
const DemoDashboard = lazy(() =>
  import("../AdminComponents/Demo/DemoDashboard/DemoDashboard")
);

const ActivityLog = lazy(() =>
  import("../AdminComponents/ActivityLog/ActivityLog")
);
const Profile = lazy(() => import("../AdminComponents/Profile/Profile"));
const LiveCharts = lazy(() => import("../AdminComponents/LiveChart/LiveChart"));
const Calendar = lazy(() =>
  import("../AdminComponents/EconomicCalendar/Calendar")
);
const Documents = lazy(() => import("../AdminComponents/Documents/Documents"));
const BankDetails = lazy(() =>
  import("../AdminComponents/BankDetails/BankDetails")
);
const ChangePassword = lazy(() =>
  import("../AdminComponents/ChangePassword/ChangePassword")
);
const ClosedDeals = lazy(() =>
  import("../AdminComponents/ClosedDeals/ClosedDeals")
);
const AccountStatement = lazy(() =>
  import("../AdminComponents/AccountStatement/AccountStatement")
);
const Deposit = lazy(() => import("../AdminComponents/Deposit/Deposit"));
const Withdraw = lazy(() => import("../AdminComponents/Withdraw/Withdraw"));
const AddBank = lazy(() => import("../AdminComponents/AddBank/AddBank"));
const AddDeposit = lazy(() =>
  import("../AdminComponents/AddDeposit/AddDeposit")
);
const AddWithdraw = lazy(() =>
  import("../AdminComponents/AddWithdraw/AddWithdraw")
);

const Register = lazy(() => import("../AdminComponents/Register/Register"));

const DemoClosedDeals = lazy(() =>
  import("../AdminComponents/Demo/DemoClosedDeals/DemoClosedDeals")
);
const DemoAccountStatement = lazy(() =>
  import("../AdminComponents/Demo/DemoAccountStatement/DemoAccountStatement")
);

const NotFound = lazy(() => import("../util/NotFound/NotFound"));

export const Routes = () => {
  return (
    <HashRouter>
      <Suspense fallback={<Loader />}>
        <Switch>
          {/* website routes start */}
          <Route exact path="/" component={WebDashboard} />

          <Route exact path="/forex" component={Forex} />
          <Route exact path="/commodities" component={Commodities} />
          <Route exact path="/indices" component={Indices} />
          <Route exact path="/stocks" component={Stocks} />
          <Route exact path="/conditions" component={Conditions} />
          <Route exact path="/faqs" component={FAQs} />
          <Route exact path="/accountType" component={AccountType} />
          <Route exact path="/registrationProcess" component={Registration} />

          <Route exact path="/vision" component={Vision} />
          <Route exact path="/tradingBenefits" component={TradingBenefits} />
          <Route exact path="/tradingSupport" component={TradingSupport} />
          <Route exact path="/helpCenter" component={HelpCenter} />

          <Route exact path="/liveAccount" component={LiveAccount} />
          <Route
            exact
            path="/accountComparision"
            component={AccountComparision}
          />
          <Route
            exact
            path="/accountVerification"
            component={AccountVerification}
          />
          <Route exact path="/tradingParameter" component={TradingParameter} />
          <Route exact path="/tradingSpreads" component={TradingSpreads} />
          <Route exact path="/stopLoss" component={StopLoss} />
          <Route exact path="/premium" component={Premium} />
          <Route exact path="/withdrawal" component={Withdrawal} />
          <Route exact path="/promotion" component={Promotion} />

          <Route exact path="/dailyMarket" component={DailyMarket} />
          <Route
            exact
            path="/financialCalendar"
            component={FinancialCalendar}
          />
          <Route exact path="/realTimeCharts" component={RealTime} />
          <Route exact path="/signals" component={Signals} />
          <Route exact path="/platforms" component={Platforms} />
          <Route exact path="/liveMarketRates" component={LiveMarket} />
          <Route exact path="/educationPackage" component={EducationPackage} />

          <Route exact path="/tradingPolicy" component={TradingPolicy} />
          <Route exact path="/disclaimer" component={Disclaimer} />
          <Route exact path="/tradingTerms" component={TradingTerms} />
          <Route exact path="/riskCaution" component={RiskCaution} />

          <Route exact path="/contact" component={ContactPage} />
          {/* website routes end */}
          <Route exact path="/vision" component={vision} />
          <Route exact path="/forex4money/admin/login" component={Login} />
          <Route
            exact
            path="/forex4money/admin/register"
            component={Register}
          />
          <Route
            exact
            path="/forex4money/admin/live/dashboard"
            component={LiveDashboard}
          />
          <Route
            exact
            path="/forex4money/admin/demo/dashboard"
            component={DemoDashboard}
          />

          <Route
            exact
            path="/forex4money/admin/activityLog"
            component={ActivityLog}
          />
          <Route exact path="/forex4money/admin/profile" component={Profile} />
          <Route
            exact
            path="/forex4money/admin/livecharts"
            component={LiveCharts}
          />
          <Route
            exact
            path="/forex4money/admin/economic"
            component={Calendar}
          />
          <Route
            exact
            path="/forex4money/admin/documents"
            component={Documents}
          />
          <Route
            exact
            path="/forex4money/admin/bankDetails"
            component={BankDetails}
          />
          <Route
            exact
            path="/forex4money/admin/changePassword"
            component={ChangePassword}
          />
          <Route
            exact
            path="/forex4money/admin/closedDeals"
            component={ClosedDeals}
          />
          <Route
            exact
            path="/forex4money/admin/accountStatement"
            component={AccountStatement}
          />
          <Route exact path="/forex4money/admin/deposit" component={Deposit} />
          <Route
            exact
            path="/forex4money/admin/withdraw"
            component={Withdraw}
          />

          <Route
            exact
            path="/forex4money/admin/bankDetails/addBank"
            component={AddBank}
          />
          <Route
            exact
            path="/forex4money/admin/deposit/addDeposit"
            component={AddDeposit}
          />
          <Route
            exact
            path="/forex4money/admin/withdraw/addWithdraw"
            component={AddWithdraw}
          />

          {/* Demo Pages */}
          <Route
            exact
            path="/forex4money/admin/demo/accountStatement"
            component={DemoAccountStatement}
          />
          <Route
            exact
            path="/forex4money/admin/demo/closedDeals"
            component={DemoClosedDeals}
          />

          <Route exact path="*" component={NotFound} />
        </Switch>
      </Suspense>
    </HashRouter>
  );
};
